-- phpMyAdmin SQL Dump
-- version 4.0.10.5
-- http://www.phpmyadmin.net
--
-- Host: mysql.msoft.priv
-- Generato il: Nov 27, 2015 alle 11:11
-- Versione del server: 5.1.73-0ubuntu0.10.04.1
-- Versione PHP: 5.3.10-1ubuntu3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tamautodb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_directories`
--

CREATE TABLE IF NOT EXISTS `kfm_directories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `parent` int(11) NOT NULL,
  `maxwidth` int(11) DEFAULT '0',
  `maxheight` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_directories`
--

INSERT INTO `kfm_directories` (`id`, `name`, `parent`, `maxwidth`, `maxheight`) VALUES
(1, 'root', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_files`
--

CREATE TABLE IF NOT EXISTS `kfm_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `directory` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_files`
--

INSERT INTO `kfm_files` (`id`, `name`, `directory`) VALUES
(1, 'logo_tamauto_interno.jpg', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_files_images`
--

CREATE TABLE IF NOT EXISTS `kfm_files_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` text,
  `file_id` int(11) NOT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_files_images`
--

INSERT INTO `kfm_files_images` (`id`, `caption`, `file_id`, `width`, `height`) VALUES
(1, '', 1, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_files_images_thumbs`
--

CREATE TABLE IF NOT EXISTS `kfm_files_images_thumbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_files_images_thumbs`
--

INSERT INTO `kfm_files_images_thumbs` (`id`, `image_id`, `width`, `height`) VALUES
(1, 1, 64, 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_parameters`
--

CREATE TABLE IF NOT EXISTS `kfm_parameters` (
  `name` text,
  `value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dump dei dati per la tabella `kfm_parameters`
--

INSERT INTO `kfm_parameters` (`name`, `value`) VALUES
('version', '1.4'),
('version_db', '8'),
('last_registration', '2011-04-15');

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_plugin_extensions`
--

CREATE TABLE IF NOT EXISTS `kfm_plugin_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(64) DEFAULT NULL,
  `plugin` varchar(64) DEFAULT NULL,
  `user_id` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_session`
--

CREATE TABLE IF NOT EXISTS `kfm_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie` varchar(32) DEFAULT NULL,
  `last_accessed` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_session`
--

INSERT INTO `kfm_session` (`id`, `cookie`, `last_accessed`) VALUES
(1, 'c4ca4238a0b923820dcc509a6f75849b', '2011-04-15 15:34:49');

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_session_vars`
--

CREATE TABLE IF NOT EXISTS `kfm_session_vars` (
  `session_id` int(11) DEFAULT NULL,
  `varname` text,
  `varvalue` text,
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dump dei dati per la tabella `kfm_session_vars`
--

INSERT INTO `kfm_session_vars` (`session_id`, `varname`, `varvalue`) VALUES
(1, 'cwd_id', '1'),
(1, 'language', '""'),
(1, 'user_id', '1'),
(1, 'username', '""'),
(1, 'password', '""'),
(1, 'loggedin', '0'),
(1, 'theme', '"default"'),
(1, 'kfm_url', '"http:\\/\\/192.168.0.77\\/tamauto\\/gestion\\/modules\\/tiny_mce\\/plugins\\/kfm\\/index.php?\\/\\/tamauto\\/gestion\\/modules\\/tiny_mce\\/plugins\\/"');

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_settings`
--

CREATE TABLE IF NOT EXISTS `kfm_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `value` varchar(256) DEFAULT NULL,
  `user_id` int(8) DEFAULT NULL,
  `usersetting` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_settings`
--

INSERT INTO `kfm_settings` (`id`, `name`, `value`, `user_id`, `usersetting`) VALUES
(1, 'kfm_url', '/tamauto/gestion/modules/tiny_mce/plugins/kfm', 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_tagged_files`
--

CREATE TABLE IF NOT EXISTS `kfm_tagged_files` (
  `file_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_tags`
--

CREATE TABLE IF NOT EXISTS `kfm_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_translations`
--

CREATE TABLE IF NOT EXISTS `kfm_translations` (
  `original` text,
  `translation` text,
  `language` varchar(2) DEFAULT NULL,
  `calls` int(11) DEFAULT '0',
  `found` int(11) DEFAULT '1',
  `context` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struttura della tabella `kfm_users`
--

CREATE TABLE IF NOT EXISTS `kfm_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `status` int(1) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `kfm_users`
--

INSERT INTO `kfm_users` (`id`, `username`, `password`, `status`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `sorttable`
--

CREATE TABLE IF NOT EXISTS `sorttable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sort` text,
  `query` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `sorttable`
--

INSERT INTO `sorttable` (`id`, `sort`, `query`) VALUES
(1, 'Modalità ordine numerico', 'ORDER BY main DESC, position'),
(2, 'Modalità alfabetico', 'ORDER BY main DESC, itemTitle'),
(3, 'Per data crescente', 'ORDER BY main DESC, creationDate asc'),
(4, 'Per data decrescente', 'ORDER BY main DESC, creationDate desc');

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautocategory`
--

CREATE TABLE IF NOT EXISTS `tamautocategory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `section` text,
  `position` bigint(20) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `father` bigint(20) NOT NULL DEFAULT '0',
  `permission` int(11) NOT NULL DEFAULT '0',
  `menu` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=13 ;

--
-- Dump dei dati per la tabella `tamautocategory`
--

INSERT INTO `tamautocategory` (`id`, `section`, `position`, `sort`, `father`, `permission`, `menu`, `active`, `del`) VALUES
(1, 'index', 1, 1, 0, 0, 1, 1, 0),
(2, 'about-us', 2, 1, 0, 0, 1, 1, 0),
(3, 'history', 3, 1, 0, 0, 1, 1, 0),
(4, 'where', 4, 1, 0, 0, 1, 1, 0),
(5, 'services', 7, 1, 0, 0, 1, 1, 1),
(6, 'gallery', 8, 1, 0, 0, 1, 1, 0),
(7, 'press', 9, 4, 0, 0, 1, 1, 0),
(8, 'news', 10, 1, 0, 0, 1, 1, 0),
(9, 'link', 11, 1, 0, 0, 1, 1, 0),
(10, 'contacts', 12, 1, 0, 0, 1, 1, 0),
(11, 'vetture', 5, 1, 0, 0, 1, 1, 0),
(12, 'palmares', 6, 1, 0, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautocategorydetails`
--

CREATE TABLE IF NOT EXISTS `tamautocategorydetails` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` text,
  `idCat` bigint(20) NOT NULL,
  `lang` tinyint(4) NOT NULL DEFAULT '1',
  `backgroundImage` text,
  `backgroundColor` text,
  `description` text,
  `keywords` text,
  `textColor` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=13 ;

--
-- Dump dei dati per la tabella `tamautocategorydetails`
--

INSERT INTO `tamautocategorydetails` (`id`, `category`, `idCat`, `lang`, `backgroundImage`, `backgroundColor`, `description`, `keywords`, `textColor`) VALUES
(1, 'Home', 1, 1, 'testata-5.jpg', '', '', '', ''),
(2, 'Chi Siamo', 2, 1, 'testata-1.jpg', '', '', '', ''),
(3, 'La Storia', 3, 1, 'testata-2.jpg', '', '', '', ''),
(4, 'Dove Siamo', 4, 1, '10673_testata-4.jpg', '', '', '', ''),
(5, 'Servizi', 5, 1, 'testata-06.jpg', '', '', '', ''),
(6, 'Gallery', 6, 1, '41672_testata-01.jpg', '', '', '', ''),
(7, 'Rassegna Stampa', 7, 1, '26610_testata-03.jpg', '', '', '', ''),
(8, 'News', 8, 1, '41979_testata-05.jpg', '', '', '', ''),
(9, 'Link', 9, 1, '45494_tamautohome.jpg', '', '', '', ''),
(10, 'Contatti', 10, 1, '15453_testata-02.jpg', '', '', '', ''),
(11, 'Vetture', 11, 1, 'testata-04.jpg', '', '', '', ''),
(12, 'Palmares', 12, 1, 'testata-05.jpg', '', '', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautocategoryimagesize`
--

CREATE TABLE IF NOT EXISTS `tamautocategoryimagesize` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `miniWidth` int(11) DEFAULT '0',
  `miniHeight` int(11) DEFAULT '0',
  `smallWidth` int(11) DEFAULT '160',
  `smallHeight` int(11) DEFAULT '120',
  `mediumWidth` int(11) DEFAULT '380',
  `mediumHeight` int(11) DEFAULT '285',
  `largeWidth` int(11) DEFAULT '640',
  `largeHeight` int(11) DEFAULT '480',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=13 ;

--
-- Dump dei dati per la tabella `tamautocategoryimagesize`
--

INSERT INTO `tamautocategoryimagesize` (`id`, `miniWidth`, `miniHeight`, `smallWidth`, `smallHeight`, `mediumWidth`, `mediumHeight`, `largeWidth`, `largeHeight`) VALUES
(1, 0, 0, 160, 120, 380, 285, 640, 480),
(2, 0, 0, 160, 120, 380, 285, 640, 480),
(3, 0, 0, 160, 120, 380, 285, 640, 480),
(4, 0, 0, 160, 120, 380, 285, 640, 480),
(5, 0, 0, 160, 120, 380, 285, 640, 480),
(6, 0, 0, 160, 120, 380, 285, 640, 480),
(7, 0, 0, 160, 120, 380, 285, 640, 480),
(8, 0, 0, 160, 120, 380, 285, 640, 480),
(9, 0, 0, 160, 120, 380, 285, 640, 480),
(10, 0, 0, 160, 120, 380, 285, 640, 480),
(11, 0, 0, 160, 120, 380, 285, 640, 480),
(12, 0, 0, 160, 120, 380, 285, 640, 480);

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautodocument`
--

CREATE TABLE IF NOT EXISTS `tamautodocument` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item` bigint(20) NOT NULL,
  `name` text,
  `description` text,
  `file` text,
  `position` bigint(20) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautoimages`
--

CREATE TABLE IF NOT EXISTS `tamautoimages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item` bigint(20) NOT NULL,
  `name` text,
  `description` text,
  `file` text,
  `position` bigint(20) NOT NULL DEFAULT '0',
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=179 ;

--
-- Dump dei dati per la tabella `tamautoimages`
--

INSERT INTO `tamautoimages` (`id`, `item`, `name`, `description`, `file`, `position`, `homepage`, `active`, `del`) VALUES
(133, 1, NULL, NULL, 'dcadriatico014mb-43-r.jpg', 0, 0, 1, 0),
(8, 4, NULL, NULL, 'cavallini.jpg', 0, 0, 1, 0),
(9, 4, NULL, NULL, '-img-6649.jpg', 0, 0, 1, 0),
(10, 4, NULL, NULL, '-luciani-roggerone.jpg', 0, 0, 1, 0),
(53, 3, NULL, NULL, '1-papa.jpg', 0, 0, 1, 0),
(13, 8, NULL, NULL, 'logo-massimo-bettiol.jpg', 0, 0, 1, 0),
(14, 9, NULL, NULL, 'logo-aci-csai.jpg', 0, 0, 1, 0),
(16, 16, NULL, NULL, 'marca01-michelini.jpg', 0, 0, 1, 0),
(17, 16, NULL, NULL, 'marca-michelini.jpg', 0, 0, 1, 0),
(21, 13, NULL, NULL, 'racing-dreams-colombini.jpg', 0, 0, 1, 0),
(19, 19, NULL, NULL, 're-marca-trevigiana.jpg', 0, 0, 1, 0),
(20, 15, NULL, NULL, 'ronde-gomitolo-gasparotto.jpg', 0, 0, 1, 0),
(22, 12, NULL, NULL, '30901_ronde-gomitolo-gasparotto.jpg', 0, 0, 1, 0),
(23, 17, NULL, NULL, 'taro-oldrati.jpg', 0, 0, 1, 0),
(24, 17, NULL, NULL, 'taro-1-oldrati.jpg', 0, 0, 1, 0),
(25, 14, NULL, NULL, 'valli-pc-4-oldrati.jpg', 0, 0, 1, 0),
(26, 22, NULL, NULL, '120206-uzzeni-varano-2012.jpg', 0, 0, 1, 0),
(27, 21, NULL, NULL, '120220-re-franciacorta-2012.jpg', 0, 0, 1, 0),
(28, 23, NULL, NULL, '120304-sora-sebino-2012.jpg', 0, 0, 1, 0),
(29, 24, NULL, NULL, 'alex-monza-11.jpg', 0, 0, 1, 0),
(30, 24, NULL, NULL, 'beltrami-monza-11.jpg', 0, 0, 1, 0),
(31, 24, NULL, NULL, 'botta-casentino-11.jpg', 0, 0, 1, 0),
(32, 24, NULL, NULL, 'canella-ronde-ossola.jpg', 0, 0, 1, 0),
(33, 24, NULL, NULL, 'cavallini-monza-11.jpg', 0, 0, 1, 0),
(34, 24, NULL, NULL, 'cinotto-aosta-11.jpg', 0, 0, 1, 0),
(35, 24, NULL, NULL, 'ferrecchi-lanterna-11.jpg', 0, 0, 1, 0),
(36, 24, NULL, NULL, 'gasparotto-esperia-11.jpg', 0, 0, 1, 0),
(37, 24, NULL, NULL, 'locatelli-monza-11.jpg', 0, 0, 1, 0),
(38, 24, NULL, NULL, 'musti-app-reggiano-11.jpg', 0, 0, 1, 0),
(39, 24, NULL, NULL, 'nodari-selvino-11.jpg', 0, 0, 1, 0),
(40, 24, NULL, NULL, 'oldrati-valli-piacentine-11.jpg', 0, 0, 1, 0),
(41, 24, NULL, NULL, 'perico-monza-11.jpg', 0, 0, 1, 0),
(42, 24, NULL, NULL, 'podio-master-wrc-monza-11.jpg', 0, 0, 1, 0),
(43, 24, NULL, NULL, 're-lanterna-11-2.jpg', 0, 0, 1, 0),
(44, 24, NULL, NULL, 're-lanterna-11-3.jpg', 0, 0, 1, 0),
(45, 24, NULL, NULL, 're-lanterna-11.jpg', 0, 0, 1, 0),
(46, 24, NULL, NULL, 're-marca-11-2.jpg', 0, 0, 1, 0),
(47, 24, NULL, NULL, 're-marca-11.jpg', 0, 0, 1, 0),
(48, 24, NULL, NULL, 're-monza-11.jpg', 0, 0, 1, 0),
(49, 24, NULL, NULL, 'silva-aosta-11.jpg', 0, 0, 1, 0),
(50, 24, NULL, NULL, 'sossella-valli-cuneesi-11.jpg', 0, 0, 1, 0),
(51, 24, NULL, NULL, '-mbt6385.jpg', 0, 0, 1, 0),
(142, 1, NULL, NULL, 'd41-8702-th.jpg', -1, 0, 1, 0),
(54, 3, NULL, NULL, '2-cane-porsche.jpg', 0, 0, 1, 0),
(55, 3, NULL, NULL, '3-tambone-targa.jpg', 0, 0, 1, 0),
(56, 3, NULL, NULL, '4-porsche-935.jpg', 0, 0, 1, 0),
(57, 3, NULL, NULL, '5-raineri-037.jpg', 0, 0, 1, 0),
(58, 3, NULL, NULL, '6-cunico-delta.jpg', 0, 0, 1, 0),
(59, 25, NULL, NULL, '120325-ferri-grifo.jpg', 0, 0, 1, 0),
(60, 26, NULL, NULL, '120402-sora-valli.jpg', 0, 0, 1, 0),
(61, 27, NULL, NULL, '995-rally-vrchovina-2012-d69f56101c.jpg', 0, 0, 1, 0),
(62, 28, NULL, NULL, 'alessandro-re.jpg', 0, 0, 1, 0),
(63, 29, NULL, NULL, 'fotoalquati-rallyvalleaosta-re1.jpg', 0, 0, 1, 0),
(64, 29, NULL, NULL, 'kostka-t.-rally-thermica.jpg', 0, 0, 1, 0),
(65, 30, NULL, NULL, 'fotoalquati-rallytaro-sossella.jpg', 0, 0, 1, 0),
(66, 31, NULL, NULL, 'odlozilik3-kopie.jpg', 0, 0, 1, 0),
(67, 32, NULL, NULL, 'fotoalquati-rallylanterna-r.jpg', 0, 0, 1, 0),
(68, 33, NULL, NULL, '555276-376695229055154-1207901381-n.jpg', 0, 0, 1, 0),
(69, 34, NULL, NULL, 'rally-della-marca.jpg', 0, 0, 1, 0),
(70, 35, NULL, NULL, '120623-re-salento.jpg', 0, 0, 1, 0),
(71, 36, NULL, NULL, '120623-comte-bornes.jpg', 0, 0, 1, 0),
(72, 37, NULL, NULL, 'sossella-casentino.jpg', 0, 0, 1, 0),
(73, 38, NULL, NULL, 'ducoli.jpg', 0, 0, 1, 0),
(74, 38, NULL, NULL, 'kostka.jpg', 0, 0, 1, 0),
(145, 58, NULL, NULL, '150411-gulfi-ronde-liburna-terra8-th.jpg', 0, 0, 1, 0),
(77, 24, NULL, NULL, '995-rally-vrchovina-2012-d69f56101c.jpg', 0, 0, 1, 0),
(78, 24, NULL, NULL, '120623-comte-bornes.jpg', 0, 0, 1, 0),
(79, 24, NULL, NULL, '303401-3817612651654-2065506012-n.jpg', 0, 0, 1, 0),
(80, 24, NULL, NULL, '318208-194923210617742-100003001941363-304405-1951251547-n.jpg', 0, 0, 1, 0),
(81, 24, NULL, NULL, '384636-4192673931861-1149011850-n.jpg', 0, 0, 1, 0),
(82, 24, NULL, NULL, '527338-213014622141934-100003001941363-344614-787713167-n.jpg', 0, 0, 1, 0),
(83, 24, NULL, NULL, '544983-10150973601507454-2084797731-n.jpg', 0, 0, 1, 0),
(84, 24, NULL, NULL, 'borsa-12.jpg', 0, 0, 1, 0),
(85, 24, NULL, NULL, '-colombini-bizzocchi2.jpg', 0, 0, 1, 0),
(86, 24, NULL, NULL, 'kostka.jpg', 0, 0, 1, 0),
(87, 24, NULL, NULL, 'lf-27-03-2012.jpg', 0, 0, 1, 0),
(88, 24, NULL, NULL, 'p-assist-lanterna-11.jpg', 0, 0, 1, 0),
(89, 24, NULL, NULL, 'p-assist-v-piac-11.jpg', 0, 0, 1, 0),
(90, 24, NULL, NULL, 'p-assist-vda-11-2.jpg', 0, 0, 1, 0),
(91, 24, NULL, NULL, 'p-assist-vda-11.jpg', 0, 0, 1, 0),
(92, 24, NULL, NULL, 'rallye-de-rouergue-11.jpg', 0, 0, 1, 0),
(93, 24, NULL, NULL, 're-franciacorta-2012.jpg', 0, 0, 1, 0),
(94, 24, NULL, NULL, 'sossella-2.jpg', 0, 0, 1, 0),
(95, 40, NULL, NULL, 'valli-cuneesi.jpg', 0, 0, 1, 0),
(96, 39, NULL, NULL, 'agropa.jpg', 0, 0, 1, 0),
(97, 43, NULL, NULL, '120910-comte-mont-blanc.jpg', 0, 0, 1, 0),
(98, 42, NULL, NULL, '120909-borsa-ronde-gomitolo.jpg', 0, 0, 1, 0),
(99, 41, NULL, NULL, '120903-re-ronde-barelli.jpg', 0, 0, 1, 0),
(100, 44, NULL, NULL, 'kostka-rally-jasiniky-12.jpg', 0, 0, 1, 0),
(101, 44, NULL, NULL, 're-rally-appennino-reggiano-12.jpg', 0, 0, 1, 0),
(102, 45, NULL, NULL, '120930-addis-ronde-terra-sarda.jpg', 0, 0, 1, 0),
(103, 45, NULL, NULL, 'sossella-rally-bassano.jpg', 0, 0, 1, 0),
(104, 46, NULL, NULL, 're-san-remo.jpg', 0, 0, 1, 0),
(105, 48, NULL, NULL, '121104-signor-monte-caio.jpg', 0, 0, 1, 0),
(106, 48, NULL, NULL, '121104-borsa-canavese.jpg', 0, 0, 1, 0),
(107, 49, NULL, NULL, '575232-10151483451737454-1056728556-n.jpg', 0, 0, 1, 0),
(108, 50, NULL, NULL, '26-11-bonanomi.jpg', 0, 0, 1, 0),
(109, 50, NULL, NULL, '26-11-alessandro-re.jpg', 0, 0, 1, 0),
(110, 50, NULL, NULL, '26-11-cavallini.jpg', 0, 0, 1, 0),
(111, 50, NULL, NULL, '26-11-longhi.jpg', 0, 0, 1, 0),
(112, 51, NULL, NULL, '21907-4320119236023-1621044413-n.jpg', 0, 0, 1, 0),
(113, 52, NULL, NULL, 'rzeznik-c4.jpg', 0, 0, 1, 0),
(117, 24, NULL, NULL, '140419-assistenza-lyon-charbo-r.jpg', 0, 0, 1, 0),
(134, 1, NULL, NULL, '140517-gulfi-rally-citta-di-torino-r.jpg', 1, 0, 1, 0),
(120, 24, NULL, NULL, '30696_141012-bresolin-rally-legend-4-r.jpg', 0, 0, 1, 0),
(119, 24, NULL, NULL, '140517-gulfi-rally-citta-di-torino-r.jpg', 0, 0, 1, 0),
(121, 24, NULL, NULL, '141012-tortone-rally-legend-5-r.jpg', 0, 0, 1, 0),
(122, 24, NULL, NULL, 'dcadriatico014mb-30-r.jpg', 0, 0, 1, 0),
(123, 24, NULL, NULL, 'dcadriatico014mb-35-r.jpg', 0, 0, 1, 0),
(124, 24, NULL, NULL, 'dcadriatico014mb-43-r.jpg', 0, 0, 1, 0),
(125, 24, NULL, NULL, 'dcadriatico014mb-61-r.jpg', 0, 0, 1, 0),
(126, 24, NULL, NULL, 'dccasentino014mb-18-r.jpg', 0, 0, 1, 0),
(127, 24, NULL, NULL, 'dccasentino014mb-54-r.jpg', 0, 0, 1, 0),
(128, 24, NULL, NULL, 'dcduevalli014mb-22-r.jpg', 0, 0, 1, 0),
(129, 24, NULL, NULL, 'dcduevalli014mb-47-r.jpg', 0, 0, 1, 0),
(130, 24, NULL, NULL, 'dcmotorshow014mb-13-r.jpg', 0, 0, 1, 0),
(131, 24, NULL, NULL, 'dcmotorshow014mb-41-r.jpg', 0, 0, 1, 0),
(135, 54, NULL, NULL, 'valmerula2015.jpg', 0, 0, 1, 0),
(138, 55, NULL, NULL, 'mbt-9533-th.jpg', -1, 0, 1, 0),
(143, 24, NULL, NULL, 'd41-8702-th.jpg', 0, 0, 1, 0),
(140, 57, NULL, NULL, '150330-tamauto.jpg', 0, 0, 1, 0),
(144, 24, NULL, NULL, 'd41-8495-th.jpg', 0, 0, 1, 0),
(146, 24, NULL, NULL, '150411-gulfi-ronde-liburna-terra8-th.jpg', 0, 0, 1, 0),
(147, 24, NULL, NULL, '150411-gulfi-ronde-liburna-terra2-th.jpg', 0, 0, 1, 0),
(148, 24, NULL, NULL, '150328-manzini-rally-1000-miglia20-th.jpg', 0, 0, 1, 0),
(149, 24, NULL, NULL, '150328-manzini-rally-1000-miglia5-th.jpg', 0, 0, 1, 0),
(150, 59, NULL, NULL, '150419-pavia.jpg', 0, 0, 1, 0),
(168, 63, NULL, NULL, '2015-rally-saint-emilion.png', 0, 0, 1, 0),
(153, 65, NULL, NULL, '3.png', 0, 0, 1, 0),
(169, 71, NULL, NULL, '150711-rassegna-stampa-rally-show-san-marino.jpg', 0, 0, 1, 0),
(155, 24, NULL, NULL, '150516-roche-rally-saint-emilion1.jpg', 0, 0, 1, 0),
(156, 24, NULL, NULL, '150516-roche-rally-saint-emilion3.jpg', 0, 0, 1, 0),
(157, 24, NULL, NULL, '150516-sossella-rally-salento3.jpg', 0, 0, 1, 0),
(158, 24, NULL, NULL, '150516-sossella-rally-salento4.jpg', 0, 0, 1, 0),
(159, 24, NULL, NULL, '150516-sossella-rally-salento7.jpg', 0, 0, 1, 0),
(160, 24, NULL, NULL, '150530-swaanen-ele-rally4.jpg', 0, 0, 1, 0),
(161, 24, NULL, NULL, '150530-swaanen-ele-rally6.jpg', 0, 0, 1, 0),
(162, 24, NULL, NULL, '150530-swaanen-ele-rally10.jpg', 0, 0, 1, 0),
(163, 66, NULL, NULL, '1.jpg', 0, 0, 1, 0),
(166, 68, NULL, NULL, '93608_2.jpg', 0, 0, 1, 0),
(165, 69, NULL, NULL, '3.jpg', 0, 0, 1, 0),
(167, 64, NULL, NULL, '2015-rally-salento.jpg', 0, 0, 1, 0),
(170, 72, NULL, NULL, '150718-rassegna-stampa-rally-casentino.jpg', 0, 0, 1, 0),
(171, 74, NULL, NULL, '150725-rassegna-stampa-rally-vyskov.jpg', 0, 0, 1, 0),
(174, 75, NULL, NULL, '150830-rassegna-stampa-rally-coeur-de-france.jpg', 0, 0, 1, 0),
(173, 76, NULL, NULL, '150912-rassegna-stampa-rally-san-martino.jpg', 0, 0, 1, 0),
(176, 78, NULL, NULL, '151017-rassegna-stampa-rally-aci-como.jpg', 0, 0, 1, 0),
(178, 77, NULL, NULL, '150919-rassegna-stampa-rally-citta-di-torino.jpg', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautoitems`
--

CREATE TABLE IF NOT EXISTS `tamautoitems` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creationDate` date DEFAULT NULL,
  `category` bigint(20) NOT NULL DEFAULT '0',
  `position` bigint(20) NOT NULL DEFAULT '0',
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=79 ;

--
-- Dump dei dati per la tabella `tamautoitems`
--

INSERT INTO `tamautoitems` (`id`, `creationDate`, `category`, `position`, `main`, `del`) VALUES
(1, '2011-03-28', 1, 1, 0, 0),
(2, '2011-03-28', 1, 2, 0, 1),
(3, '2011-03-28', 3, 1, 0, 0),
(4, '2011-03-28', 2, 1, 0, 0),
(5, '2011-03-28', 4, 1, 0, 0),
(6, '2011-03-28', 11, 1, 0, 0),
(7, '2011-03-28', 12, 1, 0, 1),
(8, '2011-04-15', 9, 1, 0, 0),
(9, '2011-04-15', 9, 2, 0, 0),
(10, '2011-01-11', 7, 62, 0, 0),
(11, '2011-03-22', 7, 2, 0, 0),
(12, '2011-03-28', 7, 3, 0, 0),
(13, '2011-04-04', 7, 4, 0, 0),
(14, '2011-04-12', 7, 5, 0, 0),
(15, '2011-05-08', 7, 6, 0, 0),
(16, '2011-05-09', 7, 7, 0, 0),
(17, '2011-05-17', 7, 8, 0, 0),
(18, '2011-05-18', 7, 9, 0, 0),
(19, '2011-05-30', 7, 10, 0, 0),
(20, '2012-02-14', 12, 1, 0, 0),
(21, '2012-02-20', 7, 11, 0, 0),
(22, '2012-02-06', 7, 12, 0, 0),
(23, '2012-03-03', 7, 13, 0, 0),
(24, '2012-03-16', 6, 1, 0, 0),
(25, '2012-03-26', 7, 14, 0, 0),
(26, '2012-04-04', 7, 15, 0, 0),
(27, '2012-04-11', 7, 16, 0, 0),
(28, '2012-04-17', 7, 17, 0, 0),
(29, '2012-05-02', 7, 18, 0, 0),
(30, '2012-05-08', 7, 19, 0, 0),
(31, '2012-05-15', 7, 20, 0, 0),
(32, '2012-05-28', 7, 21, 0, 0),
(33, '2012-06-06', 7, 22, 0, 0),
(34, '2012-06-11', 7, 23, 0, 0),
(35, '2012-06-23', 7, 24, 0, 0),
(36, '2012-06-23', 7, 25, 0, 0),
(37, '2012-07-16', 7, 26, 0, 0),
(38, '2012-07-31', 7, 27, 0, 0),
(39, '2012-08-12', 7, 28, 0, 0),
(40, '2012-08-26', 7, 29, 0, 0),
(41, '2012-09-03', 7, 30, 0, 0),
(42, '2012-09-09', 7, 31, 0, 0),
(43, '2012-09-10', 7, 32, 0, 0),
(44, '2012-09-24', 7, 33, 0, 0),
(45, '2012-10-03', 7, 34, 0, 0),
(46, '2012-10-15', 7, 35, 0, 0),
(47, '2012-10-26', 8, 1, 0, 0),
(48, '2012-11-06', 7, 36, 0, 0),
(49, '2012-11-20', 7, 37, 0, 0),
(50, '2012-11-26', 7, 38, 0, 0),
(51, '2012-12-04', 7, 39, 0, 0),
(52, '2012-12-04', 7, 40, 0, 0),
(53, '2015-03-06', 7, 41, 0, 1),
(54, '2015-03-06', 7, 42, 0, 0),
(55, '2015-03-15', 7, 43, 0, 0),
(56, '2015-03-15', 7, 44, 0, 1),
(57, '2015-03-28', 7, 44, 0, 0),
(58, '2015-04-11', 7, 45, 0, 0),
(59, '2015-05-14', 7, 46, 0, 0),
(60, '2015-05-14', 7, 47, 0, 1),
(61, '2015-05-14', 8, 2, 0, 0),
(62, '2015-05-16', 7, 47, 0, 1),
(63, '2015-05-16', 7, 48, 0, 0),
(64, '2015-05-16', 7, 49, 0, 0),
(65, '2015-06-17', 7, 50, 0, 0),
(66, '2015-06-06', 7, 51, 0, 0),
(67, '2015-06-14', 7, 52, 0, 1),
(68, '2015-06-14', 7, 53, 0, 0),
(69, '2015-06-20', 7, 54, 0, 0),
(70, '2015-07-11', 7, 55, 0, 1),
(71, '2015-07-11', 7, 56, 0, 0),
(72, '2015-07-18', 7, 57, 0, 0),
(73, '2015-07-18', 7, 58, 0, 1),
(74, '2015-07-25', 7, 58, 0, 0),
(75, '2015-08-29', 7, 59, 0, 0),
(76, '2015-09-12', 7, 60, 0, 0),
(77, '2015-09-19', 7, 61, 0, 0),
(78, '2015-09-19', 7, 1, 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautoitemsdetail`
--

CREATE TABLE IF NOT EXISTS `tamautoitemsdetail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lang` tinyint(4) NOT NULL DEFAULT '1',
  `item` bigint(20) NOT NULL,
  `itemTitle` text,
  `itemAbstract` text,
  `itemText` text,
  `sign` bigint(20) DEFAULT NULL,
  `homepage` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=79 ;

--
-- Dump dei dati per la tabella `tamautoitemsdetail`
--

INSERT INTO `tamautoitemsdetail` (`id`, `lang`, `item`, `itemTitle`, `itemAbstract`, `itemText`, `sign`, `homepage`, `active`, `del`) VALUES
(1, 1, 1, '', '', '<p><img src="../pictures/logo_tamauto_interno.jpg" alt="" /><br /><br />Preparazione auto sportive e da corsa</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n\n<p><strong>Il sito di TAMAUTO &egrave; in fase di aggiornamento. Ci scusiamo per il disagio.</strong></p>', 0, 0, 1, 0),
(2, 1, 2, 'asdasd', 'qweqweqweqweqweqweqwe', '', 0, 0, 1, 0),
(3, 1, 3, 'La Storia', '', '<p>Il nome Tam &ndash; Auto deriva dal turco e significa &ldquo;giusto&rdquo;. Aggettivo che unito al soggetto forma il marchio di una delle pi&ugrave; conosciute aziende di preparazione e gestione di auto da corsa presenti in Italia.<br />Una storia lunga iniziata nel 1973 quando Nuri Kemal decise di affidare a Peppino Zonca, ex capo officina di Virgilio Conrero e allora capo officina di Bonomelli a Brescia, le sue Porsche da pista.<br />Insieme all&rsquo;esperto meccanico fu creata una struttura che si occupava di preparare i bolidi tedeschi.&nbsp; Oggi Kemal ha lasciato le corse per occuparsi dei suoi affari in Turchia, ma nell&rsquo;officina di Romentino continuano a nascere vetture da competizione.<br />La tecnica si &egrave; affinata le mitiche Porsche hanno lasciato il posto ad altre vetture ma le fotografie appese alle pareti raccontano ancora un pezzo di storia dai contorni esaltanti.<br />Dalla prima vittoria nel Campionato Italiano Velocit&agrave; nel 1975 con la Porsche 911 di Giorgio Schoen all&rsquo;esperienza Americana con Gianpiero Moretti (n. 2 campionati IMSA) affiancato da nomi mitici dell&rsquo;automobilismo quali Danny Hongains, Hurley Haywood, Jacky Ickx, Arturo Merzario, Vittorio Brambilla, ai primi successi rallystici nell&rsquo;assoluto ottenuti nel 1978 e &lsquo;79, sempre con la vettura tedesca, grazie a Tito Cane e Carlo Cuccirelli. Per non parlare della vittoria al Giro d&rsquo;Italia del &rsquo;79 con Moretti-Schoen-Radaelli su Porsche 935. Fino all&rsquo;epopea straordinaria delle Lancia Rally 037 con le quali personaggi del calibro di Gianfranco Cunico, Enrico Bertone, Michele Rayneri, e Gian Marino Zenere seppero portare la struttura novarese ad importanti affermazioni (Campionato Assoluto Rally 1987, Trofeo Centro Sud 1986, Campionato Assoluto Rally Gruppo N 1987, Campionato Triveneto 1986) e decine di vittorie nei rally Nazionali e Internazionali dove spiccano le 7 edizioni vinte del rally di Monza.<br />A partire dagli anni 90 arrivando fino ai giorni nostri la politica dell&rsquo;azienda &egrave; cambiata come sono cambiate le vetture e i regolamenti e in generale il mondo dei rally, preparando vetture destinate ad una clientela sempre pi&ugrave; esigente chi con obiettivi di campionato e chi semplicemente per diletto.<br />Partendo dalla Ford Sierra di gruppo N passando per la mitica BMW M3 gr. A&nbsp; e la gloriosa Lancia Delta Hf gruppo A per arrivare alle varie versioni della Ford Escort Cosworth gruppo N, A e WRC seguono le Toyota Corolla WRC fino alle pi&ugrave; aggiornate e performanti Wrc e S2000 a disposizione: Peugeot 206 Wrc, 307 Wrc, 207 S2000 per arrivare ad oggi con le ultravittoriose Citroen XSara Wrc e C4 Wrc.<br /><br /><strong>VETTURE</strong><br />&bull; Dal 1973 al 1985 Porsche gr. 3 / 4 / B.<br />&bull; Dal 1985 al 1987 Lancia Rally 037 gr. B, Fiat Uno trofeo.<br />&bull; Dal 1986 al 1989 Lancia Delta 4wd / 8v. / 16v. gr. N / A<br />&bull; Dal 1988 al 1991 Ford Sierra Cosw. 2wd. / 4wd gr. N / A.<br />&bull; Dal 1991 al 1996 Bmw M3 gr. A,<br />&bull; Dal 1994 al 1999 Lancia Delta HF gr. A.<br />&bull; Dal 1994 al 2002 Ford Escort Cosworth gr. N / A / WRC.<br />&bull; Dal 2002 al 2004 Mitsubishi Lancer Evo VII gr. N<br />&bull; Dal 2003 al 2006 Toyota Corolla WRC<br />&bull; Dal 2006 Peugeot&nbsp; 206 e 307 WRC<br />&bull; Dal 2008 Peugeot 207 S2000<br />&bull; Dal 2009 Citroen XSara WRC<br />&bull; Dal 2010 Citroen C4 WRC<br /><br /></p>', 0, 0, 1, 0),
(20, 1, 20, 'Palmares', '', '<p><br />&bull; 1975 - 1&deg; Assoluto Campionato Italiano Velocit&agrave; &ndash; Porsche &ndash; Pilota G. Shoen<br />&bull; 1978 - 1&deg; Assoluto Trofeo Rally Nazionali &ndash; Porsche &ndash; Pilota G. Tinivella<br />&bull; 1979 - 1&deg; Assoluto Trofeo Rally Nazionali &ndash; Porsche &ndash; Pilota C. Cuccirelli<br />&bull; 1979 - 3&deg; Assoluto Campionato IMSA &ndash; Porsche &ndash; Pilota G. Moretti<br />&bull; 1986 - 1&deg; Assoluto Trofeo Rally Centro Sud &ndash; Lancia &ndash; Pilota M. Trombi<br />&bull; 1986 - 1&deg; Assoluto Campionato Rally Triveneto &ndash; Lancia &ndash; Pilota G. Zenere<br />&bull; 1987 - 1&deg; Assoluto Campionato Italiano Rally &ndash; Lancia &ndash; Pilota M. Ranieri<br />&bull; 1987 - 1&deg; Assoluto Campionato Italiano Rally Gr. N &ndash; Lancia- Pilota Gf. Cunico<br />&bull; 2003 - 3&deg; Assoluto I.R.C. &ndash; Ford Escort Wrc &ndash; Pilota M. Silva<br />&bull; 2004 - 1&deg; Assoluto I.R.C. &ndash; Toyota Corolla Wrc &ndash; Pilota M. Zanchi<br />&bull; 2005 - 2&deg; Assoluto I.R.C. &ndash; Toyota Corolla Wrc &ndash; Pilota M. Zanchi<br />&bull; 2005 - 3&deg; Assoluto I.R.C. &ndash; Toyota Corolla Wrc &ndash; Pilota T. Gimondi<br />&bull; 2006 - 2&deg; Assoluto T.R.A. &ndash; Peugeot 206 Wrc &ndash; Pilota M. Silva<br />&bull; 2006 - 3&deg; Assoluto I.R.C. &ndash; Peugeot 206 Wrc &ndash; Piolta T. Gimondi<br />&bull; 2007 - 1&deg; Assoluto T.R.A. &ndash; Peugeot 307 Wrc &ndash; Pilota M. Silva<br />&bull; 2007 - 1&deg; Assoluto I.R.C. &ndash; Peugeot 206 Wrc &ndash; Pilota S. Luciani<br />&bull; 2008 - 2&deg; Assoluto T.R.A. &ndash; Peugeot 307 Wrc &ndash; Pilota M. Silva<br />&bull; 2008 - 2&deg; Assoluto C.I.R. Indipendenti &ndash; Peugeot 207 &ndash; Pilota T. Cavallini<br />&bull; 2009 - 2&deg; Assoluto T.R.A. &ndash; Citroen XSara Wrc &ndash; Pilota F. Re<br />&bull; 2010 - 1&deg; Asooluto I.R.S. &ndash; Citroen XSara Wrc &ndash; Pilota M. Gasparotto<br />&bull; 2010 - 2&deg; Assoluto T.R.A. &ndash; Citroen XSara Wrc &ndash; Pilota F. Re<br />&bull; 2010 - 2&deg; Assoluto I.R.C. &ndash; Citroen XSara Wrc &ndash; Pilota D. Oldrati<br />&bull; 2010 - 3&deg; Assoluto Campionato Francese Rally &ndash; Pilota P. Roche<br />&bull; 2011 - 1&deg; assoluto I.R.C. Citroen C4 Wrc - Pilota M. Sossella<br />&bull; 2011 - 2&deg; Assoluto I.R.C. Citroen C4 Wrc - Pilota D. Oldrati<br />&bull; 2011 - 2&deg; Assoluto Campionato Francese Rally Peugeot 307 Wrc - Pilota P. Roche<br />&bull; 2011 - 1&deg; Assoluto O.R.S. Citroen C4 Wrc Pilota - M. Gasparotto<br />&bull; 2011 - 1&deg; Assoluto Campionato Sardegna Citroen C4 Wrc - Pilota C. Addis<br />&bull; 1988 &ndash; 2011 &nbsp; 1&deg; Assoluto Wrc / A / N in 144&nbsp; Rally<br />&bull; 2012 -&nbsp;1&deg; Assoluto T.R.A. Citroen C4 pilota: F. Re<br />&bull; 2012 -&nbsp;1&deg; Assoluto I.R.C. Citroen C4 pilota M. Sossella<br />&bull; 2012 -&nbsp;1&deg; Assoluto Czeck Sprintrally Citroen C4 pilota T. Kostka</p>', 0, 0, 1, 0),
(4, 1, 4, 'Chi Siamo', '', '<p>Preparazione e Noleggio autovetture rally<br /><br /><strong>TT srl</strong><br />Tangenziale Sud/Est, 2/A<br />28068 Romentino (No)<br />Tel./Fax 0321.867296<br />E-mail: <a href=\\"mailto:info@tam-auto.it\\">info@tam-auto.it</a></p>', 0, 0, 1, 0),
(5, 1, 5, 'Dove Siamo', '', '<p><strong>TT srl</strong><br />Tangenziale Sud/Est, 2/A<br />28068 Romentino (No)<br /><br />Telefono/Fax:<br />0321/867296<br /><br />E-mail:<br /><a href=\\"mailto:tamauto@inwind.it\\">tamauto@inwind.it</a><br /><a href=\\"mailto:info@tam-auto.it\\">info@tam-auto.it</a><br /><br />\r\n<iframe width="600" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.it/maps/ms?hl=it&amp;ie=UTF8&amp;t=h&amp;msa=0&amp;msid=200595228134829672851.00049f8c037a9b3166f40&amp;ll=45.471327,8.731384&amp;spn=0.048152,0.094242&amp;z=13&amp;iwloc=00049f8c0c39e374455b2&amp;output=embed"></iframe><br /><small>Visualizza <a href="http://maps.google.it/maps/ms?hl=it&amp;ie=UTF8&amp;t=h&amp;msa=0&amp;msid=200595228134829672851.00049f8c037a9b3166f40&amp;ll=45.471327,8.731384&amp;spn=0.048152,0.094242&amp;z=13&amp;iwloc=00049f8c0c39e374455b2&amp;source=embed" style="color:#0000FF;text-align:left">TT srl</a> in una mappa di dimensioni maggiori</small>\r\n</p>', 0, 0, 1, 0),
(6, 1, 6, 'Vetture', '', '<p><strong>FORD FIESTA WRC / RRC</strong></p>\n<p><strong>FORD FIESTA R5</strong></p>\n<p><strong>PEUGEOT 307 WRC</strong></p>', 0, 0, 1, 0),
(8, 1, 8, 'Massimobettiol.com', 'http://www.massimobettiol.com', '<p>Massimo Bettiol - Fotografo</p>', 0, 0, 1, 0),
(9, 1, 9, 'Csai.aci.it', 'http://www.csai.aci.it/', '<p>CSAI<br />Commissione Sportiva Automobilistica Italiana</p>', 0, 0, 1, 0),
(10, 1, 10, 'Presentazione Stagione', '', '<p>In Tam Auto &egrave; tutto pronto. Definiti gli ultimi dettagli la stagione 2011 si presenta ai nastri di partenza con il team novarese pronto a recitare il ruolo di assoluto protagonista.<br />Nel Trofeo Rally Asfalto girone A che prender&agrave; il via con il Rally della Valle D&rsquo;Aosta a fine aprile per poi continuare con il Lanterna e il Proserpina, si rinnova una delle sfide tradizionali di questa serie fra i lombardi Felice Re&nbsp; e Mara Bariani i quali disporranno di una Citroen C4 Wrc e Marco Silva che affronter&agrave; la stagione 2011 insieme a Giuseppe Pina a bordo di una Xara Wrc. Insieme a loro nello stesso girone sar&agrave; presente Luca Gulfi sulla seconda Xara Wrc. Il girone B di cui fanno parte il Rally della Marca, l&rsquo;Appennino Reggiano e il Proserpina, vedr&agrave; impegnato il pavese Matteo Musti sulla Peugeot 307 Wrc. <br />Tam Auto sar&agrave; presente anche nel IRC dove Devid Oldrati cercher&agrave; di portare alla vittoria la Citroen C4 Wrc mentre nell&rsquo;IRS&nbsp; alla C4 di Massimo Gasparotto verranno affiancate le due Xara Wrc di Ilario Nodali e Marco Miraglia. Impegni importanti anche nel Trofeo Citroen DS3R dove trofeo che vede il giovane Alessandro Re fra i pi&ugrave; accreditati protagonisti. In questo trofeo &egrave; in via di definizione una seconda vettura. Programmi importanti anche al di fuori dei confini nazionali. Tam- Auto ha infatti imbastito insieme al Team Fj Pierre Roche la partecipazione al Campionato Francese con una Peugeot 307 Wrc. Tornando in Italia, ad un programma come al solito molto nutrito si aggiungono gli impegni in via di pianificazione per il biellese Tiziano Borsa il quale disporr&agrave; della Peugeot 207 Super 2000 appena aggiornata.</p>', 0, 0, 1, 0),
(7, 1, 7, 'Palmares', '', '<p>&bull; 1975 - 1&deg; Assoluto Campionato Italiano Velocit&agrave; &ndash; Porsche &ndash; Pilota G. Shoen<br />&bull; 1978 - 1&deg; Assoluto Trofeo Rally Nazionali &ndash; Porsche &ndash; Pilota G. Tinivella<br />&bull; 1979 - 1&deg; Assoluto Trofeo Rally Nazionali &ndash; Porsche &ndash; Pilota C. Cuccirelli<br />&bull; 1979 - 3&deg; Assoluto Campionato IMSA &ndash; Porsche &ndash; Pilota G. Moretti<br />&bull; 1986 - 1&deg; Assoluto Trofeo Rally Centro Sud &ndash; Lancia &ndash; Pilota M. Trombi<br />&bull; 1986 - 1&deg; Assoluto Campionato Rally Triveneto &ndash; Lancia &ndash; Pilota G. Zenere<br />&bull; 1987 - 1&deg; Assoluto Campionato Italiano Rally &ndash; Lancia &ndash; Pilota M. Ranieri<br />&bull; 1987 - 1&deg; Assoluto Campionato Italiano Rally Gr. N &ndash; Lancia- Pilota Gf. Cunico <br />&bull; 2003 - 3&deg; Assoluto I.R.C. &ndash; Ford Escort Wrc &ndash; Pilota M. Silva <br />&bull; 2004 - 1&deg; Assoluto I.R.C. &ndash; Toyota Corolla Wrc &ndash; Pilota M. Zanchi<br />&bull; 2005 - 2&deg; Assoluto I.R.C. &ndash; Toyota Corolla Wrc &ndash; Pilota M. Zanchi<br />&bull; 2005 - 3&deg; Assoluto I.R.C. &ndash; Toyota Corolla Wrc &ndash; Pilota T. Gimondi<br />&bull; 2006 - 2&deg; Assoluto T.R.A. &ndash; Peugeot 206 Wrc &ndash; Pilota M. Silva<br />&bull; 2006 - 3&deg; Assoluto I.R.C. &ndash; Peugeot 206 Wrc &ndash; Piolta T. Gimondi<br />&bull; 2007 - 1&deg; Assoluto T.R.A. &ndash; Peugeot 307 Wrc &ndash; Pilota M. Silva<br />&bull; 2007 - 1&deg; Assoluto I.R.C. &ndash; Peugeot 206 Wrc &ndash; Pilota S. Luciani<br />&bull; 2008 - 2&deg; Assoluto T.R.A. &ndash; Peugeot 307 Wrc &ndash; Pilota M. Silva<br />&bull; 2008 - 2&deg; Assoluto C.I.R. Indipendenti &ndash; Peugeot 207 &ndash; Pilota T. Cavallini<br />&bull; 2009 - 2&deg; Assoluto T.R.A. &ndash; Citroen XSara Wrc &ndash; Pilota F. Re<br />&bull; 2010 - 1&deg; Asooluto I.R.S. &ndash; Citroen XSara Wrc &ndash; Pilota M. Gasparotto<br />&bull; 2010 - 2&deg; Assoluto T.R.A. &ndash; Citroen XSara Wrc &ndash; Pilota F. Re<br />&bull; 2010 - 2&deg; Assoluto I.R.C. &ndash; Citroen XSara Wrc &ndash; Pilota D. Oldrati<br />&bull; 2010 - 3&deg; Assoluto Campionato Francese Rally &ndash; Pilota P. Roche<br />&bull; 1988 &ndash; 2010&nbsp;&nbsp; 1&deg; Assoluto Wrc / A / N in 125&nbsp; Rall</p>', 0, 0, 1, 0),
(11, 1, 11, 'FABIO ANGELUCCI E MASSIMO CAMBRIA VINCONO LA RONDE DI GIOIOSA MAREA', '', '<p><span style=\\"text-decoration: underline;\\">FABIO ANGELUCCI E MASSIMO CAMBRIA VINCONO LA RONDE DI GIOIOSA MAREA</span><br /><span style=\\"text-decoration: underline;\\">L&rsquo;Equipaggio della Turbomark si &egrave; imposto sulla&nbsp; Peugeot 206 Wrc dopo l&rsquo;esclusione di Patti </span><br /><span style=\\"text-decoration: underline;\\">Al Rally Touquet, prima prova del campionato francese, 4&deg; posto assoluto di Roche sulla Peugeot 307</span><br /><br />Inizia sotto i migliori auspici la stagione 2011 per il team novarese. Il Romano Fabio Angelucci navigato dall&rsquo;esperto Massimo Cambria in gara sulla Peugeot 206 Wrc si &egrave; imposto nella prima edizione della Ronde di Gioiosa Marea che si &egrave; svolta in provincia di Messina lo scorso Week-end. Il portacolori del Team Turbomark &egrave; stato il pi&ugrave; veloce in due delle quattro prove concludendo i tratti cronometrati al secondo posto assoluto. La vittoria veniva assegnata dopo l&rsquo;esclusione dalla classifica finale della Peugeot 207 Super 2000 di Giuseppe Patti e Gabriele Monreale trovata sotto peso durante le verifiche post gara. Angelucci aveva iniziato nel migliore dei modi vincendo, nonostante la pioggia, il parziale d&rsquo;apertura grazie ad una azzeccata scelta dei pneumatici. Una scelta che si &egrave; rivelata troppo &ldquo;prudente&rdquo; nella parte centrale della gara dove Patti era riuscito a recuperare. L&rsquo;attacco finale aveva permesso ad Angelucci di consolidare la seconda posizione. &ldquo;Siamo molto soddisfatti&rdquo; ha commentato il driver romano a fine gara. &ldquo;La scelta dei pneumatici &egrave; stata nel bene e nel male determinante. Sulla prima prova affrontata sotto la pioggia abbiamo azzeccato tutto mentre probabilmente avremo potuto montare degli intermedi nelle due prove successive. Le regolazioni ci hanno poi consentito di vincere il parziale finale&rdquo;.<br />Nello stesso week-end Tam &ndash;Auto &egrave; stata impegnata anche in Francia dove ha partecipato con Pierre e Martine Roche al Rally Le Touquet gara d&rsquo;apertura del campionato francese. L&rsquo;equipaggio transalpino in gara sulla Peugeot 307 Wrc ha concluso al 4&deg; posto assoluto. Commenta con un certo rammarico Pierre Roche. &ldquo;In chiave campionato questo &egrave; senza dubbio un ottimo risultato. Peccato perch&eacute; il secondo posto assoluto era alla nostra portata fino a quando una foratura ci ha fatto perdere due posizioni. Siamo tuttavia molto soddisfatti sia della vettura sia di Tam Auto, un team davvero professionale&rdquo;. Nel prossimo Week-end Tam &ndash; Auto sar&agrave; impegnata alla &ldquo;3&deg; Ronde del Grifo&rdquo; di Arzignano alla quale schierer&agrave; un terzetto formato da&nbsp; Massimo Gasparotto&nbsp; e Renato Bizzotto . sulla Citroen C4 . Ilario Nodari e Davide Benigno ai quali verr&agrave; affidata la Citroen Xara Wrc mentre con la Xara gemella saranno al via Miraglia - Colombo&nbsp;</p>', 0, 0, 1, 0),
(12, 1, 12, 'MASSIMO GASPAROTTO E RENATO BIZZOTTO SECONDI ASSOLUTI AD ARZIGNANO ', '', '<p><span style=\\"text-decoration: underline;\\">MASSIMO GASPAROTTO E RENATO BIZZOTTO SECONDI ASSOLUTI AD ARZIGNANO </span><br /><span style=\\"text-decoration: underline;\\">Il pilota&nbsp; Citroen non riesce ad approfittare del ritiro di Battaglin. Terzo assoluto Nodari- Miraglia 15&deg;</span><br /><span style=\\"text-decoration: underline;\\">Nel prossimo week-end terzetto Tam-Auto alla Ronde del Canavese con Silva, Colombini e Borsa</span><br /><br />Un intera giornata sulle tracce di Alessandro Battaglin che fin dalle prime battute si era istallato davanti alla sua Citroen Xara wrc. E poi, quando il ritiro del vicentino sembrava aver aperto la strada per la vittoria, a Massimo Gasparotto al via con Renato Bazzotto sulla C4 della Tam- Auto non &egrave; riuscito il colpaccio finale e sulla prova che ha chiuso la gara si sono visti superare dalla Ford Focus di Luca Ferri&nbsp; e Massimo Daddoveri. &ldquo;Siamo partiti bene&rdquo; ha commentato il driver di Bassano. &ldquo;Anche se Battaglin ha imposto un ritmo molto alto. Sull&rsquo;ultima prova quando abbiamo raggiunto Battaglin abbiamo rallentato per renderci conto che fosse tutto ok perdendo qualche secondo. Ferri ci ha sorpresi e alla fine i 9 decimi che ci separano sono stati determinanti:&rdquo; Molto soddisfatti&nbsp; Ilario Nodari e Davide Benigno al via sulla Citroen Xara Wrc con la quale hanno concluso al terzo posto assoluto. &ldquo;Sono davvero molto soddisfatto&rdquo; racconta Nodari sulla pedana d&rsquo;arrivo. &ldquo;Siamo stati costantemente a ridosso del podio e con lo sfortunato ritiro di Battaglin abbiamo raggiunto un risultato senza dubbio ottimo&rdquo;. Soddisfatti anche Miraglia &ndash; Colombo in gara sulla seconda Citroen Xara&nbsp; con la quale hanno chiuso al 15&deg; posto assoluto. Altro week-end molto &ldquo;caldo&rdquo; quello che attende il Team novarese. La prossima domenica si corre nel Canavese per la 6&deg; edizione della Ronde promossa dalla Rally Team, gara dove la Tam-Auto schierer&agrave; un attacco a tre punte con il comasco Silva al via sulla Citroen Xara, il Sanmarinese Colombini sulla Citroen C4 mentre nella classe Super 2000 occhi puntati sul biellese Borsa al via sulla Peugeot 207.</p>', 0, 0, 1, 0),
(13, 1, 13, 'DENIS COLOMBINI E MASSIMO BIZZOCCHI VINCONO LA RONDE DEL CANAVESE', '', '<p><strong>DENIS COLOMBINI E MASSIMO BIZZOCCHI VINCONO LA RONDE DEL CANAVESE</strong><br /><span style=\\"text-decoration: underline;\\">Il sanmarinese impone il suo ritmo gi&agrave; dalle prime battute e porta al successo la Citroen C4</span><br /><span style=\\"text-decoration: underline;\\">Marco Silva e Giovanni Pina chiudono al terzo posto assoluto sulla Citroen Xara</span><br /><span style=\\"text-decoration: underline;\\">Tiziano Borsa e Carla Berra dominano nella classe Super 2000 con la Peugeot 207 e sono 6&deg; assoluti</span><br /><br />Successo pieno per gli uomini di Luca Zonca alla sesta edizione della Ronde del Canavese. Il sanmarinese Denis Colombini in gara con Massimo Mazzocchi ha portato alla vittoria la Citroen C4 Wrc vincendo tre dei quattro passaggi in programma. Colombini era all&rsquo;esordio sulla vettura francese ed ha saputo dimostrare da subito un ottimo feeling lasciando a Sora il successo sul terzo passaggio. &ldquo;Siamo molto soddisfatti&rdquo; ha dichiarato Colombini all&rsquo;arrivo. &ldquo;Abbiamo approfittato di questa gara per provare la C4 con la quale ci siamo trovati a meraviglia: Valuteremo nelle prossime settimane di imbastire i nostri impegni per il 2011&rdquo;. Il successo Tam-Auto &egrave; stato completato dal terzo posto assoluto dei comaschi Marco Silva e Giovanni Pina anche loro all&rsquo;esordio sulla Citroen Xara che con la quale saranno al via del Trofeo Rally Asfalto. &ldquo;Questa gara ci &egrave; servita per provare meglio la Xara in vista del primo impegno ad Aosta. Abbiamo lavorato in modo particolare sull&rsquo;assetto trovando il giusto sep-up. La vettura mi &egrave; piaciuta molto e credo che faremo bene&rdquo;. Sesto posto assoluto e vittoria nella classe Super 2000 per Tiziano Borsa e Carla Berra, gli unici in grado di avvicinare le performanti wrc. &ldquo;Eravamo convinti di far bene cercando di &ldquo;stuzzicare le wrc&rdquo; Ha dichiarato Borsa. &ldquo;Ovviamente il divario tecnico si &egrave; fatto sentire ma sono molto soddisfatto di come la 207 &egrave; stata migliorata nel periodo invernale&rdquo;</p>', 0, 0, 1, 0),
(14, 1, 14, 'DAVID OLDRATI E GUIDO D’AMORE TERZI ASSOLUTI  SULLA CITROEN C4AL VALLI PIACENTINE', '', '<p><strong>DAVID OLDRATI E GUIDO D&rsquo;AMORE TERZI ASSOLUTI&nbsp; SULLA CITROEN C4AL VALLI PIACENTINE</strong><br /><span style=\\"text-decoration: underline;\\">Buono il ritorno di Roberto&nbsp; Botta e Piecarlo Capolongo 4&deg; assoluti nonostante qualche problema</span><br /><br />Dopo il successo ottenuto una settimana prima alla Ronde del Canavese, Tam-Auto si &egrave; presentata al via del 24&deg; Rally delle Valli Piacentine prova d&rsquo;apertura della serie Irc Pirelli 2011. Due gli equipaggi schierati dal team novarese dopo il forfait di Matteo Musti dovuto a problemi di salute. David Oldrati e Guido D&rsquo;Amore hanno concluso in terza posizione assoluta a bordo della Citroen C4 con la quale hanno saputo dimostrare una certa incisivit&agrave; soprattutto nella seconda parte di gara dove il driver bergamasco ha trovato il giusto feeling migliorando il set-up della vettura. Ottimo 4&deg; posto assoluto per il piemontese Roberto Botta&nbsp; al via con Piercarlo Capolongo sulla Xara. Al rientro dopo oltre un anno e mezzo di stop, Botta &egrave; stato protagonista di una prima parte di gara ad alto livello dove ha fatto segnare il miglior tempo sulla seconda prova. Prestazione che il driver cuneese non &egrave; riuscito a ripetere nelle fasi finali a causa di un problema al circuito di raffreddamento dell&rsquo;aria che ha mandato in protezione il motore della Citroen dovendo lasciare il passo ad Oldrati a due prove dalla fine.</p>', 0, 0, 1, 0),
(15, 1, 15, 'BIS TAM-AUTO. GASPAROTTO VINCE A BIELLA- FERRECCHI LO IMITA A MILLESIMO', '', '<p><strong>BIS TAM-AUTO. GASPAROTTO VINCE A BIELLA- FERRECCHI LO IMITA A MILLESIMO</strong><br /><span style=\\"text-decoration: underline;\\">Le Citroen C4 ancora vincenti. Sia Gasparotto che Ferrecchi non lasciano spazio agli avversari</span><br /><span style=\\"text-decoration: underline;\\">Tiziano Borsa e Carla Berra al 4&deg; posto assoluto la Peugeot 207 S.2000 nella Ronde del Gomitolo</span><br /><br />Il pronostico parlava chiaro. Sia a Biella che a Millesimo le Citroen C4 Wrc erano le osservate speciali. Pronta la conferma arrivata dagli asfalti biellesi dove il vicentino Massimo Gasparotto al via insieme a Renato Bazzotto ha portato la C4 novarese al successo vincendo tutte e quattro i passaggi sulla insidiosa &ldquo;Romanina&rdquo; resa ancora pi&ugrave; difficile da un improvviso acquazzone nel corso del terzo passaggio. Soddisfatto Gasparotto. &ldquo;Una C4 diversa come conformazione rispetto a quella che avevo usato ad Arzignano. Dopo la prima prova abbiamo fatto alcune modifiche all&rsquo;assetto e mi sono trovato decisamente meglio. Sono particolarmente felice di aver&nbsp; vinto anche perch&eacute; lo abbiamo fatto contro una concorrenza particolarmente agguerrita. Al 4&deg; posto assoluto nell&rsquo;appuntamento laniero chiudono Tiziano Borsa e Carla Berra al via sulla Peugeot 207. &ldquo;Per noi era importante scrollarci di dosso il ritiro dello scorso anno&rdquo; commenta il pilota di Crevacuore . &ldquo;Abbiamo faticato parecchio con le gomme che rendevano la macchina particolarmente scivolosa. Aravamo partiti per cercare di vincere la classe ma oggi Bocchio,il nostro avversario principale &egrave; andato davvero forte&rdquo; <br />Si chiude con una vittoria anche il &ldquo;Bormida 30&rdquo; la Ronde 2 che ha visto il debutto vincente di Maurizio&nbsp; Ferrecchi e Fulvio Florean sulla Citreon C4. Anche in questo caso quattro su quattro i successi parziali che hanno permesso al driver&nbsp; della Lanterna Corse di vincere per la decima volta l&rsquo;appuntamento di casa. &ldquo;Abbiamo avuto un piccolo problema sul secondo passaggio ma per il resto &egrave; filato tutto liscio&rdquo; Sono le parole di Ferrecchi all&rsquo;arrivo. &ldquo;La C4 &egrave; davvero una vettura stupenda. Molto tecnica e professionale che in questo momento penso non abbia rivali nella sua categoria&rdquo;.<br />Un\\''altra settimana impegnativa quella che si profila per il team di Luca Zonca. Al Rally del Taro secondo appuntamento dell&rsquo;IRC Pirelli saranno al via Roberto Botta con la Citroen Xara e David Oldrati sulla&nbsp; C4</p>', 0, 0, 1, 0),
(16, 1, 16, 'RUDY MICHELINI E MICHELE PERNA CON LA C4 TAM-AUTO NEL TRA GIRONE B', '', '<p><strong>RUDY MICHELINI E MICHELE PERNA CON LA C4 TAM-AUTO NEL TRA GIRONE B</strong><br /><br />L&rsquo;accordo &egrave; stato siglato in questi giorni. Il veloce pilota toscano, vincitore del Rally del Carnevale, sar&agrave; al via dei tre appuntamenti del girone B del Trofeo Rally Asfalto ovvero Rally della Marca, Rally Appennino Reggiano e Proserpina a bordo della Citroen C4 Pirelli. Al suo fianco sieder&agrave; Michele Perna. &ldquo;Sono particolarmente soddisfatto&rdquo; ha dichiarato il pilota della Movisport. &ldquo;Abbiamo pianificato l&rsquo;impegno nel Trofeo Rally Asfalto girone B con l&rsquo;intenzione di far bene. La C4 &egrave; una vettura magnifica. Non vedo l&rsquo;ora di iniziare&rdquo;.</p>', 0, 0, 1, 0),
(17, 1, 17, 'SPLENDIDO BIS AL RALLY INTERNAZIONE DEL TARO - OLDRATI E BOTTA: CHE SPETTACOLO!', '', '<p><strong>SPLENDIDO BIS AL RALLY INTERNAZIONE DEL TARO</strong><br /><strong>OLDRATI E BOTTA: CHE SPETTACOLO!</strong><br /><span style=\\"text-decoration: underline;\\">I due alfieri Tam-Auto rispettivamente 1&deg; e 2&deg; staccati di 2 decimi.- L&rsquo;IRC Pirelli si fa interessante</span><br /><br />Una vittoria nitida, inequivocabile. La logica conseguenza di una prestazione maiuscola dove tutto ha funzionato alla perfezione. David Oldrati e Guido D&rsquo;Amore&nbsp; tornano dalla trasferta parmense., secondo atto dell&rsquo; IRC Pirelli 2011 con un successo importantissimo nell&rsquo;economia di una stagione ancora lunga e difficile. La Citroen C4 ha viaggiato alla perfezione permettendo all&rsquo;equipaggio della Giesse Promotion di prendere la testa della classifica dopo la terza prova. &ldquo;La grossa incognita di questa gara erano le condizioni del tempo&rdquo; afferma Oldrati sulla pedana d&rsquo;arrivo. &ldquo;Questo alternarsi di pioggia e asciutto ci hanno messo in grossa apprensione. Per questo motivo &egrave; stato preziosissimo il lavoro dei ricognitori (per Oldrati Sandro Sottile mentre per Botta era Patrizia Sciascia ndr) che ci ha permesso di partire con le stampo quando ancora pioveva. Una scelta che si &egrave; rivelata vincente.&rdquo; A pochi passi da lui un sorridente&nbsp; Roberto Botta al via con Piercarlo Capolongo sulla Citroen Xara. &ldquo;Gran bella gara &ldquo; racconta il driver della scuderia Provincia Granda. &ldquo;Ad un certo punto ci abbiamo provato e nelle fasi centrali della gara eravamo passati in testa . Poi sul secondo passaggio sulla lunghissima Folta Ondrati ci ha dato 4&rdquo;6 ed &egrave; passato a condurre. Nonostante ci&ograve; abbiamo continuato ad attaccare e sull&rsquo;ultima prova abbiamo recuperato 2&rdquo;6.&nbsp; Mancavano solo due decimi ed era fatta. Peccato anche se siamo molto soddisfatti&rdquo; Prossimo Appuntamento il Rally del Casentino in programma il 1&deg; e 2 Luglio.</p>', 0, 0, 1, 0),
(18, 1, 18, 'LUCA GULFI AL CITTA’ DI TORINO PER CONOSCERE LA PEUGEOT 207 S.2000', '', '<p><strong>LUCA GULFI AL CITTA&rsquo; DI TORINO PER CONOSCERE LA PEUGEOT 207 S.2000</strong><br /><br />Avrebbe dovuto essere il via ad Aosta al quale ha rinunciato per un problema tecnico. Luca Gulfi approfitter&agrave; di questo imprevisto per essere al via del Rally Citt&agrave; di Torino in programma il 20 e 21 Maggio prossimi. Il driver torinese della scuderia Zerosette Racing guider&agrave; per la prima volta la Peugeot 207 Super 2000. &ldquo;Una vettura che dovr&ograve; conoscere e che tecnicamente &egrave; assai diversa dalle wrc che guido abitualmente. Cercheremo di entrare subito in&nbsp; sintonia per stare nel gruppo che conta della classifica assoluta&rdquo;. Gulfi sar&agrave; al via con il ligure Danilo Roggerone.</p>', 0, 0, 1, 0),
(19, 1, 19, 'FELICE RE E MARA BARIANI - CITROEN C4 VINCONO IL 28° RALLY DELLA MARCA', '', '<p><strong>FELICE RE E MARA BARIANI - CITROEN C 4VINCONO IL 28&deg; RALLY DELLA MARCA</strong><br /><span style=\\"text-decoration: underline;\\">Terzi assoluti&nbsp; Rudy Michelini e Michele Perna all&rsquo;esorsdio sulla Citroen C 4 e vincitori di 2 prove</span><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />Le difficili strade del Veneto hanno dato la risposta per altro ampiamente pronosticata alla vigilia Sar&agrave; un\\''altra stagione spettacolare nel Trofeo Rally Asfalto. Felice&nbsp; Re e Mara Bariani si sono aggiudicati la 28&deg; edizione del Rally della Marca, appuntamento inserito nel girone B del Trofeo Asfalto. Il comasco della scuderia Etruria al via sulla Citrine C4 si &egrave; aggiudicato sette delle dieci prove speciali in programma. Un successo ottenuto anche grazie ad una tattica di gara che ha funzionato alla perfezione. &ldquo;Abbiamo senza dubbio vinto una gara difficile&rdquo; ha dichiarato Re sulla pedana d&rsquo;arrivo. &ldquo;Le scelte fatte in collaborazione con i nostri tecnici si sono rivelate subito efficaci e il feeling con la vettura &egrave; stato immediato. Il campionato &egrave; ancora ovviamente molto lungo ma sono particolarmente soddisfatto perch&eacute; da Aosta abbiamo senza dubbio migliorato molto.&rdquo; Terzo posto assoluto per il toscano Rudy Michelini insieme a Michele Perna all&rsquo;esordio sulla Citroen C4 con la quale si sono aggiudicati il crono iniziale e l&rsquo;ultima prova che ha chiuso la gara. Michelini &egrave; stato a lungo l&rsquo;antagonista principale di Re dimostrando di saper gestire ottimamente la difficile Wrc francese. Un secondo posto assoluto sfuggito a causa di una foratura sulla prova speciale otto. Soddisfatto ovviamente il portacolori della Movisport. &ldquo;Non potevamo iniziate meglio&rdquo; ha commentato il pilota lucchese. &ldquo;Nonostante le condizioni difficili siamo partiti subito molto bene azzeccando le regolazioni. Il team &egrave; stato fantastico e mi ha aiutato molto. Sono davvero soddisfatto&rdquo;. La sfida si sposta ora sulle strade ligure del Rally della Lanterna in programma il 12 Giugno.</p>', 0, 0, 1, 0),
(21, 1, 21, 'FELICE RE INIZIA NEL MIGLIORE DEI MODI IL 2012.- VITTORIA CON LA C4', 'SECONDA VITTORIA CONSECUTIVA NEL RALLY CIRCUIT SERIES', '<p>Una prima tappa per riprendere i giusti meccanismi ed entrare nel clima di una gara che lui non aveva mai disputato. Una seconda parte dove le Pirelli hanno funzionato a meraviglia. E&rsquo; racchiusa tutta qui la vittoria di Felice Re e Mara Bariani al Franciacorta 2012 terzo appuntamento del Rally Ciruit Series che si &egrave; svolto nell&rsquo;ultimo week-end all&rsquo;interno dell&rsquo;Autodromo &ldquo;Daniel Bonara&rdquo; di Castrezzato. Il pilota varesino ha concluso la prima tappa al secondo posto ad un battito di ciglia da Spoldi. &ldquo;Non avevo mai corso qui in Franciacorta&rdquo; ha commentato Re al termine . &ldquo;Nella prima parte abbiamo messo a punto alcune cose e nella seconda tappa le Pirelli hanno davvero funzionato a meraviglia permettendoci di migliorare molto le nostre prestazioni. Il team Tam-Auto come al solito &egrave; stato all&rsquo;altezza della situazione. Sono davvero soddisfatto&rdquo;.</p>', 0, 0, 1, 0),
(22, 1, 22, 'FRANCO UZZENI E DANILO FAPPANI PORTANO AL SUCCESSO AL C4 A VARANO', 'INIZIA CON UN SUCCESO IL 2012 PER TAM-AUTO', '<p>Il 2011 si era chiuso con la vittoria di Addis in Sardegna e il secondo posto di Borsa al Colline. E la nuova stagione per il team di Romentino guidato da Luca Zonca sembra essere cominciata sotto i migliori auspici. Sul circuito &ldquo;Riccardo Paletti&rdquo; di Varano novarese Franco Uzzeni ha infatti portato alla vittoria la Citroen C4 . Uzzeni, al via insieme a Danilo Fappani , ha saputo controllare le mosse del lombardo Stefano D&rsquo;Aste, pi&ugrave; veloce con la sua Lotus nella prima parte. La giornata finale, complice anche la nevicata che nella notte ha imbiancato il circuito regalando uno scenario pi&ugrave; nordico, ha ribaltato i valori in campo e messo in evidenza le trazioni integrali. Uzzeni ha preso in mano le redini del confronto&nbsp; che ha saputo tenere fino alla quinta prova dove, a causa di un testa cosa , lasciava il comando al locale Filippo Gennari in gara su una Renault Clio Willaims e la seconda posizione a Manuel Villa in gara con Carlo Canova su una Abarth Grande Punto. Uzzeni ha sferrato l&rsquo;attacco decisivo sull&rsquo;ultima prova. Una prestazione che gli &egrave; valsa la vittoria finale. &ldquo;Sono molto soddisfatto&rdquo; ha commentato Uzzeni al termine. &ldquo;La C4 si &egrave; dimostrato un mezzo impegnativo ma davvero molto performante. Quel testa coda sulla 5&deg; prova ha complicato le cose ma abbiamo saputo reagire immediatamente conquistando la vittoria.&rdquo; Il prossimo appuntamento valido per il Rally Circuit Series &egrave; fissato a Franciacorta il 17 e 18 Febbraio..</p>', 0, 0, 1, 0),
(23, 1, 23, 'RONDE del SEBINO. CLAUDIO SORA al debutto con la Citroen C4 Wrc ', '', '<p>Al di la del risultato finale, 4&deg; assoluto, &egrave; da considerarsi positivo il debutto di Claudio Sora al volante della Citreon C4 Wrc avvenuto alla Ronde del Sebino, gara test in vista degli impegni futuri nel Campionato Nazionale I.R.C.<br />Sora &egrave; risultato il pi&ugrave; veloce su tre delle quattro prove disputate, vincendo le prime due disputate il sabato pomeriggio, purtroppo nel corso della terza prova svoltasi domenica mattina, a causa delle condizioni meteo incerte, leggera pioggia e strada non completamene bagnata ma resa scivolosa, e di una scelta di gomme non ottimale incappava in un testacoda con conseguente spegnimento della vettura e circa 30&rdquo; persi. Con conseguente perdita della leadership della gara, a nulla valeva se non per confermare il giusto feeling raggiunto con l&rsquo; auto, ai fini della classifica finale, la vittoria sulla quarta e ultima prova speciale.</p>', 0, 0, 1, 0),
(24, 1, 24, 'Gallery', '', '', 0, 0, 1, 0),
(25, 1, 25, 'FERRI E DADDOVERI: ESORDIO VINCENTE CON LA C4 ALLA RONDE DEL GRIFO', '', '<p>Esordio convincente e vittoria meritata per Luca Ferri e Massimo Daddoveri sulla Citroen C4 Wrc . Sulle strade insidiose della vallata del Chiampo il pilota Citroen ha dimostrato subito una certa autorevolezza vincendo tutti i passaggi sui tredici chilometri e mezzo della&nbsp; prova di Crespadoro. Un ritmo abbassato solo nel finale dove Ferri ha prudentemente badato a gestire confezionando un esordio convincente sull&rsquo;impegnativa vettura francese. &ldquo;Siamo molto soddisfatti&rdquo; ha commentato Ferri al termine. &ldquo;Il feeling con la vettura &egrave; stato subito ottimo anche se ritengo che si possa ancora migliorare. Pur avendo trovato condizioni di asciutto e una temperatura mite, la prova era infatti particolarmente scivolosa a causa della parecchia&nbsp; sabbia presente ed era facile sbagliara.&rdquo; L&rsquo;attenzione in casa Tam-Auto si sposta adesso al Rally Valli Piacentine , primo appuntamento del Pirelli Irc Cup 2012 dove il team di Romentino schiera due Citroen C4 affidate al campione in carica della serie Manuel Sossella e a Claudio Sora.</p>', 0, 0, 1, 0),
(26, 1, 26, 'WEEK-END SFORTUNATO PER LE VETTURE TAM -AUTO', 'SOSSELLA BLOCCATO DA MALANNI FISICI - SORA FUORI QUANDO ERA SECONDO', '<p>Week-end&nbsp; da archiviare in fretta sotto la voce &ldquo;sfortuna&rdquo;. Quella che ha colpito Manuel Sossella fra i pi&ugrave; attesi della vigilia. Il campione in carica della serie &egrave; stato costretto a dare forfait appena sceso dalla pedana di partenza a causa di malanni fisici che lo avevano disturbato gi&agrave; nel corso della settimana. Con Sossella fuori dai giochi le attenzioni si sono concentrante su Claudio Sora. Il bergamasco al via con Giovanni Pina, &egrave; stato fino al momento dello stop il principale avversario di Medici. Purtroppo nel corso della settima prova la Citroen C4 della Giesse Promotion &egrave; uscita di strada vanificando ogni tentativo di attacco finale. L&rsquo;appuntamento con la seconda gara dell&rsquo;Irc Pirelli &egrave; ora fissato per il Rally del Taro i programma il 5 e 6 Maggio. Prima ancora le vetture Tam-Auto saranno impegnate in Valle D&rsquo;Aosta per la gara d&rsquo;apertura del Trofeo Rally Asfalto.</p>', 0, 0, 1, 0),
(27, 1, 27, 'LA CITROEN C4 TAM-AUTO VINCENTE ANCHE IN REPUBBLICA CECA', 'Tomas Kostka e Miroslav Houst trionfano al Rally Vrchovina', '<p>Prosegue anche al di fuori dei confini nazionali la striscia positiva per la Tam-Auto<br />Nel scorso week-end fra il 7 e 8 Aprile, il team guidato da Luca Zonca era infatti impegnato al Rally Vrchovina, valido per il campionato nazionale. La Citroen C4 era affidata a Tomas Kostka e Miroslav Houst i quali sono stati protagonisti di una vittoria nitida mai messa in discussione. L&rsquo;equipaggio Citroen ha infatti preceduto la Mitsubishi di Jan Sykora e Martina Skardova giunti alle loro spalle con un ritardo di 1&rsquo;02&rdquo;9. Prosegue intanto la marcia di avvicinamento al Rally della Valle D&rsquo;Aosta gara che aprir&agrave; la stagione nel Trofeo Rally Asfalto 2012 dove Tam-Auto da sempre &egrave; protagonista.</p>', 0, 0, 1, 0),
(28, 1, 28, ' OTTIMO ESORDIO SULLA TERRA PER IL GIOVANE ALESSANDRO RE', 'Al rally Conca D’Oro Re chiude 12° assoluto e terzo nel Trofeo Ds3', '<p>Si &egrave; conclusa positivamente la prima esperienza sulla terra per il giovane Alessandro Re<br />Il Rally Conca D&rsquo;Oro primo appuntamento del Trofeo Rally Terra si &egrave; rivelato impegnativo soprattutto per le condizioni meteo avverse&nbsp; che hanno messo a dura prova uomini e mezzi.<br />&ldquo;Il molto fango ha messo in crisi le valvole Westgate&rdquo; racconta Luca Zona patron di Tam-Auto. La gara del giovane figlio d&rsquo;arte si &egrave; conclusa con un ottimo 12&deg; posto assoluto e il podio sia nella classifica riservata al Trofeo monomarca&nbsp; che nella classe R3. &ldquo;E&rsquo; stata una gara difficile&rdquo; racconta Alessandro Re. &ldquo;Per me era la prima vera esperienza sulla terra e le condizioni del tempo non ci hanno certo aiutato. Le impegnative prove dell&rsquo;entroterra siciliano sono state rese ancora pi&ugrave; infide dal molto fango che ha provocato parecchi problemi alle vetture. Nonostante tutto penso di aver disputato un&rsquo;ottima gara. Anche la vettura ha reagito molto bene alle sollecitazioni, questo&nbsp; ci fa ben sperare per il futuro&rdquo;</p>', 0, 0, 1, 0),
(29, 1, 29, 'DALLA VALLE D’ AOSTA ALLA REPUBBLICA CECA, WEEK-END VINCENTE PER TAM AUTO', 'Felice Re e Mara Bariani vincono il primo atto del T.R.A.  Kostka – Houst si ripetono in Repubblica Ceca.', '<p>Sorride Gian Luca Zonca mentre gronda di spumante. Festeggia insieme a Felice Re e Mara Bariani una vittoria in Valle D&rsquo;Aosta mai messa in discussione mentre dalla Repubblica Ceca arriva la notizia che il ventisettenne Tomas Kostka ha portato per la seconda volta consecutiva sul gradino pi&ugrave; alto del podio la C4 nell&rsquo;appuntamento valido per il Trofeo Nazionale. Felice Re e Mara non hanno avuto avversari nell&rsquo;appuntamento di apertura del Trofeo Rally Asfalto 2012. La coppia lariana al via sulla Citroen C4 si &egrave; aggiudicata tutti i parziali in programma precedendo nel finale il valdostano Marco Blanc su Ford Fiesta Wrc e il conterraneo Fabrizio Denchasaz su Citroen Xara Wrc. &ldquo;Ci tenevo a vincere questo Valle D&rsquo;Aosta che dedico a Giorgio Caputo che, oltre ad essere l&rsquo;anima di questa gara era un grande amico&rdquo; ha dichiarato Felice Re all&rsquo;arrivo. Adesso ci prepareremo per il Rally Lanterna di fine Maggio.<br />Intanto in Repubblica Ceca Tomas Kostka e Miroslav Houst si sono aggiudicati il Rally Raid Thermica valido per il Trofeo Nazionale. Il giovane ventisettenne ha dominato anche la seconda gara di campionato vincendo tutte le prove speciali in programma. <br />L&rsquo;attenzione si sposta ora al Rally del Taro dove Manuel Sossella cercher&agrave; di riconquistare il titolo I.R.C. a bordo della Citroen C4 Tam-Auto.</p>', 0, 0, 1, 0),
(30, 1, 30, 'MANUEL SOSSELLA RIPRENDE LA MARCIA ALLA CONQUISTA DELL’IRC CUP', 'La vittoria dopo una sfida incandescente con Piero Longhi', '<p>Un duello entusiasmante concluso solo sotto la bandiera a scacchi. Una sfida ad altissima adrenalina quella vissuta al Rally del Taro, secondo appuntamento dell&rsquo;International Rally Cup 2012 che alla fine ha premiato la costanza e l&rsquo;intelligenza tattica di Manuel Sossella al via con Walter Nicola sulla Citroen C4 Tam-Auto. Una gara disturbata per lunghi tratti dalla pioggia dove la scelta dei pneumatici ha assunto un ruolo determinante per l&rsquo;esito della sfida. All&rsquo;iniziale vantaggio di Longhi, al via sulla Mini Countryman ha risposto prontamente Sossella. L&rsquo;altalena al comando della classifica assoluta ha vissuto la svolta decisiva sul secondo passaggio sui ventiquattro chilometri di Folta dove Sossella ha guadagnato sette preziosissimi secondi. Un vantaggio incrementato anche nel crono successivo affrontato sotto la pioggia con pneumatici da stampo che ha definitivamente consegnato la vittoria al pilota Citroen campione in carica dell&rsquo;Irc. &ldquo;E stata una delle migliori prestazione della mia carriera&rdquo; ha commentato Sossella all&rsquo;arrivo. &ldquo;Una sfida esaltante con uno dei piloti pi&ugrave; forti del panorama italiano&rdquo;. Prossimo appuntamento&nbsp; il Prealpi Orobiche ad inizio Giugno.</p>', 0, 0, 1, 0),
(31, 1, 31, 'POCA FORTUNA PER KOSKA E HOUST AL RALLY KOPNA', 'Tam Auto si consola con Odlozilik - Turecek  3° assoluti all’esordio sulla C4', '<p class=\\"MsoNormal\\">Poca fortuna per&nbsp; Tomas Kostka e Miroslav Houst al rally Kopna, terza prova del campionato Ceco. Una uscita di strada sulla penultima prova speciale ha vanificato la rincorsa alla leader ship dopo una sfida incandescente con Kresta poi vincitore. La gara, partita con fondo asciutto &egrave; stata poi resa difficile da un improvviso temporale che ha reso le speciali viscide. Le note positive giungono da Odlozilik - Turecek all&rsquo;esordio sulla Citroen C4. Un errore sull&rsquo;ultima prova ha vanificato un secondo posto assoluto che era alla loro portata. L&rsquo;equipaggio Citroen ha comunque chiuso in terza posizione e con lo stop di Kostka ha assunto il comando nella classifica generale del campionato.</p>', 0, 0, 1, 0),
(32, 1, 32, 'FELICE RE E MARA BARIANI DOMINANO IL RALLY DELLA LANTERNA', 'Il lombardo in gara con la Citroen C4 non ha avversari nel secondo round del Trofeo Rally Asfalto\nAlessandro Re e Nicola Berruti chiudono al settimo posto nel trofeo Citroen Ds3\n', '<p>Nemmeno un temporale improvviso &egrave; riuscito a scombinare i piani di Felice Re e Mara Bariani. L&rsquo;equipaggio portacolori della scuderia Etruria si &egrave; aggiudicato il 28&deg; Rally della Lanterna, secondo atto del Trofeo Rally Asfalto 2012 girone A. Per il lombardo della Citroen la vittoria non &egrave; mai stata messa in discussione. Davanti al resto della compagnia fin dalla prova in door svolta nell&rsquo;atmosfera unica del Palazzetto dello sport genovese che ha fatto da aperitivo alla gara sviluppata lungo nove prove speciali. Re ha via via incrementato il suo vantaggio e il temporale che si &egrave; scatenato prima del via della settima prova ha esclusivamente contribuito ad ingarbugliare la situazione di chi era all&rsquo;inseguimento del pilota Citroen. Vincendo sei dei nove tratti in programma, Felice Re ha concluso davanti al sorprendente Andrea Crugnola con un vantaggio di tre minuti. &ldquo;Non &egrave; mai facile vincere il Lanterna&rdquo; ha dichiarato Felice Re all&rsquo;arrivo. &ldquo;La bellezza di questa gara sta proprio nell&rsquo;andare forte su un tracciato cos&igrave; unico&rdquo;. I due successi in altrettanti appuntamenti pongono le basi per una stagione che ha gi&agrave; preso il piede giusto. &ldquo;Adesso decideremo come muoverci anche alla luce di quanto accadr&agrave; nel girone B che prender&agrave; il via con il Rally della Marca. E&rsquo; presto per fare delle valutazioni, certo &egrave; che il successo di Aosta e Lanterna &egrave; un ottimo bottino&rdquo;. Nel trofeo Citroen Ds3 Alessandro Re e Nicola Berruti hanno chiuso al settimo posto una gara dove va registrato il terzo tempo di trofeo nel corso del secondo passaggio sulla &ldquo;Portello&rdquo;</p>', 0, 0, 1, 0),
(33, 1, 33, 'DAVID OLDRATI E TANIA CANTON TERZI ASSOLUTI AL PREALPI OROBICHE', 'In gara sulla Citroen C4 Oldrati è protagonista di una ottima prestazione               \nNel prossimo Week-end occhi puntati sul Marca Trevigiana dove si attende il tris di Felice Re\nNel campionato Ceco Tomas Kostka e Miroslav Houst rientrano al Rally Krkonose\n', '<p>Alla fine &egrave; arrivato quel posto sul podio assolutamente meritato frutto di una gara veloce e intelligente. David Oldrati e Tania Canton archiviano con una certa soddisfazione il terzo posto assoluto ottenuto sulle strade bergamasche del Prealpi Orobiche, terzo appuntamento dell&rsquo;Irc Pirelli. A bordo della Citroen C4 il pilota della scuderia Giesse Promotion aveva chiuso la prima giornata alle spalle di Perico facendo segnare per due volte il secondo tempo assoluto. &ldquo;Nella seconda parte&rdquo; racconta Oldrati &ldquo;siamo stati raggiunti da Piero Longhi e non abbiamo cercato inutili rischi accontentandoci della posizione che comunque ci soddisfa&rdquo; Dopo l&rsquo;impegno sulle strade bergamasche , l&rsquo; attenzione torna al Trofeo Rally Asfalto che propone il Rally della Marca Trevigiana , primo appuntamento del girone B. Felice Re dopo aver messo una seria ipoteca sul girone A con le vittorie di Aosta e Lanterna, vuole firmare anche l&rsquo;appuntamento veneto vinto lo scorso anno per affrontare con una certa tranquillit&agrave; il resto della stagione. &ldquo;Aosta e Lanterna sono un bottino prezioso che non ci mette al riparo dalle sorprese&rdquo; commenta il pilota lombardo. &ldquo;Dovremo cercare di far bene anche sulle strade del Marca dove gli avversari saranno assatanati e poi tireremo le somme&rdquo;. Gli impegni Tam Auto non si fermano alla nostra penisola. In programma nella repubblica Ceca il Rally Krkonose 4&deg; appuntamento del campionato Ceco che vede il rientro di Tomas Kostka e Miroslav Houst dopo il botto rimediato al Rally Kopna. Kostka si sente in forma e vuole ritornare alla vittoria per rigudagnare il comando della classica generale.</p>', 0, 0, 1, 0),
(34, 1, 34, 'FELICE RE E MARA BARIANI DOMINANO ANCHE IL RALLY DELLA MARCA', 'Terzo successo  consecutivo per il lombardo. Dopo Aosta e Lanterna  caccia al primato anche nel girone B', '<p>Aosta e Lanterna hanno offerto un verdetto definitivo sul girone A del Trofeo Rally Asfalto. Ma Felice Re non &egrave; certo il tipo che si accontenta. Dopo anni di onorata carriera ha imparato che i conti si fanno alla fine, abituandosi a rincorrere ogni obiettivo possibile senza tralasciare nulla. La C4 si &egrave; dimostrata come nelle previsioni la vettura da battere e anche sulle strade difficili del Rally della Marca che ha aperto le ostilit&agrave; anche nel girone B del trofeo cadetto. Gi&agrave; nella prova spettacolo allo &ldquo;Zadraring&rdquo; di Valdobbiadene .la C4 Tam-Auto ha fatto segnare il miglior tempo. La gara sotto l&rsquo;aspetto squisitamente cronometrico non ha avuto storia con il lombardo della Citroen sempre in testa e l&rsquo;alternanza di successi parziali con Fontana. Quattro a testa le vittorie in prova mentre l&rsquo;ultimo impegno cronometrico che ha chiuso la gara &egrave; andato a Porro. Questo successo proietta&nbsp; l&rsquo;equipaggio lombardo della scuderia Etruria fra i protagonisti assoluti di questa stagione. &ldquo;Come ho detto prima di questa gara, i conti li facciamo alla fine&rdquo; ha dichiarato&nbsp; Felice Re dopo la vittoria veneta. &ldquo;E&rsquo; senza dubbio una stagione dove stiamo raccogliendo i frutti del nostro impegno, mio di Mara e di tutto il team Tam-Auto. <br />La trasferta al Rally Krkonose in Repubblica Ceca si &egrave;, purtroppo conclusa anticipatamente per uscita di strada dell&rsquo;equipaggio Kostka &ndash; Houst. Per il veloce pilota locale si tratta della seconda uscita per incidente dopo le vittorie riportate nelle prime due manches del Campionato Nazionale Rallysprint.</p>', 0, 0, 1, 0);
INSERT INTO `tamautoitemsdetail` (`id`, `lang`, `item`, `itemTitle`, `itemAbstract`, `itemText`, `sign`, `homepage`, `active`, `del`) VALUES
(35, 1, 35, 'FELICE RE E MARA BARIANI VINCONO ANCHE IL RALLY DEL SALENTO', 'Il portacolori della scuderia Etruria chiude a punteggio pieno il girone A del Trofeo Rally Asfalto', '<p>Affermare che sulle strade salentine &egrave; stata una passeggiata sarebbe assolutamente sbagliato. Dopo aver fatto segnare il miglior tempo nella prova spettacolo sul circuito &ldquo;Pista Salentina&rdquo;di Ugento e essersi ripetuto sul parziale di Palombara, Felice Re ha dovuto fare i conti con un banale quanto fastidioso problema elettrico che ha fatto ripetutamente spegnere il motore della Citroen C4 facendo perdere parecchi secondi e la testa della classifica. Un problema prontamente risolto in assistenza e con una vettura ritornata perfetta il pilota lariano ha prontamente recuperato il gap cronometrico riportandosi al comando e vincendo per la prima volta l&rsquo;appuntamento pugliese. Un risultato che permette a Felice Re e Mara Bariani di chiudere a punteggio pieno il girone A del Trofeo Rally Asfalto e concentrarsi sulle finali all&rsquo;Appennino Reggiano a Settembre e il Rally di Como in programma a met&agrave; Novembre. &ldquo;Con due risultati utili nel girone A per noi qui al Salento contava far presenza&rdquo; , ha dichiarato Felice Re all&rsquo;arrivo. &ldquo;Quando corri a certi livelli hai per&ograve; l&rsquo;obbligo di onorare fino i fondo l&rsquo;impegno e poi volevo vincere questa gara che non avevo mai vinto. Abbiamo avuto questo problema elettrico che faceva spegnere il motore della vettura. Il team &egrave; stato come al solito perfetto risolvendo prontamente dandoci la possibilit&agrave; di recuperare e vincere la gara.&rdquo; Adesso le attenzioni e le energie saranno puntate sulle due finale. &ldquo;Valuteremo nei prossimi giorni se disputare anche le restanti gare del girone B. Di sicuro ci presenteremo concentrati all&rsquo;Appennino Reggiano dove &egrave; importante far bene per concretizzare una stagione fino a questo punto perfetta&rdquo;.</p>', 0, 0, 1, 0),
(36, 1, 36, 'VITTORIA FRANCESE PER TAM-AUTO AL RALLY DES BORNES VINCONO I CONIUGI COMTE SULLA CITROEN C4', '', '<p>Quello sulle strade salentine, non &egrave; stato l&rsquo;unico impegno di questo week-end per la Tam-Auto. Sulle strade francesi del Rally Des Bornes, gara valida per il campionato nazionale transalpino, i coniugi Comte hanno portato alla vittoria la Citroen C4. Per Comte questa era una gara test in vista del Rally Mont Blanc in programma nel week-end fra il 7 e 8 Settembre prossimi. Comte che ha corso con pneumatici Pirelli, ha vinto sei delle dieci prove speciali in programma chiudendo con un vantaggio di due minti nei confronti di Ludovic Gal in gara su una Peugeot 207 Super 2000. Unico inconveniente in tutta la gara l&rsquo;interfono che ha disturbato l&rsquo;equipaggio nel corso della prima prova speciale. Ovviamente soddisfatto Comte. &ldquo;La C4 si &egrave; rivelata una macchina fantastica, abbiamo subito raggiunto un ottimo feeling con la vettura e siamo molto fiduciosi per il Rally Mont Blanc a Settembre&rdquo;.</p>', 0, 0, 1, 0),
(37, 1, 37, 'MANUEL SOSSELLA E WALTER NICOLA VINCONO IL RALLY DEL CASENTINO', 'Con questa vittoria il pilota vicentino risale al quinto posto della graduatoria Irc Pirelli.<br/>\nSfortunato Colombini fermato da una uscita di strada mentre era in testa all’assoluta\n', '<p>Il Rally del Casentino, quarto appuntamento dell&rsquo; IRC Pirelli 2012 ha restituito un Manuel Sossella in piena forma capace di battere la nutrita concorrenza e portare, insieme a Walter Nicola, la Citroen C4 tam-Auto alla vittoria assoluta. Il pilota Citreon ha ingaggiato una sfida incandescente con Denis Colombini il quale ha poi dovuto arrendersi per uscita di strada sulla quarta prova. Con l&rsquo;uscita di scena del sanmarinese, Sossella ha badato a gestire conquistando un successo importante, il secondo di questa stagione dopo quello al rally del Taro che lo rilancia nella corsa al titolo. &ldquo;Abbiamo sofferto sull&rsquo;ultima prova&rdquo; racconta Sossella. &ldquo;Purtroppo quando hai un vantaggio consistente &egrave; pi&ugrave; facile deconcentrasi. Io e Walter abbiamo fatto un grande lavoro specie nella seconda parte. Sono molto contento e dedico questa vittoria alla mia famiglia, in particolare alla piccola Eleonora nata dieci giorni fa.&rdquo; Prossimo appuntamento il rally delle valli Cuneesi a fine Agosto.</p>', 0, 0, 1, 0),
(38, 1, 38, 'KOSTKA: RIENTRO CON VITTORIA AL RALLY KOSTELEC – DUCOLI 2° ASSOLUTO A BRESCIA', 'Ritornano alla vittoria Tomáš Kostka e Miroslav Houšť dopo le due sfortunate uscite che hanno caratterizzato questo scorcio di stagione. ', '<p>Ritornano alla vittoria Tom&aacute;&scaron; Kostka e Miroslav Hou&scaron;ť dopo le due sfortunate uscite che hanno caratterizzato questo scorcio di stagione. L&rsquo;equipaggio ceco a bordo della Citroen C4 Tam-Auto ha vinto il Rally Kostelec. Una vittoria mai messa in discussione visto che la C4 ha primeggiato in sei delle sette prove speciali in programma. Per Kostka si tratta della terza vittoria stagionale. Prossimo impegno il Rally Agropa in programma l&rsquo;undici di Agosto. <br />Notizie positive anche da Brescia dove Felice Ducoli e Giovanni Maifredini hanno concluso al secondo posto assoluto all&rsquo;esordio sulla Citroen C4. Soddisfatto Ducoli. &ldquo;Assolutamente si. Per noi questo era l&rsquo;esordio sulla C4 che si &egrave; rivelata un mezzo eccezionale. Ci ha seguiti un team professionale che ci ha permesso di raggiungere subito un ottimo feeling con la vettura ed esprimerci al meglio. Ci siamo davvero divertiti.&rdquo;</p>', 0, 0, 1, 0),
(39, 1, 39, 'TOMAS KOSTKA CONQUISTA LA VITTORIA AL FUCHS OIL RALLY AGROPA', 'Con questo risultato il pilota ceco riconquista la leader ship del campionato a due gare dalla fine', '<p>Una vittoria importante quella ottenuta a bordo della Citroen C4 Tam-Auto dall&rsquo;equipaggio ceco Kostka &ndash; Hruza al Rally Fuchs Oil Rally Agropa, sesto appuntamento del campionato Rally Sprint Ceco che si &egrave; corso il 10 e 11 Agosto. Una prestazione ad alto livello che ha permesso al giovane pilota Citroen di riconquistare il primato nella classifica generale quando mancano due sole gare alla fine. Un successo mai messo in discussione che lo ha sicuramente caricato in vista del rush finale. &ldquo;Abbiamo vinto molto bene&rdquo; ha commentato Kostka. &ldquo;Per noi era determinante ottenere il massimo risultato per riconquistare il primato nella classifica generale. Adesso sar&agrave; importante riuscire a mantenere questo passo fino alla fine&rdquo;</p>', 0, 0, 1, 0),
(40, 1, 40, 'MANUEL SOSSELLA E WALTER NICOLA VINCONO IL RALLY VALLI CUNEESI', 'Per il driver vicentino questa è la terza vittoria consecutiva nella gara piemontese\nDopo questa gara Sossella sale al 4° posto nella classifica generale. A Bassano la sfida decisiva.', '<p>L&rsquo;imperativo era prima di tutto vincere. Manuel Sossella era sceso in Piemonte con la consapevolezza che le strade occitane costituivano l&rsquo;ultima occasione per poter sperare ancora di dare un senso vincente alla sua stagione. Convinto di poter sfruttare al meglio il vantaggio di correre su strade a lui particolarmente congeniali (ha vinto le ultime due edizioni ndr). Non si &egrave; fatto attendere il gioielliere vicentino al via come di consueto con Walter Nicola. Messe subito le cose in chiaro gi&agrave; sui sette chilometri della prova di Brondello dove la Citroen C4 ha preceduto la Subaru di Piero Longhi con un distacco apparso gi&agrave; consistente, l&rsquo;equipaggio della Publimedia&nbsp; ha poi proseguito facendo segnare il miglior tempo anche nelle altre quattro prove speciali che hanno chiuso la prima parte della competizione. La reazione degli avversari ha combaciato con l&rsquo;unico errore commesso dal pilota Citroen protagonista di un testa coda dove ha perso circa dieci secondi. La supremazia di Manuel Sossella e Walter Nicola &egrave; poi proseguita in modo inequivocabile conquistando le rimanenti cinque prove speciali e riportando vincente sul palco d&rsquo;arrivo la Citroen C4. &ldquo;L&rsquo;unico nostro obiettivo possibile era ottenere il massimo risultato per mantenere vive le speranze di raggiungere Capelli.&rdquo; Ha commentato Manuel Sossella all&rsquo;arrivo. &ldquo;Sapevamo che la concorrenza era assolutamente agguerrita e abbiamo attaccato da subito&rdquo;. Una vittoria nitida sotto tutti i punti di vista.<br />&ldquo;Si, come al solito il team ha svolto un grande lavoro mettendomi a disposizione una vettura perfetta.&rdquo; Il risultato ottenuto sulle strade piemontesi permette a Manuel Sossellae Walter Nicola, ora in quarta posizione nella classifica generale, di mantenere vive le speranze di vittoria grazie agli scarti. A Bassano fra circa un mese, ultimo appuntamento dell&rsquo;IRC Pirelli sar&agrave; ancora grande sfida.</p>', 0, 0, 1, 0),
(41, 1, 41, 'ALESSANDRO RE E MAURO MENCHINI TERZI NEL TROFEO CITROEN ALL’ALPI ORIENTALI<br/>FELICE RE E MARA BARIANI CONCLUDONO AL SECONDO POSTO IL “BARELLI RONDE”', 'Doppio impegno per Tam-Auto che nello scorso week-end è ha visto le sue vetture impegnate sulle strade friulane del rally Alpi Orientali e su quelle fra la Val Senagra e Val Cavaragna del “Barelli Ronde”', '<p>Doppio impegno per Tam-Auto che nello scorso week-end &egrave; ha visto le sue vetture impegnate sulle strade friulane del rally Alpi Orientali e su quelle fra la Val Senagra e Val Cavaragna del &ldquo;Barelli Ronde&rdquo;<br />Per il Trofeo Rally Asfalto era in programma il secondo appuntamento del girone B con il Rally Alpi Orientali. Assente giustificato Felice Re il quale con i precedenti risultati ha messo in cassaforte il passaporto per le finali. Ancora aperti invece i giochi per il Trofeo Citroen dove Alessandro Re e Mauro Manchini hanno concluso con un brillate terzo posto di trofeo a bordo della Citroen Ds3, chiudendo inoltre decimi&nbsp; assoluti nella classifica generale. Soddisfatto il giovane figlio d&rsquo;arte. &ldquo;E&rsquo; stata una buona gara&rdquo; commenta Alessandro Re. &ldquo;Le prove speciali friulane sono sempre molto belle e impegnative. Ci siamo divertiti&rdquo;. Esentato da obblighi di presenza nel Trofeo Rally Asfalto dove ha ampiamente conquistato l&rsquo;accesso alle finali, Felice Re ha partecipato al &ldquo;Barelli Ronde&rdquo; manifestazione comasca organizzata per ricordare Sergio Barelli. Il pilota lombardo al via come al solito con Mara Bariani sulla Citroen C4 ha vinto la prima prova speciale incappando per&ograve; in una errata scelta di pneumatici sul secondo passaggio partendo con le stampo su un&rsquo;asfalto ancora umido per la pioggia. Scelta che ha fatto perdere al veloce driver lombardo oltre 26 secondi e il primato assoluto. &ldquo;Purtroppo abbiamo azzardato la scelta, credendo di trovare asciutto&rdquo; ha commentato Felice Re all&rsquo;arrivo. &ldquo;Sono cose che capitano. Anche se un po mi spiace devo dire che noi eravamo qui soprattutto per onorare il ricordo di Sergio. E&rsquo; stata una gara molto bella e ben organizzata.&rdquo; Le attenzioni si spostano adesso al prossimo week end dove andr&agrave; in scena la Ronde del Gomitolo. Sulle strade biellesi Tiziano Borsa e Carla Berra cercheranno di conquistare la prima vittoria assoluta sulla Citroen C4</p>', 0, 0, 1, 0),
(42, 1, 42, 'TIZIANO BORSA E CARLA BERRA 4° ASSOLUTI AL RALLY DEL GOMITOLO', 'Un problema tecnico e un testa coda impediscono all’equipaggio biellese di raggiungere il podio', '<p>Che questa non sia propriamente una stagione fortunata per Tiziano Borsa e Carla Berra lo si era ampiamente visto negli impegni precedenti. &ldquo;Almeno questa volta siamo arrivati al fondo&rdquo; commenta con un pizzico di delusione il driver della Biella Motor team in gara sulla Citroen C4 della Tam-Auto. &ldquo;Sulla prima prova abbiamo avuto un problema con la procedura di partenza perdendo almeno una dozzina di secondi. Nel secondo passaggio ho faticato parecchio con il freno a leva. La terza prova &egrave; stata l&rsquo;unica dove abbiamo avuto modo di esprimerci al meglio e infatti abbiamo abbassato sensibilmente il nostro tempo&rdquo;. Il terzo gradino del podio sembrava alla vostra portata. &ldquo;Ci stavo provando anche perch&eacute; Uzzeni era li ad un passo. Purtroppo sull&rsquo;ultima prova ci siamo girati perdendo venti secondi.&rdquo; Nella classifica assoluto Tiziano Borsa e Carla Berra concludono al 4&deg; posto assoluto. &ldquo;Per come &egrave; andata la stagione &egrave; senza dubbio un buon risultato. Certamente mi aspettavo qualcosa in pi&ugrave;.&rdquo;</p>', 0, 0, 1, 0),
(43, 1, 43, ' AL RALLY MONT BLANC ARRIVA L’OTTAVO POSTO ASSOLUTO', 'Una foratura impedisce a Frederic Comte di chiudere alle spalle di Sarazzin', '<p>Fino a met&agrave; gara era il principale avversario di un velocissimo Sarazzin. Una foratura e la necessit&agrave; di sostituire il pneumatico in prova, ha fatto perdere a Frederic e Angeliqu&egrave; Comte oltre sei minuti e le prime posizioni in classifica. Il veloce driver francese in gara sulla Citroen C4 era partito subito forte lanciandosi prepotentemente sulle tracce di Stephan&egrave; Sarazzin. &ldquo;Stavamo andando davvero bene&rdquo; commenta il transalpino. &ldquo;Purtroppo abbiamo forato ed era impossibile raggiungere il fine prova in quelle condizioni. Abbiamo deciso di sostituire il pneumatico e siamo ripartiti. La fortuna ci ha davvero voltato le spalle perch&eacute; nella prova successiva abbiamo di nuovo forato. A quel punto ci siamo concentrati per finire la gara senza cercare inutili rischi. Peccato, il duello con Stephan&egrave; si stava facendo interessante e con le Pirelli mi sono trovato davvero bene&rdquo;.</p>', 0, 0, 1, 0),
(44, 1, 44, 'AL RALLY JASINIKY KOSTKA E HOUST SONO CAMPIONI CON UNA GARA D’ANTICIPO', 'All’Appennino Reggiano Felice Re e Mara Bariani ipotecano la vittoria nel TRA', '<p>Tam-Auto vincente anche al di fuori dei confini nazionali. Con la vittoria al Rally Jasiniky Tomas kostka e Miroslav Houst si laureano campioni nazionali con una gara d&rsquo;anticipo concretizzando una stagione che, se pur difficile e a tratti sfortunata, ha visto l&rsquo;equipaggio Citroen sempre ai vertici. &ldquo;Dopo un buon inizio, l&rsquo;uscita di strada al Rally&nbsp; Kopna sembrava aver compromesso la nostra stagione &ldquo; racconta il pilota ceco.&nbsp; &ldquo;Ma la vittoria al rientro nel Rally Kostelec ci ha ridato entusiasmo e oggi ce l&rsquo;abbiamo messa davvero tutta per portare a casa questo successo che chiude finalmente i conti. Devo ringraziare chi ci ha supportato per tutta la stagione e in particolare Tam-Auto che ci ha sempre dato una vettura formidabile&rdquo;<br />Sorrisi e volti rilassati anche all&rsquo;Appennino Reggiano, prima delle due finali valide per il Trofeo Rally Asfalto 2012 dove Felice Re e Mara Bariani con il successo assoluto ottenuto sulla Citroen C4 hanno posto una seria ipoteca sul campionato che si chiuder&agrave; a Como a met&agrave; novembre. La gara ha vissuto sul duello fra Re e Fontana. Decisivo lo scratch finale della prima giornata da dove il pilota lombardo ha costruito il suo definitivo vantaggio. &ldquo;Come avevo detto alla vigilia era importante non sbagliare perch&eacute; adesso qui ogni piccolo errore si paga caro&rdquo;. Questo il commento finale di Re. &ldquo;Fontana e Porro si sono rivelati come era prevedibile avversari molto ostici e adesso occorre non deconcentrarsi perch&eacute; a Como Porro e Fontana hanno ancora delle chance che si giocheranno fino in fondo&rdquo;. L&rsquo;appuntamento reggiano ha registrato il sesto posto di Antonio Rusce e Marco Mori anche loro su una Citroen C4 mentre Alessandro Re e Elia Ungaro archiviano una prestazione sfortunata interrotta sulla terza prova a causa di una uscita di strada.</p>', 0, 0, 1, 0),
(45, 1, 45, 'MANUEL SOSSELLA E WALTER NICOLA CONQUISTANO L’IRC PIRELLI 2012', 'Il vicentino conclude il Rally di Bassano alle spalle di Kubica e bissa il successo 2011\nClaudio Addis e Alessandro Silecchia  vincono la Ronde Terra Sarda – “Città di Arzachena”\n', '<p>Ancora un week-end di successi per Tam-Auto. Il pi&ugrave; importante e sicuramente anche il pi&ugrave; atteso quello ottenuto da Manuel Sossella e Walter Nicola al Rally di Bassano ultimo appuntamento dell&rsquo;Irc Pirelli 2012 . Al vicentino in gara come di consueto sulla Citreon C4 wrc &egrave; bastato concludere alle spalle di Robert Kubica per conquistare il prestigioso successo gi&agrave; vinto lo scorso anno. Una strategia di gara cambiata profondamente dopo il ritiro di Piero Longhi, principale avversario nella corsa al titolo. A Sossella &egrave; stato sufficiente mantenere la concentrazione per portare in porto un risultato che ha permesso all&rsquo;equipaggio della Publimedia di raggiungere quel successo che a met&agrave; stagione pareva essere difficile da conquistare . Ovviamente felici gli alfieri Citroen. &ldquo;Be, felici &egrave; addirittura poco. Mi spiace per il ritiro di Piero che ha sicuramente diminuito l&rsquo;intensit&agrave; della sfida. Noi avevamo un obiettivo che sarebbe stato diabolico non centrare anche se devo dire che Robert ha fatto un vero capolavoro. Dedichiamo questa vittoria a tutti quelli che ci hanno aiutato, alla Tam-Auto che ci ha dato una vettura straordinaria seguendoci in modo stupendo. Un grazie al mio navigatore che come al solito si &egrave; dimostrato un vero professionista e anche un ottimo psicologo aiutandomi molto nei momenti difficili&rdquo;. Gli fa eco Walter Nicola. &ldquo;Ci voleva davvero!&rdquo; esordisce il torinese . &ldquo;Questa non &egrave; stata una stagione facile almeno nella prima parte. Alla fine siamo venuti fuori bene e penso che la vittoria sia davvero meritata.&rdquo; Buone notizie anche dalla Sardegna dove Claudio Addis e Alessandro Silecchia hanno portato al successo la Citreon C4 nella 1&deg; Ronde &ldquo;Terra Sarda&rdquo; Citt&agrave; di Arzachena. Il pilota sardo ha vinto due dei tre passaggi sugli undici chilometri della prova di Porto Cervo. (L&rsquo;ultimo &egrave; stato annullato per maltempo) concedendo a Diomedi la terza prova.</p>', 0, 0, 1, 0),
(46, 1, 46, 'ALESSANDRO RE TERZO NEL TROFEO CITROEN DS3 AL RALLY DI SANREMO', 'Con questo risultato il pilota lombardo chiude al terzo posto assoluto il trofeo monomarca 2012', '<p>Alessandro Re archivia con una certa soddisfazione il Trofeo Citroen Ds3 2012. Il giovane lombardo ha infatti chiuso al terzo posto la classifica del monomarca francese che ha vissuto l&rsquo;ultimo appuntamento lungo le strade liguri del rally di Sanremo. Per Alessandro re, affiancato in questa occasione da Monica Cicognini, era la seconda partecipazione a questa gara dopo quella dello scorso anno. &ldquo;Sanremo &egrave; sempre una gara tosta&rdquo; ha dichiarato al termine Re. &ldquo;La pioggia della prima tappa ha reso tutto ancora pi&ugrave; complicato. Siamo partiti con una certa prudenza. Molto meglio nella seconda tappa dove siamo riusciti a tenere un buon ritmo. Ci tengo a ringraziare la Tam-Auto che mi ha dato una vettura sempre perfetta e la Giesse Promotion che ha supportato i miei impegni.&rdquo; <br /><br /><strong>Un problema alla frizione della Peugeot 206 Wrc nega la vittoria a Denis Colombini al Rally Legend. </strong><br /><br />Sembrava fatta, quando un problema alla frizione ha fermato la gara, fino a quel momento da leader incontrastato, di Denis Colombini al Rally Legend. La perfetta interpretazione delle condizioni meteo e le note doti velocistiche del pilota avevano permesso all&rsquo;equipaggio Tam-Auto di avere un margine di vantaggio esagerato sugli avversari, purtroppo la banale rottura di una guarnizione del sistema idraulico della frizione ne a causato lo stop e il ritiro definitivo.</p>', 0, 0, 1, 0),
(47, 1, 47, 'Video Tam-auto 2011', '', '<p><iframe src=\\"http://www.youtube.com/embed/r4DVR2UxVXk\\" frameborder=\\"0\\" height=\\"315\\" width=\\"560\\"></iframe></p>', 0, 0, 1, 0),
(48, 1, 48, ' MARCO SIGNOR E PATRIK BERNARDI VINCONO LA RONDE DI MONTECAIO ALLA RONDE DEL CANAVESE BORSA E BERRA CONCLUDONO AL 4° POSTO', 'Successo netto quello conquistato da Marco Signor e Patrik Bernardi alla Ronde di Montecaio disputata sulla Citroen C4', '<p>Successo netto quello conquistato da Marco Signor e Patrik Bernardi alla Ronde di Montecaio disputata sulla Citroen C4. Il forte pilota veneto ha fatto segnare il miglior tempo su tutti i passaggi in programma mantenendo un passo costantemente veloce nonostante le insidie della pioggia e in alcuni tratti anche della nebbia. &ldquo;Abbiamo disputato un ottima gara&rdquo; ha commentato signor all&rsquo;arrivo. &ldquo;Non &egrave; stata tuttavia una passeggiata anche perch&eacute; Chentre come era previsto &egrave; andato forte. La C4 &egrave; una vettura splendida, molto emozionante.&rdquo;<br />Soddisfatti a met&agrave; invece Tiziano Borsa e Carla Berra. Alla Ronde del Canavese &egrave; mancato ancora l&rsquo;appuntamento col successo a bordo della C4. C&rsquo;&egrave; da dire che il driver valsesiano ha vinto la prima e la terza prova presentandosi al via dell&rsquo;ultimo impegno cronometrato con oltre un secondo di vantaggio sugli avversari. &ldquo;Purtroppo sull&rsquo;ultima prova non ci siamo espressi al massimo complice anche la nebbia. Al contrario i miei avversari hanno giocato il tutto per tutto e hanno fatto meglio. Sono tuttavia soddisfatto, penso di aver disputato una buona gara e la C4 &egrave; una vettura splendida&rdquo;</p>', 0, 0, 1, 0),
(49, 1, 49, 'FELICE RE E MARA BARIANI SONO I CAMPIONI DEL TROFEO RALLY ASFALTO 2012', 'Il pilota lombardo al via con la C4 conclude al secondo posto assoluto il Rally di Como vinto da Robert Kubica', '<p>Grande prestazione di Alessandro Re e David Castiglioni secondi di classe con la Citroen Ds3<br /><br />Che non sarebbe stata una passeggiata lo aveva ampiamente previsto Felice Re. Sulle strade lariane ironia della sorte erano in tre, tutti lombardi, a giocarsi le ultime chanse per conquistare il titolo nel Trofeo Rally Asfalto 2012. Dal canto suo Felice Re partiva con il poco simpatico peso psicologico di non poter steccare l&rsquo;ultima nota dopo una stagione semplicemente perfetta con cinque vittorie su cinque gare disputate. A Como non era obbligatorio vincere. Bastava, se questo pu&ograve; sembrare poco, stare davanti ai due con le Focus cercando di controllare da vicino il marziano Kubica il quale non era certo salito in Lombardia per recitare il ruolo di comparsa. Che non fosse facile lo hanno dimostrato le prime prove speciali con Porro e Fontana anche se di poco ma davanti. Distacchi minimi dove era difficile poter disegnare una tattica diversa dal piede in spalla e pedalare. Fino alla speciale numero otto, il terzo passaggio sulla Sormano dove Felice Re mette la sua C4 davanti alla Focus di Porro e una prova pi&ugrave; tardi fa la stessa cosa con la Ford di Fontana. Sembra ancora lunga ma sui quasi trenta chilometri della Val Cavargna Porro danneggia una ruota e si ferma e Fontana fora perdendo tempo. Felice Re e Mara Bariani vincono l&rsquo;ultimo impegno cronometrato della giornata e raggiungono la Pedana nel centro del capoluogo lombardo&nbsp; con un secondo posto assoluto grazie al quale conquistano la vittoria nel Trofeo Rally Asfalto 2012. Un successo raggiunto grazie ad una stagione straordinaria dove la simbiosi perfetta uomo &ndash; macchina &egrave; stata l&rsquo;arma vincente. Adesso pu&ograve; davvero sorridere Felice Re. &ldquo;Non &egrave; stato facile e lo sapevamo. Qui nessuno ci avrebbe fatto degli sconti. La svolta &egrave; avvenuta in un momento dove comunque eravamo riusciti ad imporre un buon passo cercando di aumentare quei distacchi che per tutta la gara erano stati davvero minimi. Siamo contentissimi per noi ma anche per chi ci ha appoggiato in questa stagione. In modo particolare per Tam-Auto che ci ha sempre messo a disposizione una macchina perfetta.&rdquo; La festa in casa Re &ndash; Tam-Auto &egrave; completata dal sesto posto assoluto di Alessandro Re e David Castiglioni secondi di classe sulla Citroen Ds3 autore del terzo tempo assoluto sulla lunghissima Val Cavargna. &ldquo;E&rsquo; stata una gara bellissima&rdquo; commenta io giovane Alessandro Re. &ldquo;Noi ci abbiamo provato fino alla fine ma Vittalini &egrave; andato davvero forte. Sono strafelice per il successo di Pap&agrave; e di Mara, una soddisfazione che loro e tutto il team merita ampiamente&rdquo;.</p>', 0, 0, 1, 0),
(50, 1, 50, 'AL MONZA RALLY SI CONFERMA UNA STAGIONE FANTASTICA PER TAM-AUTO - LONGHI 3° ASSOLUTO, BONANOMI 4° , CAVALLINI 8°', 'Marco Bonanomi e Marco Confortola vincono la finale del “Master Show WRC 2.0” disputata contro Piero Longhi', '<p>Spettacolo, vetrina, adrenalina. Chiamatelo come volete. Il &ldquo;Monza Rally Show&rdquo; non ha rivali in questo senso. A questa regola non &egrave; sfuggita nemmeno l&rsquo;edizione 2012 che ha festeggiato i 90 anni dell&rsquo;Autodromo lombardo. <br />A Monza Tam-Auto ha chiuso in bellezza con Piero Longhi sul podio assoluto seguito appena dietro da Marco Bonanomi, pilota ufficiale AUDI terzo all&rsquo;ultima 24h di Le Mans e al debutto nel rally Brianzolo, mentre nei primi dieci assoluti ha concluso anche Tobia Cavallini. A tutto questo va aggiunta una finale del Master Show, trofeo intitolato a Marco Simoncelli, infiammata dalla sfida fra Longhi e Bonanomi vinta da quest&rsquo;ultimo. Piero Longhi al via come di consueto insieme a Luca Cassol &ldquo;Capitan ventosa&rdquo; incontra per la prima volta la C4. Il pilota di Borgomanero parte prudente ma gi&agrave; sulla seconda prova chiude con il terzo tempo assoluto che ripete anche nella 5&deg; prova. Un crescendo di prestazioni che alla fine gli valgono il terzo gradino del podio assoluto con soli 4 decimi di vantaggio su Bonanomi. &ldquo;Siamo soddisfatti&rdquo; commenta Piero Longhi. &ldquo;In alcune fasi abbiamo faticato con il freno a mano e alla fine ho capito il meccanismo e siamo andati decisamente meglio.&rdquo; Da parte sua Bonanomi ha da recriminare per aver preso un paio di penalit&agrave; che gli sono costate 15 preziosi secondi.<br />Come di consueto la tre giorni Monzese si &egrave; conclusa con il Master show che ha infuocato la platea. Le Citroen C4 della Tam-Auto, come gi&agrave; l&rsquo;hanno scorso, hanno dominato la gara delle WRC 2.0 occupando tutte e tre le posizioni del podio; questa volta &egrave; stato Bonanomi ad imporsi su Longhi che cos&igrave; commenta: &ldquo;si, purtroppo ho spento la vettura proprio all&rsquo;ultimo giro. Bonanomi ha meritato, e andato forte per tutta la durata della gara&rdquo;. Felice anche Marco Bonanomi. Il pilota &ldquo;pistaiolo&rdquo; era in gara con la C4 insieme a Marco Confortola, simpatico scalatore degli 8000 &ldquo;Abbiamo perso il terzo posto nel Rally per soli quattro decimi. Un niente se si considera la durata e la difficolt&agrave; della gara&rdquo; racconta il pilota AUDI. &ldquo;Ci siamo rifatti nel Master e sono davvero soddisfatto&rdquo;. Tobia Cavallini e Andrea Rossetto hanno portato la terza C4 all&rsquo;ottavo posto assoluto e terza nel Master Show.<br />Per concludere, buon risultato anche dalla piccola di casa Tam-Auto/Citroen, la DS3R3T di Alessandro Re che ha chiuso in 41&deg; posizione assoluta e 2^ di gruppo R3.</p>', 0, 1, 1, 0),
(51, 1, 51, 'TIZIANO BORSA E CARLA BERRA CHIUDONO CON UN BRILLANTE 3° POSTO IN SARDEGNA', 'PRESTAZIONE CONVINCENTE SULLA CITROEN C4  ALLA RONDE D’OGLIASTRA', '<p>La vittoria assoluta sulla Citroen C4 dovr&agrave; attendere il 2013. E&rsquo; tuttavia molto soddisfatto Tiziano Borsa. In terra sarda si &egrave; chiusa una stagione per certi versi interlocutoria. Ma la prestazione sfoderata nella terra dei nuraghi dal pilota valse siano e dalla sua navigatrice Carla Berra &egrave; stata senza dubbio convincente ed ha permesso di portare la C4 Tam-Auto al terzo posto assoluto. &ldquo;Non era una gara facile&rdquo; commenta Tiziano Borsa. &ldquo;Una prova tecnica e molto bella che le insidie per l&rsquo;umidit&agrave; e la salsedine hanno reso ancora pi&ugrave; impegnativa. Abbiamo sicuramente pagato il fatto di aver provato poco contro avversari che invece conoscono molto bene questa zona. La macchina era perfetta e siamo riusciti a migliorare ad ogni passaggio.&rdquo; Soddisfatto anche della cornice che ha reso questa trasferta piacevole. &ldquo;Semplicemente stupenda. La cerimonia di partenza &egrave; stata una vera e propria festa. Da queste parti hanno vivono in modo particolare queste occasioni.&rdquo; Una stagione che tutto sommato, si chiude in modo positivo. &ldquo;Sinceramente ad inizio anno ci aspettavamo di ottenere qualcosa in pi&ugrave;. Un po di sfortuna, qualche episodio che ci ha lasciato l&rsquo;amaro in bocca, uno per tutti il ritiro di Canelli dopo aver vinto la prima prova. Ma i rally sono anche questo. Siamo soddisfatti per come abbiamo chiuso la stagione qui in Sardegna. Adesso ci concediamo una pausa e vedremo di pensare a qualcosa per il 2013&rdquo;</p>', 0, 0, 1, 0),
(52, 1, 52, 'RZEZNIK E MAZUR ESORDIO POSITIVO SULLA CITROEN C4 TAM-AUTO', 'L’ equipaggio polacco ha chiuso in  5° posizione assoluta il Rally “Barborka”', '<p>Tre prove speciali con l&rsquo;obiettivo di chiudere nei primi trenta dell&rsquo;assoluta per accedere alla prova spettacolo nel centro di Varsavia. Un vero e proprio rally show il &ldquo;Barborka&rdquo; che si &egrave; svolto nello scorso week-end intorno alla capitale polacca. Questo appuntamento ha coinciso con l&rsquo;esordio sulla Citroen C4 per Rzeznik &ndash; Mazur solitamente in gara con una Skoda Super 2000. L&rsquo;equipaggio polacco ha chiuso con un brillante 5&deg; posto assoluto. &ldquo;E&rsquo; stata senza dubbio una bella esperienza&rdquo; ha commentato Rzeznik all&rsquo;arivo. &ldquo;La C4 &egrave; molto diversa ovviamente dalla skoda. E&rsquo; una vettura con un potenziale enorme ma penso di essermi adattato in fretta alle molte differenze che la distinguono dalla super 2000&rdquo;. Un vero e proprio bagno di folla ha seguito la prova spettacolo nel centro di varsavia che ha chiuso la gara.</p>', 0, 1, 1, 0),
(53, 1, 53, 'RONDE della VAL MERULA', 'COLOMBINI 1° ASSOLUTO alla Ronde della Val Merula', '<p style="margin-bottom: 0cm;"><em>Andora, 08/02/2015</em></p>\n<p style="margin-bottom: 0cm;">Si &egrave; conclusa con un successo netto la partecipazione alla 2^ Ronde della Val Merula per l&rsquo;equipaggio TAM-AUTO Colombini &ndash; Bizzocchi al volante della Ford Fiesta R5.</p>\n<p style="margin-bottom: 0cm;">Il forte pilota Sanmarinese ha fatto segnare il miglior tempo su tutti i quattro passaggi della PS della gara,  nonostante gli avversari pi&ugrave; accreditati disponessero di vetture WRC.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Abbiamo disputato un ottima gara&rdquo; ha dichiarato Denis Colombini all&rsquo;arrivo &ldquo;durante la gara abbiamo provato diverse soluzioni di gomme e assetti, &egrave; stato un ottimo test in vista di impegni futuri pi&ugrave; importanti, la macchina &egrave; sempre stata perfetta&rdquo;.</p>', 0, 0, 1, 0),
(54, 1, 54, 'RONDE della VAL MERULA', 'COLOMBINI 1° ASSOLUTO alla Ronde della Val Merula', '<p style="margin-bottom: 0cm;"><em>Andora, 08/02/2015</em></p>\n<p style="margin-bottom: 0cm;">Si &egrave; conclusa con un successo netto la partecipazione alla 2^ Ronde della Val Merula per l&rsquo;equipaggio TAM-AUTO Colombini &ndash; Bizzocchi al volante della Ford Fiesta R5.</p>\n<p style="margin-bottom: 0cm;">Il forte pilota Sanmarinese ha fatto segnare il miglior tempo su tutti i quattro passaggi della PS della gara,  nonostante gli avversari pi&ugrave; accreditati disponessero di vetture WRC.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Abbiamo disputato un ottima gara&rdquo; ha dichiarato Denis Colombini all&rsquo;arrivo &ldquo;durante la gara abbiamo provato diverse soluzioni di gomme e assetti, &egrave; stato un ottimo test in vista di impegni futuri pi&ugrave; importanti, la macchina &egrave; sempre stata perfetta&rdquo;.</p>', 0, 0, 1, 0),
(55, 1, 55, 'POCA FORTUNA ALLA 2^ GARA RCS AD IMOLA PER TAM-AUTO', '', '<p>Week end da archiviare in fretta quello appena passato&hellip;l&rsquo; equipaggio composto da Enrico e Maurizio Tortone che portavano in gara per la prima volta la Fiesta Wrc erano entusiasti della macchina, dopo la seconda prova speciale in programma occupavano la seconda posizione assoluta, dietro il futuro vincitore della gara di Imola, avendo fatto segnare il secondo tempo sulla PS 1 e vincendo la PS 2. Sulla PS 3 un problema in partenza vanificava la rincorsa alla prima posizione, nel corso della PS 4 una toccata fermava definitivamente la gara del nostro equipaggio.</p>\n<p>&ldquo;Peccato&rdquo; esordisce cos&igrave; Enrico &ldquo;ci stavamo divertendo senza forzare, la Fiesta &egrave; una grande macchina, proprio quando pensavamo di conoscerla un po&rsquo; meglio in una curva da 30 km/h abbiamo commesso un piccolo errore che abbiamo pagato caro&hellip;&rdquo;</p>', 0, 0, 1, 0),
(58, 1, 58, 'LUCA GULFI 7° ASSOLUTO alla RONDE LIBURNA TERRA', '', '<p>Luca Gulfi per il suo ritorno su una vettura WRC non si &egrave; fatto mancare niente&hellip;la scelta &egrave; caduta su una gara su terra da affrontare con la Peugeot 307 Wrc.</p>\n<p>Il pilota Torinese spiega cos&igrave; la sua scelta &ldquo;Era tanto che volevo correre sulla terra con una WRC e finalmente ci sono riuscito, direi che ho fatto un&rsquo;ottima scelta sia per la gara bellissima che per la macchina, la 307 sulla terra &egrave; molto facile da usare, correre su questo fondo &egrave; molto divertente e anche per chi corre poco come me con una macchina a posto ci si diverte da matti.&rdquo;</p>\n<p>Dopo i quattro passaggi previsti sulla mitica prova speciale di Ulignano, una classica di tanti Rally Sanremo degli anni passati, l&rsquo;equipaggio Tam-Auto ha concluso in 7&deg; posizione assoluta senza problemi alla vettura e divertendosi &ldquo;sicuramente ripeteremo questa esperienza&rdquo; chiude cos&igrave; il suo commento Luca Gulfi.</p>', 0, 0, 1, 0),
(56, 1, 56, 'POCA FORTUNA ALLA 2^ GARA RCS AD IMOLA PER TAM-AUTO  ', '', '<p style="margin-bottom: 0cm;">Week end da archiviare in fretta quello appena passato&hellip;l&rsquo; equipaggio composto da Enrico e Maurizio Tortone che portavano in gara per la prima volta la Fiesta Wrc erano entusiasti della macchina, dopo la seconda prova speciale in programma occupavano la seconda posizione assoluta, dietro il futuro vincitore della gara di Imola, avendo fatto segnare il secondo tempo sulla PS 1 e vincendo la PS 2. Sulla PS 3 un problema in partenza vanificava la rincorsa alla prima posizione, nel corso della PS 4 una toccata fermava definitivamente la gara del nostro equipaggio.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Peccato&rdquo; esordisce cos&igrave; Enrico &ldquo;ci stavamo divertendo senza forzare, la Fiesta &egrave; una grande macchina, proprio quando pensavamo di conoscerla un po&rsquo; meglio in una curva da 30 km/h abbiamo commesso un piccolo errore che abbiamo pagato caro&hellip;&rdquo;</p>', 0, 0, 1, 0),
(57, 1, 57, 'CRISTIANO MANZINI 5° ASSOLUTO al 39° RALLY 1000 MIGLIA', '', '<p>Risultato positivo e soddisfacente per il team TAM-AUTO al 39&deg; Rally 1000 Miglia prima gara del Campionato Italiano WRC, svoltasi venerd&igrave; 27 e sabato 28 Marzo a Brescia dove l&rsquo;equipaggio Manzini &ndash; Lucchi al volante della Ford Fiesta Wrc hanno concluso in 5^ posizione assoluta e primo equipaggio Bresciano al traguardo della competizione.</p>\n<p>&ldquo;Sono molto contento&rdquo; esordisce cos&igrave; il pilota Bresciano Cristiano Manzini &ldquo;mi sono divertito molto, la macchina &egrave; fantastica e ha funzionato alla perfezione, ringrazio il team per l&rsquo;ottimo lavoro svolto&rdquo;.</p>', 0, 0, 1, 0),
(59, 1, 59, '19/04/2015 TORTONE 2° ASSOLUTO al RALLY CIRCUIT PAVIA', '', '<p style="margin-bottom: 0cm;">Lo scorso fine settimana, al volante della Ford Fiesta WRC, il pilota torinese della scuderia WRT ha chiuso in seconda posizione assoluta l&rsquo;ultima gara del Campionato RCS svoltasi sul circuito Tazio Nuvolari di Cervesina (Pv).</p>\n<p style="margin-bottom: 0cm;">Una prima prova speciale disputata il Sabato sotto un temporale e poi altre quattro prove la Domenica occupando sempre le posizioni di vertice per la coppia formata da Enrico e Maurizio Tortone, in gara per la seconda volta con la Fiesta WRC preparata dalla Tam-Auto e gommata Pirelli che hanno permesso di chiudere in 2^ posizione assoluta la gara a soli 4 decimi di secondo dal vincitore della gara Franco Uzzeni.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Enrico Tortone: &ldquo;Sono molto contento sia per la macchina che &egrave; stata perfetta, per le gomme Pirelli e per la mia prestazione, aver chiuso con un distacco cos&igrave; ridotto da Uzzeni mi fa ben sperare per i nostri prossimi impegni dove cercheremo di migliorarci ulteriormente. Ringrazio i nostri sponsor per la fiducia che ci dimostrano e il team per la macchina sempre al top che ci mette a disposizione.&rdquo;</p>', 0, 0, 1, 0),
(60, 1, 60, '19/04/2015 TORTONE 2° ASSOLUTO al RALLY CIRCUIT PAVIA', '', '<p style="margin-bottom: 0cm;">Lo scorso fine settimana, al volante della Ford Fiesta WRC, il pilota torinese della scuderia WRT ha chiuso in seconda posizione assoluta l&rsquo;ultima gara del Campionato RCS svoltasi sul circuito Tazio Nuvolari di Cervesina (Pv).</p>\n<p style="margin-bottom: 0cm;">Una prima prova speciale disputata il Sabato sotto un temporale e poi altre quattro prove la Domenica occupando sempre le posizioni di vertice per la coppia formata da Enrico e Maurizio Tortone, in gara per la seconda volta con la Fiesta WRC preparata dalla Tam-Auto e gommata Pirelli che hanno permesso di chiudere in 2^ posizione assoluta la gara a soli 4 decimi di secondo dal vincitore della gara Franco Uzzeni.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Enrico Tortone: &ldquo;Sono molto contento sia per la macchina che &egrave; stata perfetta, per le gomme Pirelli e per la mia prestazione, aver chiuso con un distacco cos&igrave; ridotto da Uzzeni mi fa ben sperare per i nostri prossimi impegni dove cercheremo di migliorarci ulteriormente. Ringrazio i nostri sponsor per la fiducia che ci dimostrano e il team per la macchina sempre al top che ci mette a disposizione.&rdquo;</p>', 0, 0, 1, 0),
(61, 1, 61, 'RONDE LIBURNA TERRA PASS N 5', '', '<p><iframe src="https://drive.google.com/file/d/0B82pfxA_PxsdSWtRblNJbEs4a00/preview" height="480" width="640"></iframe>l video</p>', 0, 0, 1, 0),
(62, 1, 62, '16/05/2015 RALLYE SAINT EMILION Pierre Roché 2° Assoluto con molti rimpianti', '', '<p style="margin-bottom: 0cm;">E&rsquo; un gradito ritorno quello di Pierre Roch&eacute; su una vettura TAM-AUTO, dopo le stagioni di collaborazione 2010 e 2011 il forte pilota Francese, frequentatore abituale del Campionato Francese Rallye, ha scelto la Ford Fiesta R5 del team Novarese per &ldquo;assaggiare&rdquo; le nuove R5.</p>\n<p style="margin-bottom: 0cm;">La gara prescelta &egrave; stato il Rallye Saint Emilion gara valevole per la Coppa di Francia Rally e svoltosi sulle strade della zona di Bordeaux.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Pur essendo al debutto sulla vettura Pierre &egrave; stato autore di una gara esemplare e velocissima, vincendo 9 delle 12 PS in programma, purtroppo un inconveniente occorso sulla PS 7 gli ha negato la vittoria assoluta. Una foratura con conseguente rottura del semiasse anteriore hanno fatto perdere circa un minuto e la testa della classifica all&rsquo;equipaggio Roch&eacute;-Roch&eacute;. Riparata la vettura, Pierre, sulle ultime tre PS della gara guidando alla grande ha ridotto quasi completamente il distacco accumulato per l&rsquo;inconveniente risalendo dal 6&deg; al 2&deg; posto assoluto a soli undici secondi dal vincitore assoluto. &nbsp;</p>', 0, 0, 1, 0),
(63, 1, 63, '16/05/2015 RALLYE SAINT EMILION Pierre Roché 2° Assoluto con molti rimpianti', '', '<p style="margin-bottom: 0cm;">E&rsquo; un gradito ritorno quello di Pierre Roch&eacute; su una vettura TAM-AUTO, dopo le stagioni di collaborazione 2010 e 2011 il forte pilota Francese, frequentatore abituale del Campionato Francese Rallye, ha scelto la Ford Fiesta R5 del team Novarese per &ldquo;assaggiare&rdquo; le nuove R5.</p>\n<p style="margin-bottom: 0cm;">La gara prescelta &egrave; stato il Rallye Saint Emilion gara valevole per la Coppa di Francia Rally e svoltosi sulle strade della zona di Bordeaux.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Pur essendo al debutto sulla vettura Pierre &egrave; stato autore di una gara esemplare e velocissima, vincendo 9 delle 12 PS in programma, purtroppo un inconveniente occorso sulla PS 7 gli ha negato la vittoria assoluta. Una foratura con conseguente rottura del semiasse anteriore hanno fatto perdere circa un minuto e la testa della classifica all&rsquo;equipaggio Roch&eacute;-Roch&eacute;. Riparata la vettura, Pierre, sulle ultime tre PS della gara guidando alla grande ha ridotto quasi completamente il distacco accumulato per l&rsquo;inconveniente risalendo dal 6&deg; al 2&deg; posto assoluto a soli undici secondi dal vincitore assoluto. &nbsp;</p>', 0, 0, 1, 0),
(64, 1, 64, '16/05/2015 SOSSELLA 3° Assoluto al 48° Rally del Salento', '', '<p style="margin-bottom: 0cm;">Il forte pilota Vicentino Manuel Sossella ha scelto le difficili e anomale prove speciali del Salento per il proprio esordio stagionale nel Campionato Italiano Wrc 2015 con la Ford Fiesta WRC della TAM-AUTO gommata Michelin.</p>\n<p style="margin-bottom: 0cm;">Alla fine un buon terzo assoluto ha premiato l&rsquo;equipaggio TAM-AUTO, autore di una gara veloce e regolare dove si &egrave; badato pi&ugrave; a non fare errori e ritrovare il giusto feeling con l&rsquo;auto dopo 7 mesi di lontananza dalle corse che ha cercare exploit velocistici.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Sono contento&rdquo; esordisce cos&igrave; Sossella a fine gara &ldquo;era tanto che non correvo e il Rally del Salento non &egrave; il posto pi&ugrave; facile dove andare subito forte se non si &egrave; allenati a dovere. La macchina &egrave; andata bene e non abbiamo mai avuto problemi, ero all&rsquo;esordio anche con le gomme Michelin e il feeling &egrave; stato buono ma possiamo migliorare, senza qualche piccolo errore di guida di troppo avremmo potuto essere secondi ma va bene cos&igrave;&hellip;in questa gara i miei avversari avevano qualcosa in pi&ugrave; quindi essere arrivati terzi con un distacco non incolmabile mi va bene e fa ben sperare per la prossima gara di campionato, il Rally della Lanterna&rdquo;.</p>', 0, 0, 1, 0),
(65, 1, 65, '30/05/2015 ELE RALLYE Jaroen Swaanen 1° Assoluto con la Ford Fiesta WRC TAM-AUTO ', '', '<p style="margin-bottom: 0cm;">Per la prima volta al volante della Ford Fiesta WRC l&rsquo;equipaggio Tam-Auto composto da Jaroen Swaanen e Sander Von Barschot colgono la vittoria assoluta nel prestigioso ELE RALLY gara valida per il Campionato Olandese Rally svoltasi nelle vicinanze di Eindhoven nei giorni 28/29/30 di Maggio.</p>\n<p style="margin-bottom: 0cm;">La gara lunga e caratterizzata da continui e improvvisi cambiamenti meteo ha visto l&rsquo;equipaggio Tam-Auto dapprima in ritardo per una errata scelta di pneumatici avvenuta nelle prime fasi di gara e successivamente in rimonta per concludere vittoriosi al termine dei due giorni di gara.</p>\n<p style="margin-bottom: 0cm;">Soddisfatto il pilota all&rsquo;arrivo &ldquo;era la prima volta che usavo la Fiesta Wrc e che correvo con il team, tutto &egrave; andato alla perfezione, la macchina &egrave; stata sempre perfetta cos&igrave; come il lavoro svolto dal team. Qui in Olanda sbagliare le gomme non &egrave; cosa rara, il meteo cambia di continuo e spesso affidarsi alle previsioni non &egrave; garanzia di successo&hellip;ed &egrave; quello che &egrave; successo a noi nelle prime prove speciali, fortunatamente siamo riusciti a restare concentrati e prova dopo prova abbiamo recuperato il tempo perso.&rdquo;</p>\n<p style="margin-bottom: 0cm;"><a name="_GoBack"></a>Soddisfatto anche Gianluca Zonca &ldquo;per essere la nostra prima gara in Olanda direi che &egrave; andata molto bene, mi piace quando conosco nuove persone e realt&agrave; rallistiche diverse dalla nostra ed &egrave; proprio il caso di questa gara, ringrazio l&rsquo;equipaggio per l&rsquo;ottimo risultato, ringrazio inoltre Hans Weijs e tutto il personale del HWM, il team Olandese con il quale abbiamo collaborato per questa trasferta.&rdquo;</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&nbsp;</p>', 0, 0, 1, 0),
(66, 1, 66, '06/06/2015 RALLY LANTERNA Sossella 4° Assoluto e ritiro per Gulfi ', '', '<p style="margin-bottom: 0cm;">Gara in chiaro scuro per il team Tam-Auto al Rally della Lanterna, svoltosi nell&rsquo;entroterra sopra Genova, l&rsquo;equipaggio Sossella-Falzone a bordo della Ford Fiesta Wrc &egrave; stato rallentato da una foratura nel corso della PS n. 4 che gli faceva perdere il contatto con le posizioni di vertice e ogni possibilit&agrave; di ambire alla vittoria. Nelle successive prove Manuel perdeva ulteriore tempo a causa della rottura della leva del freno a mano rilegandolo in 4^ posizione assoluta definitivamente.</p>\n<p style="margin-bottom: 0cm;">A fine gara Sossella dichiarava &ldquo;ci aspettavamo tutt&rsquo;altro risultato da questa gara ma bisogna prendere quello che viene senza recriminare troppo, il risultato non &egrave; granch&eacute; ma in chiave campionato sono sempre punti. Peccato e speriamo di aver pagato il nostro conto negativo tutto oggi e dalla prossima gara essere pi&ugrave; in alto in classifica&rdquo;.</p>\n<p style="margin-bottom: 0cm;">&nbsp;</p>\n<p style="margin-bottom: 0cm;">Gara da dimenticare anche per Luca Gulfi che al volante della Peugeot 307 Wrc si &egrave; dovuto ritirare dopo la PS n. 3 a causa di problemi elettrici persistenti che compromettevano il perfetto funzionamento dell&rsquo;auto.</p>', 0, 0, 1, 0),
(67, 1, 67, '14/06/2015 RALLYE DES VINS MACON 3° Assoluto per l’equipaggio Beaubelique – Vial ', '', '<p style="margin-bottom: 0cm;">Gara e risultato positivo in Francia per Tam-Auto e per Jean Charles Beaubelique che al debutto sulla Fiesta WRC, coglievano un buon 3&deg; posto assoluto alla fine della gara disputata sulle colline intorno alla citt&agrave; di Macon.</p>\n<p style="margin-bottom: 0cm;">Poteva essere un 2&deg; assoluto ma una foratura lenta verificatasi nel corso della penultima PS faceva perdere una posizione all&rsquo;equipaggio Tam-Auto.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Molto soddisfatto il pilota all&rsquo;arrivo &ldquo;sono contento, sia per la mia prestazione che per come &egrave; andata la gara. La macchina &egrave; stata perfetta e se non fosse stato per la foratura che abbiamo avuto nel finale saremmo potuti arrivare secondi ma in quel momento, a causa del tempo incerto, di scorta avevamo due gomme rain e quindi abbiamo dovuto disputare l&rsquo;ultima prova speciale con tre gomme slick e una rain compromettendo assetto e performance della nostra Fiesta.&rdquo; &nbsp;</p>', 0, 0, 1, 0),
(68, 1, 68, '14/06/2015 RALLYE DES VINS MACON 3° Assoluto per l’equipaggio Beaubelique – Vial ', '', '<p style="margin-bottom: 0cm;">Gara e risultato positivo in Francia per Tam-Auto e per Jean Charles Beaubelique che al debutto sulla Fiesta WRC, coglievano un buon 3&deg; posto assoluto alla fine della gara disputata sulle colline intorno alla citt&agrave; di Macon.</p>\n<p style="margin-bottom: 0cm;">Poteva essere un 2&deg; assoluto ma una foratura lenta verificatasi nel corso della penultima PS faceva perdere una posizione all&rsquo;equipaggio Tam-Auto.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Molto soddisfatto il pilota all&rsquo;arrivo &ldquo;sono contento, sia per la mia prestazione che per come &egrave; andata la gara. La macchina &egrave; stata perfetta e se non fosse stato per la foratura che abbiamo avuto nel finale saremmo potuti arrivare secondi ma in quel momento, a causa del tempo incerto, di scorta avevamo due gomme rain e quindi abbiamo dovuto disputare l&rsquo;ultima prova speciale con tre gomme slick e una rain compromettendo assetto e performance della nostra Fiesta.&rdquo; &nbsp;</p>', 0, 0, 1, 0);
INSERT INTO `tamautoitemsdetail` (`id`, `lang`, `item`, `itemTitle`, `itemAbstract`, `itemText`, `sign`, `homepage`, `active`, `del`) VALUES
(69, 1, 69, '20/06/2015 RALLY DELLA MARCA 2° Assoluto al fotofinish per Sossella - Falzone', '', '<p style="margin-bottom: 0cm;">Doveva essere una gara d&rsquo; attacco per l&rsquo;equipaggio Tam-Auto al Rally della Marca e cos&igrave; &egrave; stato. In testa alla classifica sin dall&rsquo;inizio e restandoci fino alla PS 7 Sossella ha lottato per tutta la gara con avversari agguerriti e determinati, tutti racchiusi in pochi secondi. Il meteo incerto e molto variabile ha ulteriormente complicato le cose, ciononostante Manuel e Lele sono sempre stati molto veloci e concentrati chiudendo la gara in seconda posizione assoluta a soli 2&rdquo;e 8 decimi dal vincitore Porro.</p>\n<p style="margin-bottom: 0cm;">&ldquo;Peccato non aver vinto&rdquo; esordisce cos&igrave; Sossella &ldquo;ma quando le gare sono cos&igrave; combattute non c&rsquo;&egrave; molto da recriminare, specie poi con le condizioni meteo che abbiamo trovato nei due giorni della gara&hellip;pioggia, sole, di nuovo pioggia e ancora sole. Le scelte delle gomme non erano per niente facili e scontate ma il team ha lavorato benissimo senza mai sbagliare. Dopo la gara opaca di Genova avevamo bisogno di un buon risultato che ci rilanciasse in campionato e l&rsquo;abbiamo trovato qui, la vittoria sarebbe stata il top&hellip;ma bene cos&igrave;.&rdquo;</p>\n<p style="margin-bottom: 0cm;">A testimonianza della competitivit&agrave; della gara il fatto che sono stati ben 5 gli equipaggi che hanno iscritto il proprio nome in vetta alla classifica di almeno una prova speciale.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Dopo la quarta gara del Campionato Italiano WRC 2015 Sossella occupa la terza posizione assoluta in classifica generale e la seconda posizione nel trofeo Michelin. Determinanti, per sancire il vincitore del campionato 2015 saranno le prossime e ultime due gare a calendario, entrambe con coefficiente 1,5 il Rally San Martino di Castrozza del 11 e 12 Settembre e il successivo Rally Aci Como del 16 e 17 Ottobre.</p>', 0, 0, 1, 0),
(70, 1, 70, 'I TORTONE VINCONO IL 2° RALLY SHOW SAN MARINO ', '', '<p style="margin-bottom: 0cm; line-height: 100%; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Helvetica, serif;"><span>Una gara nella gara, cos&igrave; si pu&ograve; definire il Rally Show San Marino, dove sulla PS interamente su asfalto compresa nella gara CIR si disputa snodata su tre passaggi una gara indipendente e spettacolare aperta a tutte le vetture, comprese le Wrc, che ha visto al via 50 iscritti. </span></span></p>\n<p style="margin-bottom: 0cm; line-height: 100%; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Helvetica, serif;"><span>Al termine dei tre passaggi percorsi tra i cinquanta equipaggi iscritti i pi&ugrave; veloci sono stati i nostri portacolori Maurizio e Enrico Tortone che hanno vinto la gara al volante della Ford Fiesta WRC.</span></span></p>\n<p style="margin-bottom: 0cm; line-height: 100%; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Helvetica, serif;"><span>Maurizio era praticamente al debutto con la Fiesta Wrc ma non si &egrave; fatto intimorire e con una gara sempre in crescendo  senza commettere errori ha saputo regolare gli avversari e cogliere la vittoria. All&rsquo;arrivo Maurizio cos&igrave; esprimeva la sua felicit&agrave;: &ldquo;erano anni che non disputavo una PS vera e propria e sono stracontento del risultato della macchina e del team, sono venuto qui per divertirmi e torno a casa con la coppa del vincitore che altro pretendere?&rdquo;</span></span></p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&nbsp;</p>', 0, 0, 1, 0),
(71, 1, 71, '11/07/2015 I TORTONE VINCONO IL 2° RALLY SHOW SAN MARINO', '', '<p style="margin-bottom: 0cm; line-height: 100%; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Helvetica, serif;"><span>Una gara nella gara, cos&igrave; si pu&ograve; definire il Rally Show San Marino, dove sulla PS interamente su asfalto compresa nella gara CIR si disputa snodata su tre passaggi una gara indipendente e spettacolare aperta a tutte le vetture, comprese le Wrc, che ha visto al via 50 iscritti. </span></span></p>\n<p style="margin-bottom: 0cm; line-height: 100%; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Helvetica, serif;"><span>Al termine dei tre passaggi percorsi tra i cinquanta equipaggi iscritti i pi&ugrave; veloci sono stati i nostri portacolori Maurizio e Enrico Tortone che hanno vinto la gara al volante della Ford Fiesta WRC.</span></span></p>\n<p style="margin-bottom: 0cm; line-height: 100%; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><span style="font-family: Helvetica, serif;"><span>Maurizio era praticamente al debutto con la Fiesta Wrc ma non si &egrave; fatto intimorire e con una gara sempre in crescendo  senza commettere errori ha saputo regolare gli avversari e cogliere la vittoria. All&rsquo;arrivo Maurizio cos&igrave; esprimeva la sua felicit&agrave;: &ldquo;erano anni che non disputavo una PS vera e propria e sono stracontento del risultato della macchina e del team, sono venuto qui per divertirmi e torno a casa con la coppa del vincitore che altro pretendere?&rdquo;</span></span></p>\n<p>&nbsp;</p>', 0, 0, 1, 0),
(72, 1, 72, '18/07/2015 rally del casentino  ', '', '<p style="margin-bottom: 0cm;"><a name="_GoBack"></a>Dopo aver disputato una gara tutta all&rsquo;attacco ed essere stato in testa alla classifica assoluta per tutta la gara il forte pilota Sanmarinese, per l&rsquo;occasione in coppia con Flavio Zanella, deve accontentarsi della terza posizione finale a causa di una foratura patita nel corso dell&rsquo;ultima prova speciale disputata in notturna. Colombini era al debutto sulla Fiesta Wrc della Tam-Auto dopo un periodo di lontananza dalle corse.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Peccato&rdquo; esordisce Colombini &ldquo;il feeling con la macchina &egrave; stato fin da subito ottimo e senza la foratura avremmo sicuramente vinto, il Casentino &egrave; una gara che mi piace molto e quest&rsquo;anno con 20 auto Wrc al via era particolarmente stuzzicante esserci e lottare per la vittoria&hellip;evidentemente non era il mio anno buono. La macchina &egrave; sempre stata al top, sicuramente conoscerla meglio mi avrebbe permesso di andare ancora pi&ugrave; forte&rdquo;</p>', 0, 0, 1, 0),
(73, 1, 73, '18/07/2015 rally del casentino  ', '', '<p style="margin-bottom: 0cm;"><a name="_GoBack"></a>Dopo aver disputato una gara tutta all&rsquo;attacco ed essere stato in testa alla classifica assoluta per tutta la gara il forte pilota Sanmarinese, per l&rsquo;occasione in coppia con Flavio Zanella, deve accontentarsi della terza posizione finale a causa di una foratura patita nel corso dell&rsquo;ultima prova speciale disputata in notturna. Colombini era al debutto sulla Fiesta Wrc della Tam-Auto dopo un periodo di lontananza dalle corse.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Peccato&rdquo; esordisce Colombini &ldquo;il feeling con la macchina &egrave; stato fin da subito ottimo e senza la foratura avremmo sicuramente vinto, il Casentino &egrave; una gara che mi piace molto e quest&rsquo;anno con 20 auto Wrc al via era particolarmente stuzzicante esserci e lottare per la vittoria&hellip;evidentemente non era il mio anno buono. La macchina &egrave; sempre stata al top, sicuramente conoscerla meglio mi avrebbe permesso di andare ancora pi&ugrave; forte&rdquo;</p>', 0, 0, 1, 0),
(74, 1, 74, '25/07/2015 Rally Vyskov  porta bene l’europa alla Tam-auto', '', '<p style="margin-bottom: 0cm;">Un gradito ritorno quello di Thomas Kostka, il fortissimo pilota della Repubblica Ceca, che dopo aver vinto con la Tam-Auto il campionato Rally Sprint in Repubblica Ceca nel 2012 al volante di una Citroen C4, quest&rsquo;anno ha scelto ancora il team di Novara per il rally di casa e come nel 2012 anche questa volta Thomas non ha perso l&rsquo;occasione e ha vinto facilmente la gara vincendo, al volante della Fiesta Wrc by Tam-Auto, tutte le prove speciali in programma.</p>', 0, 0, 1, 0),
(75, 1, 75, '29/08/2015 rally COEUR DE FRANCE Pierre roche 2° assoluto', '', '<p style="margin-bottom: 0cm;"><a name="_GoBack"></a>Dopo la Fiesta Tam-auto in versione R5 per questa gara Pierre Roche ha scelto la pi&ugrave; performante versione della Ford la Wrc. Una gara su strade velocissime e particolarmente insidiose per la pioggia caduta abbondante i giorni precedenti la gara e con un plateau di avversari di tutto rispetto con 8 auto Wrc al via e poco meno di classe R5. La coppia Roch&eacute;-Roch&eacute; (figlio e madre) chiudono la gara al 2&deg; posto assoluto alle spalle di una vettura gemella. Sono stati 4 gli scratch su 9 prove disputate per l&rsquo;equipaggio del team Novarese ma non sono stati sufficienti per ottenere la vittoria assoluta dopo una bagarre durata tutta la gara con il vincitore, il pilota locale Bronson. &nbsp;</p>', 0, 0, 1, 0),
(76, 1, 76, '12/09/2015 RALLY SAN MARTINO SOSSELLA CHIUDO AL TERZO POSTO', '', '<p style="margin-bottom: 0cm;">Con il Rally San Martino iniziava la fase finale e pi&ugrave; delicata del Campionato Italiano Wrc dove il forte pilota Vicentino in coppia con il navigatore Varesino Lele Falzone a bordo della Ford Fiesta Wrc della Tam-Auto sono in lotta per la vittoria finale.</p>\n<p style="margin-bottom: 0cm;">In questa gara, con coefficiente 1 e mezzo come per la prossima e ultima prova del campionato, era necessario non sbagliare e cercare di prendere pi&ugrave; punti possibile per poi giocarsi il titolo sulle strade del Rally Aci Como. Manuel e Lele non hanno sbagliato niente e visto la giornata non propriamente favorevole al nostro pacchetto tecnico hanno preferito non prendere particolari rischi chiudendo al terzo posto assoluto. Con questo risultato i nostri portacolori si presenteranno a Como occupando la seconda posizione in classifica generale di campionato e in grado di ambire alla vittoria finale.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&ldquo;Oggi non &egrave; stata la nostra migliore giornata&rdquo; il commento di Manuel a fin gara &ldquo;non eravamo in palla, speravamo fosse pi&ugrave; freddo invece le temperature sono state miti, in queste condizioni e con il grip normale delle prove speciali le nostre gomme pagano qualcosa agli avversari. Aver portato a casa un podio e quindi aver preso punti importanti per il campionato &egrave; gi&agrave; molto positivo. A Como ci giochiamo il campionato&hellip;noi siamo pronti!&rdquo;</p>', 0, 0, 1, 0),
(77, 1, 77, '19/09/2015 Rally Citta’ di Torino', '', '<p style="margin-bottom: 0cm;">Il nostro portacolori Enrico Tortone in coppia con il Ligure Danilo Roggerone chiude 5&deg; assoluto nella gara di casa al volante della Fiesta in versione Rrc.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">Dopo pi&ugrave; di un anno di lontananza dalle prove speciali classiche Enrico ha scelto le difficili strade delle valli circostanti Torino per il suo ritorno nella specialit&agrave;. La gara non stava andando male ma causa una toccata nelle fasi iniziali della gara e una successiva foratura che facevano perdere parecchi secondi al nostro equipaggio consigliavano di mantenere un passo meno aggressivo per fare pi&ugrave; esperienza possibile.</p>', 0, 0, 1, 0),
(78, 1, 78, '19/09/2015 RALLY ACI COMO VINCE SOSSELLA E SI LAUREA CAMPIONE ITALIANO WRC', '', '<p style="margin-bottom: 0cm;">Il nostro equipaggio Sossella &ndash; Falzone al volante della Ford Fiesta Wrc vincono la gara Lariana e si aggiudicano il titolo Italiano della specialit&agrave;.</p>\n<p style="margin-bottom: 0cm;">Con una partenza tutta di attacco Manuel Sossella si &egrave; costruito un vantaggio prezioso che nella seconda parte della gara gli ha permesso di amministrare il ritorno degli avversari.</p>\n<p style="margin-bottom: 0cm;"><a name="_GoBack"></a>&ldquo;Non ho parole per esprimere la mia gioia, aver vinto per il secondo anno questa gara ed essermi laureato campione Italiano Wrc &egrave; il massimo. La nostra gara &egrave; stata tatticamente perfetta, abbiamo indovinato il set up della vettura e per questo ringrazio il team, siamo partiti forte e questo ci ha permesso di portarci subito in testa con un vantaggio importante. Nel corso della PS 4 quando ho visto il leader del campionato fermo mi sono deconcentrato un attimo e ho commesso un piccolo errore perdendo secondi importanti, per fortuna abbiamo mantenuto il sangue freddo e abbiamo amministrato il vantaggio che restava. Alla fine, pur guardando di non prendere rischi che avrebbero compromesso il campionato siamo riusciti a mantenere la testa della classifica e vincere la gara&hellip;che dire, meglio di cos&igrave;. E&rsquo; stato un campionato difficile e combattuto, il nostro punto di forza &egrave; stata la costanza di rendimento e la totale affidabilit&agrave; della vettura, siamo sempre andati a punti in tutte le gare disputate e questo la dice lunga sull&rsquo;ottimo lavoro svolto dalla Tam-Auto sulla macchina e quello altrettanto buono mio e di Lele sulle prove speciali&rdquo;.</p>\n<p>&nbsp;</p>\n<p style="margin-bottom: 0cm;">&nbsp;</p>', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautolang`
--

CREATE TABLE IF NOT EXISTS `tamautolang` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `lang` text,
  `def` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `tamautolang`
--

INSERT INTO `tamautolang` (`id`, `iso`, `lang`, `def`, `active`) VALUES
(1, 'it', 'Italiano', 1, 1),
(2, 'en', 'English', 0, 0),
(3, 'fr', 'Français', 0, 0),
(4, 'de', 'Deutsch', 0, 0),
(5, 'es', 'Español', 0, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `tamautologin`
--

CREATE TABLE IF NOT EXISTS `tamautologin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` text NOT NULL,
  `psw` text NOT NULL,
  `name` text,
  `surname` text,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `permission` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `tamautologin`
--

INSERT INTO `tamautologin` (`id`, `user`, `psw`, `name`, `surname`, `admin`, `permission`, `active`, `del`) VALUES
(1, 'sd34rff56', '9ba102691155d2794e147898949b01ea28fdb030', 'Super', 'Admin', 1, 0, 1, 0),
(2, 'msoft', '1b7f3f17b243eac5cf402202f963ec457a9e069a', 'Supporto', 'MSoft', 1, 0, 1, 0),
(3, 'tam', '@uto2015', 'Tam', 'Auto', 1, 0, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
