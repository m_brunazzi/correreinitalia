<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Etichette extends BLM_Auth_Controller {
	
        
	function __construct(){
		parent::__construct();
                
                
          
                
	}

        
      
	public function index()
	{
		
		$this->view_data["content_comp"]  = "admin/comp_etichette_params";
        $this->view_data["page_title"]  = "Etichette";
        $this->view_data["destination_uri"]  = base_url().'admin/etichette/crea';
      
      
                
                // Carico le iniziali delle ragioni sociali
                
                $sql = "SELECT id, cognome, CONCAT(cognome, ' ', nome) AS nome_intero 
				FROM lido_contatti 
				WHERE attivo = 1
				ORDER BY cognome ASC";
                $query = $this->db->query($sql);
                $result = $query->result();
				
				$clienti = Array('' => '---');
				foreach($result as $item){
					$clienti[$item->cognome] = $item->nome_intero;
				
				}
                
                
                $this->view_data["clienti"]  = $clienti;
		
		$this->render();
	}
        
	function crea(){
		require_once(APPPATH.'libraries/fpdf/fpdf.php');
		
		
		 // Carico le iniziali delle ragioni sociali
                
		$sql = "SELECT * FROM lido_contatti ";
		if($this->input->post('parti_da'))
			$sql .= " WHERE cognome >= '".$this->input->post('parti_da')."'";
		$sql .= " WHERE attivo = 1 ORDER BY cognome ASC";
		$query = $this->db->query($sql);
		$result = $query->result();
		
		
		if($this->input->post('w') === false ||
			$this->input->post('h') === false ||
			$this->input->post('lpp') === false ||
			$this->input->post('lpr') === false ||
			$this->input->post('um') === false ||
			$this->input->post('lm') === false
			)
		$this->handle_error('Parametri non validi');
		
		$w = $this->input->post('w');
		$h = $this->input->post('h');
		$lpp = $this->input->post('lpp');
		$lpr = $this->input->post('lpr');
		$um = $this->input->post('um');
		$lm = $this->input->post('lm');
		
		
		$pdf = new FPDF();
		
		$pdf->SetMargins($lm, $um);
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(false);
		
		$font  = "Arial";
		
		$fs =  $this->input->post('fs');
		if(!$fs)
			$fs = 13;
		$pdf->SetFont($font, "B", $fs);
		$counter = 0;
		
		foreach($result as $item){
			if($item->cognome != '' && $item->indirizzo != ''){
			
				$etichetta = "\n";
				if($item->nome != '')
					$etichetta .= $item->cognome.' '.$item->nome;
				else
					$etichetta .= $this->input->post('lnn').' '.$item->cognome;
					
				$etichetta .= "\n".$item->indirizzo."\n".$item->cap." ".$item->comune;
				
				if($item->provincia != '')
					$etichetta .= " (".strtoupper($item->provincia).")";
			
				
				
				$x = ($counter % $lpr) * $w;
				$y = floor(($counter) / $lpr) * $h;
				
				$pdf->SetXY($x + 5, $y); // aggiungo 5mm di margine sinistro
				//$pdf->SetY($y);
				
				$pdf->MultiCell($w,$h / 5,iconv("UTF-8", "ISO-8859-1",$etichetta));
				
				//echo "$counter | $x | $y | $etichetta <br/>";
				
				$counter++;
				
				if($counter != 0 && $counter == $lpp){
							
					$pdf->AddPage();
					$counter = 0;
				}
			}
		}
		$pdf->Output();
	
	}
        
	
	private function render(){
		
		$this->load->view('admin/base_template', $this->view_data);
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */