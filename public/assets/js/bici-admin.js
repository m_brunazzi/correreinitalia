 $(document).ready(function(){
     
    // Attivo l'editor html 
    $('.html-editor').redactor({
        imageUpload: BASE_URL + '/rpc/redactor/imageUpload',
        imageGetJson: BASE_URL + '/rpc/redactor/imageBrowse',
        minHeight: 300,
        pastePlainText: true

    });
    
    // Attivo il calendario per le date
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'it',
        todayHighlight: true
    });
    
    // chiedo conferma per le azioni triggerate dai pulsanti rossi
    $('.btn-danger').click(function(e){
        e.preventDefault();
        if(confirm('Procedere con l\'operazione selezionata?')){
            var url = $(e.target).attr('href');
            window.location.href = url;
        }
    });
    
   
  
    $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
        var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

        if (token) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
        }
    });
    
    
    
});