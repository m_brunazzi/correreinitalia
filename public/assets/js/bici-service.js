var BiCi = new function() {
    
    this.baseUrl = '';
    this.modelPictures = [];
    this.modelAttachments = [];
    this.loading = false;
    this.onSuccess = null;
    
    
    this.init = function(options){
        this.baseUrl = options.baseUrl || '';
        this.onSuccess = options.onSuccess || null
        
    }
        
    
    this.deletePicture = function(params){
        
        if(params.pictureId === undefined)
            return false;
        
        if(confirm('Procedere con la cancellazione dell\'immagine?')){
            this.loading = true;
            $.ajax({
                url: this.baseUrl + '/rpc/form/delete/picture',
                data:{'picture_id' : params.pictureId},
                success: function(response){
                    this.loading = false;
                    params.onSuccess(response);
                },
                dataType: 'json'
            });
        }
    }
    
    
    this.loadPictures = function(params){
        
        if(params.pictureableId === undefined)
            return false;
        
        if(params.pictureableType === undefined)
            return false;
        
        this.loading = true;
        $.ajax({
                url: this.baseUrl + '/rpc/form/load/pictures',
                data:{'pictureable_type' : params.pictureableType, 'pictureable_id' : params.pictureableId},
                success: function(response){
                    this.loading = false;
                    this.modelPictures = response.data || null; 
                    params.onSuccess(response);
                },
                dataType: 'json'
        });
    }
    
    this.setMainPicture = function(params){
        
        if(params.pictureId === undefined)
            return false;
        
        this.loading = true;
        $.ajax({
                url: this.baseUrl + '/rpc/form/setMain/picture',
                data:{'picture_id' : params.pictureId, 'picture_main' : params.pictureMain},
                success: function(response){
                    this.loading = false;
                    this.modelPictures = response.data || null; 
                    params.onSuccess(response);
                },
                dataType: 'json'
        });
    }
    
    this.setOrderPicture = function(params){
        
        if(params.record_id === undefined)
            return false;
        
        this.loading = true;  
        $.ajax({
                url: this.baseUrl + '/rpc/form/setOrder/picture',
                data:{'record_id' : params.record_id, 'ids' : params.ids},
                success: function(response){
                    this.loading = false;
                    params.onSuccess(response);
                },
                dataType: 'json'
        });
    }
    
    
    
    this.deleteAttachment = function(params){
        
        if(params.attachmentId === undefined)
            return false;
        
        this.loading = true;
        if(confirm('Procedere con la cancellazione dell\'allegato?')){
            $.ajax({
                url: this.baseUrl + '/rpc/form/delete/attachment',
                data:{'attachment_id' : params.attachmentId},
                success:function(response){
                    this.loading = false;
                    params.onSuccess(response);
                }, 
                dataType: 'json'
            });
        }
    }
    
    
    this.loadAttachments = function(params){
        
        if(params.attachmentableId === undefined)
            return false;
        
        if(params.attachmentableType === undefined)
            return false;
        this.loading = true;
        $.ajax({
                url: this.baseUrl + '/rpc/form/load/attachments',
                data:{'attachmentable_type' : params.attachmentableType, 'attachmentable_id' : params.attachmentableId},
                success: function(response){
                    this.loading = false;
                    this.modelPictures = response.data || null; 
                    params.onSuccess(response);
                },
                dataType: 'json'
        });
    }
    
    this.setOrderAttachment = function(params){
        
        if(params.record_id === undefined)
            return false;
        
        this.loading = true;  
        $.ajax({
                url: this.baseUrl + '/rpc/form/setOrder/attachment',
                data:{'record_id' : params.record_id, 'ids' : params.ids},
                success: function(response){
                    this.loading = false;
                    params.onSuccess(response);
                },
                dataType: 'json'
        });
    }
};
