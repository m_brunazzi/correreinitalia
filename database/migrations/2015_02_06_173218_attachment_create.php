<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttachmentCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_attachment', function(Blueprint $table)
		{
			$table->increments('id');
                        
                        $table->string('label');
                        $table->string('filename')->nullable();
                        $table->string('original_filename')->nullable();
                        
                        $table->text('title')->nullable();
                        $table->text('description')->nullable();
                        $table->string('online')->nullable()->default(true);
                        $table->string('extension')->nullable();
                        $table->integer('size')->nullable();
                        $table->integer('download_count')->nullable()->default(0);
                        $table->integer('order')->nullable()->default(0);
                        
			$table->integer('attachmentable_id');
			$table->string('attachmentable_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_attachment');
	}

}
