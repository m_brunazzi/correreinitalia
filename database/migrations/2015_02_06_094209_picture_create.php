<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PictureCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_picture', function(Blueprint $table)
		{
                    $table->increments('id');
                    $table->string('label');
                    $table->string('filename');
                    
                    $table->integer('pictureable_id');
                    $table->string('pictureable_type');
                    
                    $table->integer('type_id')->nullable();
                    $table->string('title')->nullable();
                    $table->boolean('online')->nullable()->default(true);
                    $table->boolean('main')->nullable()->default(false);
                    $table->integer('order')->nullable();
                    
                    
                    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_picture');
	}

}
