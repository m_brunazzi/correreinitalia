<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CarouselCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_carousel', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('label', 128);
			$table->text('name')->nullable();
			$table->text('summary')->nullable();
			$table->string('image')->nullable();
			$table->boolean('online')->nullable()->default(true);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_carousel');
	}

}
