<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LanguageCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_language', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('language');
			$table->boolean('online')->default(false);
			$table->string('code', 3)->nullable();
			$table->string('icon')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_language');
	}

}
