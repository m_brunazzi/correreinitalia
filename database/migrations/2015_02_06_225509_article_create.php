<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticleCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_article', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('label', 128);
			$table->text('name')->nullable();
			$table->text('summary')->nullable();
			$table->text('description')->nullable();
			$table->date('date')->nullable();
			$table->boolean('online')->nullable()->default(true);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_article');
	}

}
