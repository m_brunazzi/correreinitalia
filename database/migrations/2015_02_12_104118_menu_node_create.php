<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuNodeCreate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blm_menu_node', function(Blueprint $table)
		{
			$table->increments('id');
                        $table->integer('menu_id');
                        $table->integer('language_id');
			$table->string('title');
			$table->text('url')->nullable();
			$table->boolean('url_blank_page')->nullable()->default(false);
			$table->integer('order')->nullable()->default(null);
			$table->integer('parent_id')->nullable()->default(null);
                        $table->text('extra')->nullable();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blm_menu_node');
	}

}
