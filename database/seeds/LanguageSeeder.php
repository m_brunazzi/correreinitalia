<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LanguageSeeder extends Seeder {

	public function run()
        {
            
            DB::table('blm_language')->truncate();
            
            $language = new Language;
            $language->language = 'Italiano';
            $language->code = 'it';
            $language->online = true;
            $language->save();
        }

}
