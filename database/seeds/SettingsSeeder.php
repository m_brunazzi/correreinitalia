<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SettingsSeeder extends Seeder {

	public function run()
        {
            Config::clearDb();
            
            Config::store('bloom.admin_email', 'dev@bl8m.it');
            Config::store('bloom.admin_items_per_page', '20');
            Config::store('bloom.admin_thumbnail_height', '75');
            Config::store('bloom.admin_thumbnail_width', '75');
            Config::store('bloom.public_images_upload_folder', 'upload/images/');
            Config::store('bloom.public_files_upload_folder', 'upload/files/');
            Config::store('bloom.cache_folder', 'upload/cache/');
            Config::store('bloom.session_language_var', 'BLOOM_VAR_LANGUAGE');
            Config::store('bloom.site_name', 'Bloom CMS Base Site');
            Config::store('bloom.website_contact_email', 'dev@bl8m.it');
            
            
        }

}
