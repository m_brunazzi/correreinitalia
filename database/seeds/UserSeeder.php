<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name= 'Bloom Design';
        $user->email= 'dev@bl8m.it';
        $user->password= bcrypt('dev');
        $user->active= true;
        $user->admin= true;
        $user->save();
    }
}
