<!doctype html>
<html lang="en">
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        {!! Meta::tag('title'); !!}
        {!! Meta::tag('description'); !!}
        {!! Meta::tag('image'); !!}
        
        {!! Meta::twitterCard() !!}
        {!! Meta::openGraph() !!}
        
        <link rel="icon" type="image/png" href="{{ asset('assets/images/favicon.png') }}">
        <title>@yield('title') {{{ Config::get('bloom.site_name') }}}</title>

        @if(isset($head_assets['css']) && is_array($head_assets['css']) && count($head_assets['css']) > 0)
            @foreach(@$head_assets['css'] as $item)
                <link href="{{ URL::asset($item) }}" rel="stylesheet">
            @endforeach
        @endif

        @if(isset($head_assets['js']) && is_array($head_assets['js']) && count($head_assets['js']) > 0)
            @foreach(@$head_assets['js'] as $item)
                <script src="{{ URL::asset($item) }}"></script>
            @endforeach
        @endif

        <script type="text/javascript">
        var BASE_URL =  '{{ URL::to('/') }}';
    
        </script>
        @section('head')
        @show
    </head>
    <body>
        @include('www._partials.navbar')
        @include('www._partials.social_login')
        
        @yield('main')
        
        @include('www._partials.footer')
    </body>
</html>