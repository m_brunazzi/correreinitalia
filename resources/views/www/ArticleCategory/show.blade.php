@extends('www._layouts.default')

@section('main')

    @if($category)

        {{-- TITLE --}}
        <h1>{{ $category->name }}</h1>

        {{-- DESCRIPTION --}}
        {!! $category->description    !!}
        
        @if(count($category->article) > 0)
            <ul>
            @foreach($category->article as $item)
                <li>
                    <a href="{{ route('public.article.show', Array($item->id, str_slug($item->name))) }}">{{ $item->name }}</a>
                </li>
            @endforeach
            </ul>
        @endif


        {{-- PICTURES --}}
        @if(count($category->picture) > 0)

            @for($i = 0; $i < count($category->picture); $i++)
            <a href="{{ asset($category->picture[$i]->filename) }}">
                <img src="{{ Image::resize($category->picture[$i]->filename, 200, 200, true) }}" alt=""  class="img-responsive"/>
            </a>
            @endfor

        @endif

        {{-- ATTACHMENTS --}}
        @if(count($category->attachment) > 0)
        <ul>
            @for($i = 0; $i < count($category->attachment); $i++)
            <li>
                <a href="{{ asset($category->attachment[$i]->filename) }}">
                    {{ $category->attachment[$i]->filename }}
                </a>
            </li>
            @endfor
        </ul>
        @endif


    @endif

@stop
