 
@if(isset($languages) && count($languages) > 0)
    <ul class="language_selection">
    @foreach($languages as $item)
        <li>
        @if($item->id != $language_current)
            <a href="?lang={{ $item->id }}">
            <img class="language-flag" src="{{ asset($item->icon) }}" alt="{{ $item->language }}"/>
            </a>
        @else
            <img class="language-flag active" src="{{ asset($item->icon) }}" alt="{{ $item->language }}"/>
        @endif
        </li>   
    @endforeach
    </ul>
@endif