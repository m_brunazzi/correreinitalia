<div class="pull-right">
@if(!$user)

    <a class="btn btn-primary" href="{{ route('facebook.login') }}">Login with Facebook</a>

@else
    {{ $user->email }} - <a href="{!! route('rpc.logout') !!}">Esci</a>
@endif
</div>
