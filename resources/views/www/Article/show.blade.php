@extends('www._layouts.default')

@section('main')

    @if($article)

        {{-- TITLE --}}
        <h1>{{ $article->name }}</h1>

        {{-- DESCRIPTION --}}
        {!! $article->description    !!}


        {{-- PICTURES --}}
        @if(count($article->picture) > 0)

            @for($i = 0; $i < count($article->picture); $i++)
            <a href="{{ asset($article->picture[$i]->filename) }}">
                <img src="{{ Image::resize($article->picture[$i]->filename, 200, 200, true) }}" alt=""  class="img-responsive"/>
            </a>
            @endfor

        @endif

        {{-- ATTACHMENTS --}}
        @if(count($article->attachment) > 0)
        <ul>
            @for($i = 0; $i < count($article->attachment); $i++)
            <li>
                <a href="{{ asset($article->attachment[$i]->filename) }}">
                    {{ $article->attachment[$i]->filename }}
                </a>
            </li>
            @endfor
        </ul>
        @endif


    @endif

@stop
