@extends('www._layouts.default')


@section('main')

  
  
<div class="error-404-container">
    <div class="container">
        
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>Pagina non trovata</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 content page-not-found text-center">
                
                <div class="icon-huge"><span class="glyphicon  glyphicon-thumbs-down"></span></div>
                
                <p class="text-center"><strong>Siamo spiacenti, il contenuto cercato non è disponibile</strong><br/>
                <em>{{ Session::get('url_404') }}</em>
                </p>
            </div>
        </div>
           
    
    </div>
</div>
    
@stop
