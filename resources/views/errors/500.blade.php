@extends('www._layouts.default')

@section('main')
  
<div class="error-500-container">
    <div class="container">
        
        <div class="row">
            <div class="col-sm-12">
                <h1>Errore!</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 content page-not-found text-center">
                
                <div class="icon-huge"><span class="glyphicon glyphicon-ban-circle"></span></div>
                
                <p class="text-center">Si è verificato un errore. Vi invitiamo a riprovare più tardi o a contattarci.</p>
                @if(isset($exception))
                <div class="alert alert-danger">
                    {{ $exception->getMessage() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
    
@stop
