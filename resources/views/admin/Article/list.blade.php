@extends('admin._layouts.default')
 
@section('main')

<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    
    <li class="active">Articoli</li>
</ol>


 
<h1>Articoli - Elenco</h1>

<div class="well">
    
    <div class="row">
        <div class="col-sm-4">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Article.create')!!}" class="btn btn-primary btn-sm">Nuovo Record</a>
        </div>
        <div class="col-sm-8">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.Article.index', 'method' => 'GET', 'class' => 'form-inline'))!!}
                <div class="form-group">
                    {!! Form::select('article_category_id', $options_category, Input::get('article_category_id'), Array('class' => 'form-control input-sm')) !!}
                </div>
                <div class="form-group">
                    <div class="input-group">
                    <input type="text" name="search" class="form-control input-sm" value="{!! $search !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="submit">Cerca</button>
                        </span>
                    </div>
                </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover ">
        <thead>
            <tr>
                <th>ID</th>
                <th>Online</th>
                <th>Etichetta</th>
                <th>Categorie</th>
                
                
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            <td>{!! $row->online?'<span class="label label-icon label-success"><span class=" glyphicon glyphicon-ok-circle"></span></span>':'<span class="label label-danger label-icon"><span class="glyphicon glyphicon-remove-circle"></span></span>'!!}</td>
            <td><strong>{!! $row->label !!}</strong></td>
            <td>
                @foreach($row->category as $item)
                    <a href="{!! route('admin.Article.index') !!}?article_category_id={!! $item->id !!}"><span class="label label-primary">{!! $item->label !!}</span></a>
                @endforeach
                
            </td>
            <td class="text-right">
                <a href="{!!URL::route('admin.Article.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.Article.destroy', $row->id), 'method' => 'delete', 'class' => 'form-delete')) !!}
                        <button type="submit" href="{!! URL::route('admin.Article.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-default">Nessun record trovato</div>
@endif


@stop