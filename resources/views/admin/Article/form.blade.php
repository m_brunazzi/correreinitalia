@extends('admin._layouts.content_form')
 
@section('form_content')
 



@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Article.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Article.store', 'method' => 'POST')) !!}
@endif
          
 
            <div class="form-group">
                {!! Form::label('label', 'Etichetta') !!}
                {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
            </div>
            
            <div class="form-group">
            {!! Form::label('name', 'Nome') !!}
            {!! Language::multilanguageWidget('name',  Language::decodeTranslations(@$record->name), null, false) !!}
            </div>

            <div class="form-group">
            {!! Form::label('summary', 'Sottotitolo') !!}
            {!! Language::multilanguageWidget('summary',  Language::decodeTranslations(@$record->summary), null, false) !!}
            </div>
            
            <div class="form-group">
            {!! Form::label('name', 'Descrizione') !!}
            {!! Language::multilanguageWidget('description',  Language::decodeTranslations(@$record->description)) !!}
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('date', 'Date') !!}
                    {!! Form::text('date', null, Array('class'=> 'form-control datepicker' ,'placeholder' => 'label')) !!}      
                </div>
                <div class="col-sm-3">
                    <div class="checkbox">
                        <br/>
                        <label for="online">{!! Form::checkbox('online', 1) !!} Online</label>
                    </div>
                </div>
            </div>
            

            <div class="panel panel-default panel-form">
                <div class="panel-body">
                    <label>Categorie</label>
                    <div class="row">

                    <?php
                    $category_checked = Array();
                    if(null !== (@$record->category)){
                        foreach(@$record->category as $item){
                            $category_checked[] = $item->id;    
                        }
                    }

                    ?>
                    @if(count($categories)> 0)

                        <?php $i = 0 ?>
                        @foreach($categories as $row)
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                <div class="checkbox">
                                    <label >
                                        {!! Form::checkbox('category['.$row->id.']', $row->id, in_array($row->id,$category_checked)) !!}
                                        <span>{!! $row->label !!}</span>
                                    </label>
                                </div>
                            </div>
                            <?php $i++; ?>
                        @endforeach
                        
                    @endif
                
                    </div>
                </div>
            </div>

            @include('admin._partials.tags_manager')
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop
