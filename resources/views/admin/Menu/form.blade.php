@extends('admin._layouts.default')
 
@section('main')
 




  @if(@$form['breadcrumbs'])
<?php $i = 0 ?>
<ol class="breadcrumb">

    @foreach($form['breadcrumbs'] as $key => $value)
        @if(++$i == count($form['breadcrumbs']))
            <li class="active">{!! $key !!}</li>
        @else
            <li ><a href="{!! $value !!}">{!! $key !!}</a></li>

        @endif
    @endforeach
</ol>
@endif

@include('admin._partials.form_header', ['title' => @$form["title"]])


<ul class="nav nav-pills">
    <li class="active"><a href="#main" data-toggle="tab">Informazioni generali</a></li>
    <li><a href="#nodes" data-toggle="tab">Voci</a></li>

</ul>

{!! Notification::showAll() !!}

@if ($errors->any())
        <div class="alert alert-danger">
                {!! implode('<br>', $errors->all()) !!}
        </div>
@endif



<div class="tab-content">
    <div class="tab-pane fade in active " id="main">
        @if (isset($record))
            {!! Form::model($record, array('route' => Array('admin.Menu.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
        @else
            {!! Form::open(array('role' => 'form', 'route' => 'admin.Menu.store', 'method' => 'POST', 'files' => true)) !!}
        @endif
                    @if ($errors->has('login'))
                        <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
                    @endif

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('label', 'Etichetta') !!}
                                {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
                            </div>
                        </div>
                       
                    </div>


                    <div class="form-group">
                    {!! Form::label('name', 'Nome') !!}
                    {!! Language::multilanguageWidget('name',  Language::decodeTranslations(@$record->name), null, false) !!}
                    </div>

                    <div class="form-group">
                    {!! Form::label('description', 'Descrizione') !!}
                    {!! Language::multilanguageWidget('description',  Language::decodeTranslations(@$record->description)) !!}
                    </div>

                   
                    <div class="form-actions">
                        {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
                    </div>

                {!! Form::close() !!}
        





    </div>
    <div class="tab-pane fade in " id="nodes">
        
        @if(!isset($record->id))
            <div class="alert alert-warning">&Egrave; necessario il record prima di poter gestire la struttura del menu</div>
        @else
        
        {{-- INIZIO PARTE DINAMICA PER NESTABLE --}}
        <?php $counter = 0; ?>
         <ul class="nav nav-tabs nav-tabs-translation">
        @foreach($languages as $key => $value)
           
        <li{!! $counter==0?' class="active"':'' !!}><a href="#language_{!! $key !!}" data-toggle="tab">{!! $value !!}</a></li>
    

        <?php $counter++ ?>
        @endforeach
        </ul>
         <?php $counter = 0; ?>
         
        <div class="tab-content">
            @foreach($languages as $key => $value)
            <div role="tabpanel" class="tab-pane tab-pane-translation{!! $counter==0?' active':'' !!}" id="language_{!! $key !!}">
                
                <div class="row">
                    <div class="col-sm-3">
                        <button class="btn btn-info btn-block node_add" data-language-id="{!! $key !!}"><span class="glyphicon glyphicon-plus"></span>&nbsp;Aggiungi Nodo</button>
                        <button class="btn btn-primary btn-block menu_save" data-language-id="{!! $key !!}"><span class="glyphicon glyphicon-ok"></span>&nbsp;Salva la struttura</button>
                        
                        <hr/>
                        <div class="dropdown">
                            <button class="btn btn-warning btn-block dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                              Duplica da
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                              @foreach($languages as $code => $language)
                                @if($code != $key)
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="duplicate-button" data-source-language-id="{!! $code !!}" data-destination-language-id="{!! $key !!}">{!! $language !!}</a></li>
                                @endif
                            @endforeach
                            </ul>
                          </div>
                    </div>
                    <div class="col-sm-9">
                        <div id="menu_nodes_{!! $key !!}" class="dd dd_{!! $key !!}" data-language-id="{!! $key !!}">
                            
                        </div>
                        
                        
                    </div>
                </div>
                
                
                
                
                
                
            </div>
            <?php $counter++ ?>
            @endforeach
        </div>
        
        
        
        
        
<div style="clear: both; margin-bottom: 40px;"></div>




{{-- INIZIO MODAL PER LA MODIFICA DEL NODO --}}
<div class="modal fade" id="node_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Elemento menu</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
        <input type="hidden" name="node_id" id="node_id">
        <input type="hidden" name="node_menu_id" id="node_menu_id">
        <input type="hidden" name="node_parent_id" id="node_parent_id">
        {{-- <input type="hidden" name="node_language_id" id="language_id">--}}
        
        
        
                <div class="form-group">
		    <label for="node_title" class="col-sm-4 control-label">Lingua</label>
		    <div class="col-sm-8">
		      {!! Form::select('node-language_id', $languages, null, Array('id' => 'node_language_id', 'class' => 'form-control', 'readonly' => 'readonly')) !!}
		    </div>
                </div>
                <div class="form-group">
		    <label for="node_title" class="col-sm-4 control-label">Titolo</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="node_title" placeholder="Titolo">
		    </div>
                </div>
                <div class="form-group">
		    <label for="node_url" class="col-sm-4 control-label">Url</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="node_url" placeholder="Indirizzo web">
		    </div>
                </div>
                <div class="form-group">
		    <div class="col-sm-offset-4 col-sm-8">
		      <div class="checkbox">
		        <label>
		          <input type="checkbox" name="node_url_blank_page" id="node_url_blank_page"> Apri in una nuova finestra
		        </label>
		      </div>
		    </div>
                </div>
                <div class="form-group">
		    <label for="node_url" class="col-sm-4 control-label">Attributi extra</label>
		    <div class="col-sm-8">
		      <input type="text" class="form-control" id="node_extra" placeholder="Attributi extra">
		    </div>
                </div>
		</form>
        
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-primary" id="node_save">Salva</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
{{-- FINE MODAL PER LA MODIFICA DEL NODO --}}


<script>

$(document).ready(function(){
    $('.dd').nestable({ /* config options */ });
    
    
    
    // aggiunta di un nodo - form
    $(document).on('click', '.node_add', function(e){
    
    
    	var language_id = $(e.target).attr('data-language-id');
        console.log('language_id: ' + language_id);
        
        
    	var rel = $(e.target).parent().attr('rel');
        
        var div_menu = '.dd_' + language_id;
    	
    	if(rel == undefined)
    		rel = '';
    
        $('#node_id').val('');
        $('#node_parent_id').val(rel);
        $('#node_menu_id').val({!! $record->id !!});
        $('#node_title').val('');
        $('#node_language_id').val(language_id);
    	$('#extra').val('');
    	$('#node_url').val('');
    	$('#node_url_blank_page').removeAttr('checked');
    
       $('#node_modal').modal();
    });
    
    
    // Salvataggio di un nuovo nodo
    // Funzione chiamata dal tasto salva del modal
     $('#node_save').click(function(){
     	
        // codice di base per la creazione inline dell'elemento
        li_html = ' <li class="dd-item dd3-item" data-id="__item_id__"><div class="dd-handle dd3-handle">Drag</div><div class="dd3-content"><span class="dd3-label"><span class="badge">__item_id__</span>__item_title__</span><div class="dd3-actions"><a class="node_edit" rel="__item_id__" href="#"><span class="glyphicon glyphicon-edit"></span></a>                    <a  class="node_add_child node_add" rel="__item_id__"><span class="glyphicon glyphicon-plus-sign"></span></a>                    <a class="node_delete" rel="__item_id__"><span class="glyphicon glyphicon-remove-circle"></span></a></div></div></li>';
        
        
        
        id = $('#node_id').val();
        menu_id = {!! $record->id !!};
        parent_id = $('#node_parent_id').val();
        title = $('#node_title').val();
        url = $('#node_url').val();
        extra = $('#node_extra').val();
        
        url_blank_page = $('#node_url_blank_page').is(':checked')?'1':'0';
        language_id = $('#node_language_id').val();
        
        
        ajax_url = '{!! URL::to('/') !!}/rpc/menu/saveNode';
        
        $.ajax({
          url: ajax_url,
          type: 'POST',
          data: {'title' : title, 'url' : url, 'url_blank_page' : url_blank_page, 'menu_id' : menu_id, 'language_id' : language_id, 'id' : id, 'extra' : extra},
          success: function(result){
          	if(result.status == '1'){
          	
          		li_html = li_html.replace(/__item_id__/g, result.data.id);
          		li_html = li_html.replace(/__item_title__/g, result.data.title);
          		
          		
          		if($('li[data-id='+result.data.id+']').length > 0){
          			$('li[data-id='+result.data.id+'] .dd3-label').html(result.data.title);
          			console.log('Elemento esistente');
          		}
          		else{
          		
          			console.log('Nuovo elemento');
          			if(parent_id == ''){
          				console.log('livello base');
          				
      					$('.dd_' + result.data.language_id + ' > .dd-list').append(li_html);
          			}
          			else{
          				console.log('figlio di '+parent_id);
          				// se non c'è un OL, l'aggiungo
          				if($('li[data-id='+parent_id+'] ol.dd-list').length == 0){
          					html = '<ol class="dd-list"></ol>';	
          					$('li[data-id='+parent_id+']').append(html);
          				}
          				
      					$('li[data-id='+parent_id+'] ol.dd-list').append(li_html);
          			}
          		}
          	}
          	else{
          		toastr.error(result.message);
          	}
          
          },
          dataType: 'json'
        });  
        
    	$('#node_modal').modal('hide');
    });
    
    
    // PUO SERVIRE
	//$(e.target).closest('.companion_item').remove();
	
    // Caricamento di un nodo
	
    $(document).on('click', 'a.node_edit', function(e){
     	
     	e.preventDefault();
     	rel = $(e.target).parent().attr('rel');
     	
     	ajax_url = '{!! URL::to('/') !!}/rpc/menu/loadNode/'+rel;
     
     	
     	$.ajax({
          url: ajax_url,
          type: 'GET',
          success: function(result){
          	if(result.status == '1'){
          	
          		$('#node_id').val(result.data.id);
          		$('#node_menu_id').val(result.data.menu_id);
          		$('#node_language_id').val(result.data.language_id);
          		$('#node_title').val(result.data.title);
	        	$('#node_url').val(result.data.url);
	        	$('#node_extra').val(result.data.extra);
	        	if(result.data.url_blank_page == '1')
		        	$('#node_url_blank_page').attr('checked', 'checked');
		        
		        $('#node_modal').modal();
          	
          	
          	}
          	else{
          		toastr.error(result.message);
          	}
          
          },
          dataType: 'json'
        }); 
     	
     });
	
    
    // Cancellazione di un nodo
     
     $(document).on('click', 'a.node_delete', function(e){
     	
     	e.preventDefault();
     	rel = $(e.target).parent().attr('rel');
     	
     	ajax_url = '{!! URL::to('/') !!}/rpc/menu/deleteNode/'+rel;
     
     	
     	$.ajax({
          url: ajax_url,
          type: 'GET',
          success: function(result){
          	if(result.status == '1'){
          		toastr.success(result.message);
          		$('li[data-id='+rel+']').remove();
          	}
          	else{
          		toastr.error(result.message);
          	}
          
          },
          dataType: 'json'
        }); 
     	
     });
     
     
     
     
     // Salvataggio del menu intero
    
    $('.menu_save').click(function(e){
    
    
        var language_id = $(e.target).attr('data-language-id');
    	var ajax_url = '{!! URL::to('/') !!}/rpc/menu/saveTaxonomy';
        
        $.ajax({
          url: ajax_url,
          type: 'POST',
          data: {'menu_id' : {!! $record->id !!},  'taxonomy' : $('.dd_' + language_id).nestable('serialize')},
          success: function(result){
          	if(result.status == '1'){
          	
          		toastr.success('Menu salvato correttamente');
          	}
          	else{
          		toastr.error(result.message);
          	}
          
          },
          dataType: 'json'
        }); 
    
        
    });
    
});

$('.dd').on('change', function() {
   // potrei fare il salvataggio per ogni modifica (troppe risorse)
});

// CARICAMENTO DEI MENU
$('.dd').each(function(){

    var language_id = $(this).attr('data-language-id');
    var menu_id = {!! $record->id !!};
    var container_selector = '.dd_' + language_id;
    
    loadMenu(menu_id, language_id, container_selector);
   
});


// FUNZIONE PER IL CARICAMENTO DI UN MENU


function loadMenu(menu_id, language_id, container_selector){
    
    var ajax_url = '{!! URL::to('/') !!}/rpc/menu/loadMenu/' + menu_id + '/' + language_id;
    $.ajax({
          url: ajax_url,
          type: 'GET',
          success: function(result){
          	if(result.status == '1'){
                    $(container_selector).html(result.data);
          	}
          	else{
                    toastr.error(result.message);
          	}
          },
          dataType: 'json'
        }); 
    
    
}


// Gestisco la duplicazione

$('.duplicate-button').click(function(){
    var source_language_id = $(this).attr('data-source-language-id');
    var destination_language_id = $(this).attr('data-destination-language-id');
    var menu_id = {!! $record->id !!};
    
    if(confirm('Attenzione! Le voci attualmente presenti nel menu per la lingua corrente verranno eliminate. Procedere?')){
        
        var ajax_url = '{!! URL::to('/') !!}/rpc/menu/duplicateMenu/' + menu_id + '/' + source_language_id + '/' + destination_language_id;
        
        console.log(ajax_url);
        $.ajax({
              url: ajax_url,
              type: 'GET',
              
              success: function(result){
                    if(result.status == '1'){
                        
                        var container_selector = '.dd_' + result.language_id;
                        loadMenu(result.menu_id, result.language_id, container_selector)
                        
                        
                    }
                    else{
                        toastr.error(result.message);
                    }
              },
              dataType: 'json'
            }); 
        
        
    }
    else
       return;
    
    
});


</script>
        
        {{-- FINE PARTE DINAMICA PER NESTABLE --}}
        
        
        @endif



    </div>
</div>
    




@stop







