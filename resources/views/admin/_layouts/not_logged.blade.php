<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{!! $page_title or '' !!} {!! Config::get('bloom.site_name') !!} - Control Panel</title>

        @if(isset($head_assets['css']) && is_array($head_assets['css']) && count($head_assets['css']) > 0)
            @foreach(@$head_assets['css'] as $item)
                <link href="{!! URL::asset($item) !!}" rel="stylesheet">
            @endforeach
        @endif

        @if(isset($head_assets['js']) && is_array($head_assets['js']) && count($head_assets['js']) > 0)
            @foreach(@$head_assets['js'] as $item)
                <script src="{!! URL::asset($item) !!}"></script>
            @endforeach
        @endif


        <script type="text/javascript">
        var BASE_URL =  '{!! URL::to('/'); !!}';
        </script>
    </head>
    <body>
        <nav class="navbar navbar-inverse" role="navigation">

            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <a class="navbar-brand" href="#">
                     {!! Config::get('bloom.site_name') !!}
                </a>
            </div>
        </nav>
                
       <div class="main-container">
            
            <div class="content-wrapper container">
                <div class="row">


                        <!-- content area -->
                        <div class="col-xs-12">
                           @yield('main')


                        </div>
                </div>
                
                
            </div>
            
	    <footer class="footer">
                @include('admin._partials.footer')
            </footer>
            
            
            
        </div>
    </body>
</html>