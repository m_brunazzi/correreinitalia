@extends('admin._layouts.default')
 
@section('head')
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
@stop

@section('main')

            @if(@$form['breadcrumbs'])
            <?php $i = 0 ?>
            <ol class="breadcrumb">

                @foreach($form['breadcrumbs'] as $key => $value)
                    @if(++$i == count($form['breadcrumbs']))
                        <li class="active">{!! $key !!}</li>
                    @else
                        <li ><a href="{!! $value !!}">{!! $key !!}</a></li>

                    @endif
                @endforeach
            </ol>
            @endif

            
            @include('admin._partials.form_header', ['title' => @$form["title"]])
            

            <ul class="nav nav-pills">
                @if(!(@$form['hide_pictures'] && @$form['hide_attachments']))
                <li class="active"><a href="#main" data-toggle="tab">Informazioni generali</a></li>
                @endif
                
                @if(!@$form['hide_pictures'])
                <li><a href="#pictures" data-toggle="tab">Immagini</a></li>
                @endif
                
                @if(!@$form['hide_attachments'])
                <li><a href="#attachments" data-toggle="tab">Allegati</a></li>
                @endif
                
            </ul>
            <hr/>

            {!! Notification::showAll() !!}

            @if ($errors->any())
                    <div class="alert alert-danger">
                            {!! implode('<br>', $errors->all()) !!}
                    </div>
            @endif



            <div class="tab-content">
                <div class="tab-pane fade in active " id="main">
                    
                    
                @yield('form_content')

                

                </div>



                @if(!@$form['hide_pictures'])
                {{-- PICTURES --}}
                <div class="tab-pane fade in panel panel-default" id="pictures">
                    <div class="panel-body">
                        <h2>Immagini</h2>

                        @if(isset($record->id))

                        <div class="row">
                            <div class="col-sm-6">    
                                <div id="picture-uploader" class="dm-uploader text-center">

                                    <strong>Trascinare qui le immagini da caricare</strong>
                                </div>
                            </div>
                            <div class="col-sm-6">    
                                <div class="form-upload-progress-container">
                                    <div id="picture-upload-progress" >
                                        <div class="progress">
                                            <div id="picture-upload-progress-bar" class="progress-bar progress-bar-striped" style="width: 100%">
                                                 In attesa
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div id="picture-upload-log-container" class=" form-upload-log">
                                    <div id="picture-upload-log ">
                                        Per caricare nuove immagini, trascinarle nel box a sinistra

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">

                                <table class="table table-hover picture-table" id="picture-table">
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>





                        @else
                            <div class="alert alert-info">Per caricare le immagini è prima necessario salvare il record</div>
                        @endif
                    </div>
                </div><!-- PICTURES-->
            {{-- END PICTURES --}}  
            @endif

            
            @if(!@$form['hide_attachments'])
            {{-- ATTACHMENTS --}}
                <div class="tab-pane fade in panel panel-default" id="attachments">
                    <div class="panel-body">
                        <h2>Allegati</h2>

                        @if(isset($record->id))

                        <div class="row">
                            <div class="col-sm-6">    
                                <div id="attachment-uploader" class="dm-uploader text-center">

                                    <strong>Trascinare qui gli allegati da caricare</strong>
                                </div>
                            </div>
                            <div class="col-sm-6">    
                                <div class="form-upload-progress-container">
                                    <div id="attachment-upload-progress" >
                                        <div class="progress">
                                            <div id="attachment-upload-progress-bar" class="progress-bar progress-bar-striped" style="width: 100%">
                                                 In attesa
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div id="attachment-upload-log-container" class=" form-upload-log">
                                    <div id="attachment-upload-log">
                                        Per caricare nuovi file, trascinarli nel box a sinistra

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-12">

                                <table class="table table-hover attachment-table" id="attachment-table">
                                    <tbody>

                                    </tbody>
                                </table>

                            </div>
                        </div>





                        @else
                            <div class="alert alert-info">Per caricare le immagini è prima necessario salvare il record</div>
                        @endif
                </div>
            {{-- END ATTACHMENTS --}}
            @endif

            </div>
        </div>





<script>
    $(document).ready(function(){


    @if(isset($record->id))
       // IMMAGINI
        
        BiCi.init({
                baseUrl: '{!! url('/') !!}'
        });
    
        updatePictures();
        updateAttachments();
        
        
        // gestisco la cancellazione delle immagini
        $(document).on('click', '.btn-delete-picture', function(e){
            BiCi.deletePicture({
                pictureId : $(e.target).attr('rel'),
                onSuccess : function(response){
                        $('#picture-upload-progress-bar').removeClass('active');
                        $('#picture-upload-progress-bar').html('Completato');
                        $('#picture-upload-log').prepend('<div class="alert alert-info">Immagine cancellata correttamente</div>');
                        
                        if(response.status == '1'){
                            updatePictures();
                        }
                        else
                           $('#picture-upload-log').prepend('<div class="alert alert-error">Errore nella cancellazione dell\'immagine</div>');
                }
            });
        });  
        
        // gestisco la cancellazione degli allegati
        $(document).on('click', '.btn-delete-attachment', function(e){
            BiCi.deleteAttachment({
                attachmentId : $(e.target).attr('rel'),
                onSuccess : function(response){
                        $('#attachment-upload-progress-bar').removeClass('active');
                        $('#attachment-upload-progress-bar').html('Completato');
                        $('attachment-upload-log').prepend('<div class="alert alert-info">Allegato cancellato correttamente</div>');
                        
                        if(response.status == '1'){
                            updateAttachments();
                        }
                        else
                           $('#attachment-upload-log').prepend('<div class="alert alert-error">Errore nella cancellazione dell\'allegato</div>');
                }
            });
        });   
        
        
        // attivo il widget per l'upload
       $("#picture-uploader").dmUploader({
                                        url : '<?= route('rpc.form.upload.picture') ?>',
                                        allowedTypes : 'image/*',
                                        dataType : 'json',
                                        fileName : 'file',
                                        extraData : {'pictureable_type' : '<?= $form['model_name'] ?>', 'pictureable_id' : '<?= $record->id ?>', 'destination_path' : '<?= Config::get('bloom.public_images_upload_folder') ?>'},
                                        onNewFile: function(){
                                            $('#picture-upload-progress-bar').addClass('active');
                                            $('#picture-upload-progress-bar').html('Sto caricando il file');
                                        },
                                        onComplete: function(){
                                            $('#picture-upload-progress-bar').removeClass('active');
                                            $('#picture-upload-progress-bar').html('Completato');
                                            updatePictures()
                                        },
                                        onUploadSuccess : function(id, data){
                                            $('#picture-upload-log').prepend('<div class="alert alert-success">Caricato <strong>' + data.filename + '</strong></div>');
                                        },
                                        onUploadError : function(id, message){
                                            $('#picture-upload-log').prepend('<div class="alert alert-success">' + message +  '</div>');
                                            $('#picture-upload-progress-bar').removeClass('active');
                                            $('#picture-upload-progress-bar').html('Errore');
                                        },
                                        onFileTypeError: function(file){
                                            $('#picture-upload-log').prepend('<div class="alert alert-danger">Formato non valido</div>');
                                        }
        });
        
        // attivo il widget per l'upload
       $("#attachment-uploader").dmUploader({
                                        url : '<?= route('rpc.form.upload.attachment') ?>',
                                        allowedTypes : '*',
                                        dataType : 'json',
                                        fileName : 'file',
                                        extraData : {'attachmentable_type' : '<?= $form['model_name'] ?>', 'attachmentable_id' : '<?= $record->id ?>', 'destination_path' : '<?= Config::get('bloom.public_files_upload_folder') ?>'},
                                        onNewFile: function(){
                                            $('#attachment-upload-progress-bar').addClass('active');
                                            $('#attachment-upload-progress-bar').html('Sto caricando il file');
                                        },
                                        onComplete: function(){
                                            $('#attachment-upload-progress-bar').removeClass('active');
                                            $('#attachment-upload-progress-bar').html('Completato');
                                            updateAttachments()
                                        },
                                        onUploadSuccess : function(id, data){
                                            $('#attachment-upload-log').prepend('<div class="alert alert-success">Caricato <strong>' + data.filename + '</strong></div>');
                                        },
                                        onUploadError : function(id, message){
                                            $('#attachment-upload-log').prepend('<div class="alert alert-success">' + message +  '</div>');
                                            $('#attachment-upload-progress-bar').removeClass('active');
                                            $('#attachment-upload-progress-bar').html('Errore');
                                        },
                                        onFileTypeError: function(file){
                                            $('#attachment-upload-log').prepend('<div class="alert alert-danger">Formato non valido</div>');
                                        }
        });
        
        
        // FUNZIONI
            
        // aggiornamento delle immagini
        function updatePictures(){
            
            BiCi.loadPictures({
                pictureableId : '<?= $record->id ?>',
                pictureableType : '<?= $form['model_name'] ?>',
                onSuccess : function(response){
                    drawPicturesTable(response.data)
                }
            });
        }
        
        // aggiornamento degli allegati
        function updateAttachments(){
            
            BiCi.loadAttachments({
                attachmentableId : '<?= $record->id ?>',
                attachmentableType : '<?= $form['model_name'] ?>',
                onSuccess : function(response){
                    drawAttachmentsTable(response.data)
                }
            });
        }
        
        
        
        // funzione per disegnare la tabella 
        function drawPicturesTable(modelPictures){
            var tableContent = '';

            for(i = 0; i < modelPictures.length; i++){
                tableContent += '<tr rel="' + modelPictures[i].id + '">';
                tableContent += '<td class="text-center"><span class="glyphicon glyphicon-sort"></span></td>';
                tableContent += '<td><a href="{!! asset('/') !!}' + modelPictures[i].filename + '"><img class="img-thumbnail" src="' + modelPictures[i].thumbnail + '"></a></td>';
                //tableContent += '<td>' + modelPictures[i].order + '</td>';
                tableContent += '<td><span class="badge">' + modelPictures[i].id + '</span></td>';
                tableContent += '<td><strong><a href="#" class="editable" id="label" data-type="text" data-pk="' + modelPictures[i].id + '" data-url="{!! route('rpc.form.rename.picture') !!}" data-title="Inserisci il nome del file">' + modelPictures[i].label + '</a></strong></td>';
                tableContent += '<td><label class="radio"><input type="radio" name="main" value="1" data-picture-id="' + modelPictures[i].id + '" id="cb_picture_' + modelPictures[i].id + '" class="cb_picture_main"'+(modelPictures[i].main == '1'?' checked="checked"':'')+'/>Principale</label></td>';
                tableContent += '<td class="text-right"><button class="btn btn-danger btn-delete-picture" rel="' + modelPictures[i].id + '">Cancella</button></td>';
                tableContent += '</tr>';
            }

            $('#picture-table tbody').html(tableContent);
            
            $.fn.editable.defaults.mode = 'inline';
            $('.editable').editable();
            $('#picture-table tbody').sortable({
                update : function(){ 
                                var ids = [];
                                $('#picture-table tbody tr').each(function(index){
                                    ids.push($(this).attr('rel'));
                                });
                                
                                BiCi.setOrderPicture({
                                record_id : '{{ $record->id}}',
                                ids : ids,
                                onSuccess : function(response){
                                    if(response.status == 1){
                                        toastr.success(response.message);
                                    }
                                    else
                                        toastr.error(response.message);
                                }
                            });
                            }
            });
        }
        
        
        // funzione per disegnare la tabella 
        function drawAttachmentsTable(modelAttachments){
            var tableContent = '';

            for(i = 0; i < modelAttachments.length; i++){
                
                tableContent += '<tr rel="' + modelAttachments[i].id + '">';
                tableContent += '<td class="text-center"><span class="glyphicon glyphicon-sort"></span></td>';
                tableContent += '<td><a target="_blank" class="btn btn-primary" href="{!! asset('/') !!}' + modelAttachments[i].filename + '"><span class="glyphicon glyphicon-download"></span></a></td>';
                tableContent += '<td><span class="badge">' + modelAttachments[i].id + '</span></td>';
                tableContent += '<td><strong><a href="#" class="editable" id="label" data-type="text" data-pk="' + modelAttachments[i].id + '" data-url="{!! route('rpc.form.rename.attachment') !!}" data-title="Inserisci il nome del file">' + modelAttachments[i].label + '</a></strong></td>';
                //tableContent += '<td><strong>' + modelAttachments[i].order + '</strong></td>';
                tableContent += '<td class="text-right"><button class="btn btn-danger btn-delete-attachment" rel="' + modelAttachments[i].id + '">Cancella</button></td>';
                tableContent += '</tr>';
            }

            $('#attachment-table tbody').html(tableContent);
            
            $.fn.editable.defaults.mode = 'inline';
            $('.editable').editable();
            $('#attachment-table tbody').sortable({
                update : function(){ 
                                var ids = [];
                                $('#attachment-table tbody tr').each(function(index){
                                    ids.push($(this).attr('rel'));
                                });
                                
                                BiCi.setOrderAttachment({
                                record_id : '{{ $record->id}}',
                                ids : ids,
                                onSuccess : function(response){
                                    if(response.status == 1){
                                        toastr.success(response.message);
                                    }
                                    else
                                        toastr.error(response.message);
                                }
                            });
                            }
            });

        }

        $(document).on('click', '.cb_picture_main', function(e){
            
            var is_checked = $(e.target).is(':checked');
            
            // chiamo la funzione per aggiornare il record
            BiCi.setMainPicture({
                pictureId : $(e.target).attr('data-picture-id'),
                pictureMain : is_checked,
                onSuccess : function(response){
                    if(response.status == 1){
                        $('cb_picture_' + response.picture_id).attr('checked');
                        toastr.success(response.message);
                    }
                    else
                        toastr.error(response.message);

                    $('.cb_picture_main').each(function(){
                        if( $(this).attr('data-picture-id') == response.picture_id){
                            $(this).attr('checked', 'checked');
                        }
                        else
                            $(this).removeAttr('disabled');
                    });
                }
            });
            
        });

    @endif
});

</script>                       
<!-- END EDIT -->
@overwrite  
        
