@extends('admin._layouts.default')
 
@section('main')
 

<h1>Parametri</h1>
    
    {!! Notification::showAll() !!}

    {!! Form::open(array('role' => 'form', 'route' => 'admin.settings.save', 'method' => 'POST')) !!}
    
    <table class='table table-hover'>
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Gruppo</th>
                <th>Chiave</th>
                <th>Valore</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr class="parameter-new">
                <td class="text-center"><strong>Nuovo</strong></td>
                <td>{!! Form::text('group_new', 'bloom', Array('class'=> 'form-control' ,'placeholder' => 'Gruppo')) !!}</td>
                <td>{!! Form::text('key_new', null, Array('class'=> 'form-control' ,'placeholder' => 'Chiave')) !!}</td>
                <td>{!! Form::text('value_new', null, Array('class'=> 'form-control' ,'placeholder' => 'Valore')) !!}</td>
                <td>&nbsp;</td>
            </tr>
            
            @foreach($rows as $row)
            <tr>
                <td class="text-center"><span class="badge">{!! $row->id!!}</span></td>
                <td>{!! Form::text('group['.$row->id.']', $row->group, Array('class'=> 'form-control', 'readonly' => 'readonly')) !!}</td>
                <td>{!! Form::text('key['.$row->id.']', $row->key, Array('class'=> 'form-control', 'readonly' => 'readonly')) !!}</td>
                <td>{!! Form::text('value['.$row->id.']', $row->value, Array('class'=> 'form-control')) !!}</td>
                <td class="text-right"><a href="{!! route('admin.settings.delete', Array($row->group, $row->key)) !!}" class="btn btn-danger">Cancella</a></td>
            </tr>
            @endforeach
        </tbody>
            
        
    </table>
    
    <div class="form-actions">
        {!! Form::submit('Salva i parametri', array('class' => 'btn btn-block btn-primary')) !!}
    </div>
    
    {!! Form::close() !!}



@stop
