@extends('admin._layouts.default')
 
@section('main')
 
<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    <li><a href="{!! route('admin.Language.index') !!}">Lingue</a></li>
    <li class="active">Modulo</li>
</ol>

@include('admin._partials.form_header', ['title' => $record->language])

{!! Notification::showAll() !!}
 
@if ($errors->any())
        <div class="alert alert-danger">
                {!! implode('<br>', $errors->all()) !!}
        </div>
@endif
        @if (isset($record))
            {!! Form::model($record, array('route' => Array('admin.Language.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
        @else
            {!! Form::open(array('role' => 'form', 'route' => 'admin.Language.store', 'method' => 'POST', 'files' => true)) !!}
        @endif
                    
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        {!! Form::label('language', 'Lingua') !!}
                        {!! Form::text('language', null, Array('class'=> 'form-control' ,'placeholder' => 'language')) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        {!! Form::label('code', 'Codice') !!}
                        {!! Form::text('code', null, Array('class'=> 'form-control' ,'placeholder' => 'code')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <br/>
                        <label for="online">{!! Form::checkbox('online', 1) !!} Online</label>
                    </div>
                </div>
                
            </div>

            @include('admin._partials.picture_form', Array('input_name' => 'icon', 'label' => 'Icona', 'filename' => @$record->icon))

                    

                    <div class="form-actions">
                        {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
                    </div>
               
 
        {!! Form::close() !!}
@stop


