<?php

if(!isset($title) || $title == "")
    $title = 'Immagini';

if(!isset($pictureable_type) || $pictureable_type == "")
    die('Errore');

if(!isset($pictureable_id) || $pictureable_id == "")
    die('Errore');

?>
<fieldset>
<legend>{!! $title !!}</legend>

<div class="well">
    <div class="row">
        <div class="col-sm-12">
            Record trovati: <strong>{!! count($pictures) !!}</strong> - <a href="{!!URL::route('admin.Picture.create', Array('pictureable_type' => $pictureable_type, 'pictureable_id' => $pictureable_id))!!}" class="btn btn-primary btn-sm">Nuovo Record</a>
        </div>
    </div>
</div>
    @if (count($pictures) > 0)
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Immagine</th>
                    <th>ID</th>
                    <th>Etichetta</th>
                    <th>Url</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
        @foreach ($pictures as $row)
            <tr>
                <td class="text-center"><img src="{!! Image::resize($row->img, 100, 100, true) !!}" alt="{!! $record->label !!}" class="img-rounded red_thumb"/></td>
                <td><span class="badge">{!! $row->id !!}</span></td>
                <td><strong>{!! $row->main == true?'<span style="color: red;">'.$row->label.'</span>':$row->label !!}</strong></td>
                <td><em><a target="_blank" href="{!! url($row->img) !!}">{!! url($row->img) !!}</a></em></td>
                <td class="text-center"><a href="{!!URL::route('admin.Picture.edit', $row->id)!!}" class="btn btn-primary">Modifica</a></td>
                <td class="text-center" class="text-center">
                    {!! Form::open(array('route' => array('admin.Picture.destroy', $row->id), 'method' => 'delete')) !!}
                            <button type="submit" href="{!! URL::route('admin.Picture.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    @else
        <div class="alert alert-info">Nessun record trovato</div>
    @endif
</fieldset>