<?php
if(!isset($input_name) || $input_name == "")
    $input_name = 'file';
if(!isset($title) || $title == "")
    $title = 'File Upload';
?>
<fieldset>
<legend>{!! $title !!}</legend>
<div class="row">

    <div class="col-sm-2 text-center ">
        
        @if(isset($file_name) && $file_name !='')
        <div class="well">
            <a target="_blank" href="{!! URL::asset($file_name) !!}"><span class="glyphicon glyphicon-file"></span></a> 
            <p><a target="_blank" href="{!! URL::asset($file_name) !!}"><small style="word-wrap: break-word;">{!! $file_name !!}</small></a></p>
        </div>
        @else
            <span class="badge">Nessun file allegato</span>
        @endif
        
    </div>
    <div class="col-sm-4">
        <div class="radio">
        <label>{!! Form::radio('action_file', '', true) !!}Lascia Invariato</label>
        </div>
         <div class="radio">
        <label>{!! Form::radio('action_file', 'add') !!}Aggiungi/sostituisci file</label>
        </div>
         <div class="radio">
        <label>{!! Form::radio('action_file', 'delete') !!}Rimuovi file</label>
        </div>

        <div id="container_file" style="display: none;">
            {!! Form::label('Seleziona il file da caricare') !!}
            <input name="{!! $input_name !!}" id="{!! $input_name !!}" type="file" style="display:none">
            <div class="input-group">
                <input id="{!! $input_name !!}_wrapper" class="form-control" type="text">
                <span class="input-group-btn">
                    <a class="btn btn-primary" onclick="$('input[id={!! $input_name !!}]').click();">Seleziona</a>
                </span>
            </div>

            <script type="text/javascript">
                
             $(document).ready(function() {
                            $(".fancybox").fancybox();
                            
                             $('input[id={!! $input_name !!}]').change(function() {
                                $('#{!! $input_name !!}_wrapper').val($(this).val());
                            });

                            $('input[name="action_file"]').click(function(){
                                        if($(this).val() == 'add')
                                            $('#container_file').show(); 
                                        else 
                                            $('#container_file').hide();
                                });
                    });
            
           
            </script>
        </div>
    </div>
</div>
</fieldset>
<hr/>