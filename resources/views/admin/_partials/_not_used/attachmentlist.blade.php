<?php

if(!isset($title) || $title == "")
    $title = 'Immagini';

if(!isset($attachmentable_type) || $attachmentable_type == "")
    die('Errore');

if(!isset($attachmentable_id) || $attachmentable_id == "")
    die('Errore');


?>
<fieldset>
<legend>{!! $title !!}</legend>

<div class="well">
    <div class="row">
        <div class="col-sm-12">
            Record trovati: <strong>{!! count($attachment) !!}</strong> - <a href="{!!URL::route('admin.Attachment.create', Array('attachmentable_type' => $attachmentable_type, 'attachmentable_id' => $attachmentable_id))!!}" class="btn btn-primary btn-sm">Nuovo Record</a>
        </div>
    </div>
</div>
    @if (count($attachments) > 0)
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    
                    <th>ID</th>
                    <th>Etichetta</th>
                    <th>File</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
        @foreach ($attachments as $row)
            <tr>
                
                <td><span class="badge">{!! $row->id !!}</span></td>
                <td><strong>{!! $row->label !!}</strong></td>
                <td class="text-center"><a class="btn btn-info" href=""><span class="glyphicon glyphicon-file"></span></a></td>
                <td class="text-center"><a href="{!!URL::route('admin.Attachment.edit', $row->id)!!}" class="btn btn-primary">Modifica</a></td>
                <td class="text-center" class="text-center">
                    {!! Form::open(array('route' => array('admin.Attachment.destroy', $row->id), 'method' => 'delete')) !!}
                            <button type="submit" href="{!! URL::route('admin.Attachment.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </table>
    @else
        <div class="alert alert-info">Nessun record trovato</div>
    @endif
</fieldset>