<?php
if(!isset($input_name) || $input_name == "")
    $input_name = 'file';
if(!isset($show_image) || $show_image == "")
    $show_image = false;
if(!isset($label) || $label == "")
    $label = 'Image Upload';
?>
<fieldset>
<legend>{!! $label !!}</legend>
<div class="row">

    <div class="col-sm-4 text-center ">
        
        @if(isset($filename) && $filename !='')
        <div class="well">
            <a href="{!! URL::asset($filename) !!}"><img src="{!! Image::resize($filename, 100, 100, true) !!}"  class="img-rounded red_thumb"/></a>
            <p><a target="_blank" href="{!! URL::asset($filename) !!}"><small style="word-wrap: break-word;">{!! $filename !!}</small></a></p>
        </div>
        @else
            <span class="badge">Nessun file allegato</span>
        @endif
        
    </div>
    <div class="col-sm-4">
        
        
        {!! Form::select($input_name.'_action', Array('' => 'Lascia invariato', 'add' => 'Aggiungi/sostituisci file', 'remove' => 'Rimuovi'), null, Array('rel' => $input_name, 'class' => 'form-control image-action'))!!}
        
        
        

        <div id="container_file" style="display: none;">
            <input type="file" name="{!! $input_name !!}" class="image-file" id="input_file_{!! $input_name!!}"/>
            
        </div>
        
       
    </div>
    <div class="col-sm-4">
        {!! Form::text($input_name.'_fake', null, Array('id' => $input_name.'_fake' , 'disabled' => 'disabled', 'class' => 'form-control hidden' )) !!}
        
    </div>
</div>
</fieldset>

<script>
    
$(document).ready(function(){
    if(pictureFormInitialized === undefined){
        handlePictureForm();
        var pictureFormInitialized = true;
    }
});

function handlePictureForm(){
    
    console.log('Picture form initialized');
    
    // action change 
    $('.image-action').each(function(e){
        $(this).val('');
    });
       
    // action change 
    $('.image-action').on('change', function(e){
        input_name = $(this).attr('rel');
        input_file_id = '#input_file_' + input_name;
        if($(this).val() == 'add'){
            $(input_file_id).trigger('click');
            $('#' + input_name + '_fake').removeClass('hidden');        
        }
        else{
            $('#' + input_name + '_fake').addClass('hidden');
        }
    });
    
    // filename change
    $('.image-file').on('change', function(e){
        input_name = $(this).attr('name');
        input_file_id = '#input_file_' + input_name;
        $('#' + input_name + '_fake').val( $(input_file_id).val());
    });
}    
    
  
</script>
<hr/>