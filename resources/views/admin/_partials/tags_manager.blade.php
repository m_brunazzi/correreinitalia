{{-- TAGS --}}
        <div class="panel panel-default  panel-form">
            <div class="panel-body">
                 <div class="form-group">
                     <a name="tags-container" id="tags-container"></a>
                {!! Form::label('Tags') !!}<br/>
                <input name="tags_input" id="tags_input" value="" type="text" class="input-sm tm-input tm-input-inverse form-control" style="width:150px; display: inline;" placeholder="Nuovo tag">  


                @if(isset($tags_favourites) && count($tags_favourites) > 0)
                    <p><small>

                    <strong>Preferiti</strong>&nbsp;
                     @foreach($tags_favourites as $item)
                     <a href="#tags-container" class="tags-favourite">{!! $item->name !!}</a>

                     @endforeach
                     </small></p>
                @endif
                 </div>
            </div>
        </div>
     <script>
        $(document).ready(function(){
            // TAGS
            <?php
                // Preparo la lista dei tag

                $tags_array = Array();
                if(isset($record->id)){
                    foreach($record->tag as $item){
                        $tags_array[] = $item->name;
                    }
                }
                $tags = implode(',', $tags_array);
            ?>

            $('#tags_input').tagsManager({'hiddenTagListName' : 'tags', 'prefilled' : '{!! $tags !!}'});
            $('.tags-favourite').click(function(e){
                e.preventDefault();
                $('#tags_input').tagsManager('pushTag',$(e.target).text());
            });
        });
     </script>
{{-- END TAGS --}}