<div class="row form-header">
    <div class="col-sm-9">
        <h1>
            @if(@isset($title))
                {!! $title !!}
            @else
                Nuovo Record
            @endif
        </h1>

    </div> 
    <div class="col-sm-3 text-right">
        @if(isset($record->id))
        <div class="record-info">
            Creazione: <strong>{!! $record->created_at !!}</strong><br/>
            Ultima modifica: <strong>{!! $record->updated_at !!}</strong>
        </div>
        @endif
    </div>    
</div>