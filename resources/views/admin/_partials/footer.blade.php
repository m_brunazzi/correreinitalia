<div class="container text-center">
    
    @if($current_user)
        {{ $current_user->name }} -  
    @endif
    &copy; {!! date('Y') !!} Bloom Design
    
</div>