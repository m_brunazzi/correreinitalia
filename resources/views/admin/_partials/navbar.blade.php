<nav class="navbar  navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bloom-navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">{!! Config::get('bloom.site_name') !!}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bloom-navbar">
      <ul class="nav navbar-nav">
        
          
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Contenuti <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! URL::route('admin.Article.index') !!}">Articoli</a></li>
            <li><a href="{!! URL::route('admin.ArticleCategory.index') !!}">Categorie di articoli</a></li>
            <li><a href="{!! URL::route('admin.Carousel.index') !!}">Carousel</a></li>
            <li class="divider"></li>
            
            <li><a href="{!! URL::route('admin.Menu.index') !!}">Menu</a></li>
          </ul>
        </li>
          
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Autenticazione <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! URL::route('admin.User.index') !!}">Utenti</a></li>
            <li><a href="{!! URL::route('admin.Group.index') !!}">Gruppi</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configurazione <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! URL::route('admin.Language.index') !!}">Lingue</a></li>
            <li><a href="{!! URL::route('admin.settings') !!}">Parametri</a></li>
            
          </ul>
        </li>
        
        
        <li><a href="{!! route('rpc.logout') !!}">Disconnessione</a></li>
      </ul>
     
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>