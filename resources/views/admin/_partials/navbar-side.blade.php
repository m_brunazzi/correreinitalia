
      <ul class="nav nav-sidebar">
        
          
        <li class="dropdown">
          <a href="#"  role="button" aria-expanded="false">Contenuti</span></a>
          <ul role="menu">
            <li><a href="{!! URL::route('admin.Article.index') !!}">Articoli</a></li>
            <li><a href="{!! URL::route('admin.ArticleCategory.index') !!}">Categorie di articoli</a></li>
            <li><a href="{!! URL::route('admin.Carousel.index') !!}">Carousel</a></li>
            <li class="divider"></li>
            
            <li><a href="{!! URL::route('admin.Menu.index') !!}">Menu</a></li>
          </ul>
        </li>
          
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Autenticazione <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! URL::route('admin.User.index') !!}">Utenti</a></li>
            <li><a href="{!! URL::route('admin.Group.index') !!}">Gruppi</a></li>
          </ul>
        </li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configurazione <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{!! URL::route('admin.Language.index') !!}">Lingue</a></li>
            <li><a href="{!! URL::route('admin.settings') !!}">Parametri</a></li>
            
          </ul>
        </li>
        
        
        <li><a href="{!! route('rpc.logout') !!}">Disconnessione</a></li>
      </ul>
     