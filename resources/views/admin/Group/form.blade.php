@extends('admin._layouts.default')
 
@section('main')
 
<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    <li><a href="{!! route('admin.Group.index') !!}">Gruppi</a></li>
    <li class="active">Modulo</li>
</ol>



@include('admin._partials.form_header', ['title' => $record->name])



{!! Notification::showAll() !!}
 
@if ($errors->any())
        <div class="alert alert-danger">
                {!! implode('<br>', $errors->all()) !!}
        </div>
@endif



{{-- INIZIO SEZIONE MAIN --}}
@if (isset($record->id))
    {!! Form::model($record, array('route' => Array('admin.Group.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Group.store', 'method' => 'POST')) !!}
@endif
            @if ($errors->has('login'))
                <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
            @endif
 
            <div class="form-group">
                {!! Form::label('name', 'Nome') !!}
                {!! Form::text('name', null, Array('class'=> 'form-control' ,'placeholder' => 'name')) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('description', 'Descrizione') !!}
                {!! Form::textarea('description', null, Array('class'=> 'form-control' ,'placeholder' => 'name')) !!}
            </div>
             
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
{{-- FINE SEZIONE GENERALE --}}


@stop


