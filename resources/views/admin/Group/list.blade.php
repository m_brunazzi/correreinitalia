@extends('admin._layouts.default')
 
@section('main')
 
<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    <li class="active">Gruppi</li>
</ol>
 

<h1>Gruppi di Account - Elenco</h1>
<p><strong>Attenzione</strong>: Il gruppo degli amministratori non pu&ograve; essere modificato o cancellato.</p>

<div class="well well-sm">
    <div class="row">
        <div class="col-sm-12">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.Group.create')!!}" class="btn btn-primary btn-sm">Nuovo Record</a>
        </div>
        
    </div>
</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table  table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Account</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            
            <td><strong>{!! $row->name !!}</strong></td>
            <td><span class="badge">{!! count($row->user) !!}</span></td>
            
            
            <td class="text-right">
                @if($row->id != $admin_group_id)
                <a href="{!!URL::route('admin.Group.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.Group.destroy', $row->id), 'method' => 'delete', 'class' => "form-delete")) !!}
                        <button type="submit" href="{!! URL::route('admin.Group.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
                @else
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Modifica">
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Cancella">
                @endif
            </td>
                
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-default">Nessun record trovato</div>
@endif


@stop