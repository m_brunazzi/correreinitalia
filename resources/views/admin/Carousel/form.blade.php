@extends('admin._layouts.content_form')
 
@section('form_content')
 



@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.Carousel.update', $record->id), 'role' => 'form', 'method' => 'PUT', 'files' => true)) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.Carousel.store', 'method' => 'POST', 'files' => true)) !!}
@endif
            @if ($errors->has('login'))
                <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
            @endif
            
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        {!! Form::label('label', 'Etichetta') !!}
                        {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                     <div class="checkbox">
                         <br/>
                        <label for="online">{!! Form::checkbox('online', 1) !!} Online</label>
                    </div>
                </div>
            </div>
            
            
            <div class="form-group">
            {!! Form::label('name', 'Nome') !!}
            {!! Language::multilanguageWidget('name',  Language::decodeTranslations(@$record->name), null, false) !!}
            </div>
            
            <div class="form-group">
            {!! Form::label('summary', 'Sommario') !!}
            {!! Language::multilanguageWidget('summary',  Language::decodeTranslations(@$record->summary), null, false) !!}
            </div>
            
            @include('admin._partials.picture_form', Array('input_name' => 'image', 'label' => 'Immagine', 'filename' => @$record->image))
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop
