@extends('admin._layouts.content_form')
 
@section('form_content')
 



@if (isset($record))
    {!! Form::model($record, array('route' => Array('admin.ArticleCategory.update', $record->id), 'role' => 'form', 'method' => 'PUT')) !!}
@else
    {!! Form::open(array('role' => 'form', 'route' => 'admin.ArticleCategory.store', 'method' => 'POST')) !!}
@endif
            @if ($errors->has('login'))
                <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
            @endif
 
            <div class="form-group">
                {!! Form::label('label', 'Etichetta') !!}
                {!! Form::text('label', null, Array('class'=> 'form-control' ,'placeholder' => 'label')) !!}
            </div>
            
            <div class="form-group">
            {!! Form::label('name', 'Nome') !!}
            {!! Language::multilanguageWidget('name',  Language::decodeTranslations(@$record->name), null, false) !!}
            </div>
            
            <div class="form-group">
            {!! Form::label('name', 'Descrizione') !!}
            {!! Language::multilanguageWidget('description',  Language::decodeTranslations(@$record->description)) !!}
            </div>
            
            
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 
        {!! Form::close() !!}
@stop
