@extends('admin._layouts.default')
 
@section('main')

<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    
    <li class="active">Categorie di Articoli</li>
</ol>


 
<h1>Categorie di articoli - Elenco</h1>

<div class="well">
    
    <div class="row">
        <div class="col-sm-9">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.ArticleCategory.create')!!}" class="btn btn-primary btn-sm">Nuovo Record</a>
        </div>
        <div class="col-sm-3">
            <div class="pull-right">
                {!! Form::open(array('role' => 'form', 'route' => 'admin.ArticleCategory.index', 'method' => 'GET','class' => 'form-inline')) !!}
                <div class="form-group">
                    <div class="input-group">
                    <input type="text" name="search" class="form-control input-sm" value="{!! $search_string !!}">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="submit">Cerca</button>
                        </span>
                    </div>
                </div>
                {!! Form::close() !!}
           </div>
        </div>
    </div>

</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table table-hover ">
        <thead>
            <tr>
                <th>ID</th>
                <th>Etichetta</th>
                <th>Articoli</th>
                
                <th>&nbsp;</th>
            </tr>
        </thead>
        
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            <td><strong>{!! $row->label !!}</strong></td>
            <td><a href="{!! route('admin.Article.index') !!}?article_category_id={!! $row->id !!}"><span class="badge">{!! count($row->article) !!}</span></a></td>
            <td class="text-right">
                <a href="{!!URL::route('admin.ArticleCategory.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.ArticleCategory.destroy', $row->id), 'method' => 'delete', 'class' => 'form-delete')) !!}
                        <button type="submit" href="{!! URL::route('admin.ArticleCategory.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-default">Nessun record trovato</div>
@endif


@stop