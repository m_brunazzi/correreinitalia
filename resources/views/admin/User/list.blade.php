@extends('admin._layouts.default')
 
@section('main')

<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    <li class="active">Utenti</li>
</ol>
 


<h1>Utenti - Elenco</h1>
<p><strong>Attenzione</strong>: L'account <strong>{!! Config::get('bloom.admin_email');!!}</strong> non pu&ograve; essere modificato o cancellato.</p>

<div class="well well-sm">
    <div class="row">
        <div class="col-sm-12">
            Record trovati: <strong>{!! $rows->total() !!}</strong> - <a href="{!!URL::route('admin.User.create')!!}" class="btn btn-primary btn-sm">Nuovo Record</a>
        </div>
        
    </div>
</div>

{!! Notification::showAll() !!}

@if (count($rows) > 0)
    <table class="table  table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Nome</th>
                <th>Attivo</th>
                <th>Amministratore</th>
                
                <th>&nbsp;</th>
            </tr>
        </thead>
    @foreach ($rows as $row)
        <tr>
            <td><span class="badge">{!! $row->id !!}</span></td>
            
            <td><strong>{!! $row->email !!}</strong></td>
            <td>{!! $row->name !!}</td>
            <td class="text-center">
            @if($row->active)
            <span class="glyphicon glyphicon-ok-circle"></span>
            @endif  
            </td>
            <td class="text-center">
            @if($row->admin)
            <span class="glyphicon glyphicon-ok-circle"></span>
            @endif  
            </td>
          
          
          
            
            
            <td class="text-right">
                
                
                @if($row->id != $admin_user_id)
                <a href="{!!URL::route('admin.User.edit', $row->id)!!}" class="btn btn-primary">Modifica</a>
                {!! Form::open(array('route' => array('admin.User.destroy', $row->id), 'method' => 'delete', 'class' => "form-delete")) !!}
                        <button type="submit" href="{!! URL::route('admin.User.destroy', $row->id) !!}" class="btn btn-danger btn-mini">Cancella</button>
                {!! Form::close() !!}
                @else
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Modifica">
                    <input type="button" disabled class="btn btn-danger btn-mini" value="Cancella">
                @endif
            </td>
                
        </tr>
    @endforeach
    </table>
    {!! $rows->appends($query_string)->render() !!}
@else
    <div class="alert alert-default">Nessun record trovato</div>
@endif


@stop