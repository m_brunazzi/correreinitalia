@extends('admin._layouts.default')
 
@section('main')
 
<ol class="breadcrumb">
    <li><a href="{!! route('admin.dashboard') !!}">Dashboard</a></li>
    <li><a href="{!! route('admin.User.index') !!}">Utenti</a></li>
    <li class="active">Modulo</li>
</ol>

@include('admin._partials.form_header', ['title' => $record->email])


<ul class="nav nav-pills">
    <li class="active"><a href="#main" data-toggle="tab">Informazioni generali</a></li>
    <li><a href="#groups" data-toggle="tab">Gruppi</a></li>
</ul>

<div class="alert alert-warning alert-dismissable hidden" id="tabs_notification">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <strong>Attenzione!</strong> Assicurarsi di aver salvato le modifiche prima di passare ad un'altra sezione.
</div>

{!! Notification::showAll() !!}
 
@if ($errors->any())
        <div class="alert alert-danger">
                {!! implode('<br>', $errors->all()) !!}
        </div>
@endif


<div class="tab-content">
    <div class="tab-pane fade in active" id="main">
    {{-- INIZIO SEZIONE MAIN --}}
    
  
    @if (isset($record->id))
        {!! Form::model($record, array('role' => 'form', 'route' => Array('admin.User.update', $record->id), 'method' => 'PUT')) !!}
    @else
        {!! Form::open(array('role' => 'form', 'route' => 'admin.User.store', 'method' => 'POST')) !!}
    @endif
    
    
    
            @if ($errors->has('login'))
                <div class="alert alert-error">{!! $errors->first('login', ':message') !!}</div>
            @endif
            
          
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email', null, Array('class'=> 'form-control' ,'placeholder' => 'Email')) !!}
                    </div>
                </div>
                <div class="col-sm-4">    
                    <div class="form-group">
                        {!! Form::label('password', 'Password') !!}
                        {!! Form::password('password',  Array('class'=> 'form-control' ,'placeholder' => 'Password')) !!}
                    </div>
                </div>
                <div class="col-sm-4">    
                    <div class="form-group">
                        {!! Form::label('password_confirmation', 'Conferma password') !!}
                        {!! Form::password('password_confirmation',  Array('class'=> 'form-control' ,'placeholder' => 'Conferma password')) !!}
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Nome completo') !!}
                        {!! Form::text('name', null, Array('class'=> 'form-control' ,'placeholder' => 'Nome completo')) !!}
                    </div>
                </div>
                
                <div class="col-sm-3">    
                    <div class="form-group">
                        <br/>
                        <label for="active">{!! Form::checkbox('active', 1) !!} Attivo</label>
                    </div>
                </div>
                <div class="col-sm-3">    
                    <div class="form-group">
                        <br/>
                        <label for="admin">{!! Form::checkbox('admin', 1) !!} Amministratore</label>
                    </div>
                </div>
            </div>
                
            
         
            
    </div>
    <div class="tab-pane fade in" id="groups">
       
        @if(isset($groups) && count($groups) > 0)
            @foreach($groups as $item)
                <div class="col-sm-4">
                    <div class="checkbox">
                        <label>{!! Form::checkbox('group['.$item->id.']', $item->id, in_array($item->id, $groups_ids)) !!}{{ $item->name }}</label>
                    </div>
                </div>
            @endforeach
            
        @else
        <div class="alert alert-info">Nessun gruppo disponibile.</div>
            
        @endif
        
    </div>
    
</div>
            
            <div class="form-actions">
                {!! Form::submit('Salva', array('class' => 'btn btn-block btn-primary')) !!}
            </div>
 <script type="text/javascript">
    $(document).ready(function(){
        
        $('.nav-pills li a').click(function(){
                    $('#tabs_notification').removeClass('hidden');
                });

    });

</script>
        {!! Form::close() !!}



@stop


