<?php namespace App\Models;


use Illuminate\Database\Eloquent\Model;

// Classe astratta da estendere per contenuti generici.
// Gestisce i seguenti aspetti:
// - Immagini associate al contenuto (Picture)
// - Allegati associati al contenuto (Attachment)
// - Tag associati al contenuto (Tag)



use Config;
use Input;

 
abstract class Content extends Model {
    
   protected $searchable_fields = Array(); // Contiene i nomi dei campi in cui effettuare la ricerca semplice
    
    
    protected $validation_rules;
    
    public static $errors; 
    
   
    
    // RICERCA GENERICA
    
    public function filter($search_string = null, $params = Array()){
        
        $searchable_fields = $this->searchable_fields;
        
        $instance = $this;
        
        if(isset($search_string) && $search_string != ''){
            
            if(is_array($searchable_fields) && count($searchable_fields) > 0){
                            
                $instance = $instance->where(function($query) use($instance, $searchable_fields, $search_string){
                    $counter = 0;
                    
                    foreach($searchable_fields as $item){
                        
                        if($counter == 0){
                            $query->where($item, 'LIKE', '%'.$search_string.'%');
                            
                        }
                        else{
                            $query->orWhere($item, 'LIKE', '%'.$search_string.'%');
                            
                        }
                        $counter++;
                    }   
                    
                });
            
            }
        }

        if(is_array($params) && count($params) > 0){
            foreach($params as $key => $value){
                $instance = $instance->where($key, '=', $value);
            }
        }
        
        return $instance;
        
    }
    
    
    
    
    
    // RELAZIONI
    
    public function picture()
    {
        return $this->morphMany('Picture', 'pictureable');
    }
    
    public function attachment()
    {
        return $this->morphMany('Attachment', 'attachmentable');
    }
    
    public function tag()
    {
        return $this->morphToMany('Tag', 'taggable', 'blm_taggable', 'taggable_id', 'tag_id');
    }
    
    
    
    // VALIDAZIONE
    
    public static function validate($data = null)
    {
         $instance = new static;
        
        $data = $data ?: \Input::all();
   
        $validation = \Validator::make($data, $instance->validation_rules);
 
        if ($validation->passes()) 
            return true;
        else{
            self::$errors['validation'] = $validation->messages();
            return false;   
        }
    }
    
    
    public function handleUpload($input_name, $destination_folder){
        
        $action = Input::get($input_name.'_action');

        // cancello il file esistente nel caso di richiesta di rimozione o di un nuovo file
        if($action == 'add' || $action == 'remove'){
            if($this->$input_name && file_exists($destination_folder.$this->$input_name))
                unlink($destination_folder.$this->$input_name);
        }

        if($action == 'remove')
            return '';

        if($action == 'add'){
            $file = Input::file($input_name);

            if(!$file->isValid())
                return false;

            $extension = $file->getClientOriginalExtension(); //if you need extension of the file

            $filename = uniqid().'.'.$extension;
            $uploadSuccess = $file->move($destination_folder, $filename);

            return $destination_folder.$filename;
        }
        
        return $this->$input_name;
    }

    
    // VARIE
    
    public  function getOptions($value_field, $id_field = 'id', $empty = true){
        $data = self::get();
        //$data = new static;
        $data->get();
        $result = Array();
        if($empty)
            $result[] = '---';
        foreach($data as $item){
            $result[$item->$id_field] = $item->$value_field;
        }
        return $result;
        
    }
    
}

?>