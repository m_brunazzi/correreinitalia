<?php namespace App\Models;

// Classe astratta da estendere per contenuti generici.
// Gestisce i seguenti aspetti:
// - Immagini associate al contenuto (Picture)
// - Allegati associati al contenuto (Attachment)
// - Tag associati al contenuto (Tag)
// - Contenuti multilingua da specificare in una variabile TBD


use Config;
use Input;

 
abstract class Translatable extends Content {
    
    protected $translatable_fields = Array(); // Contiene i nomi dei campi che possono essere tradotti
    
    
    
    
    protected $validation_rules;
    
    public static $errors; 
    
    public static $auto_translate = false; // definisce se i testi traducibili vengono o no tradotti in automatico
    
// TRADUZIONI
    
    
    public static function enableTranslation($status = true){
        
        $instance = new static;
        $instance::$auto_translate = $status;
        
        return $instance;
    }
    
    // Accessor custom per i campi traducibili. Ovverride della funzione originale, che invece di ritornare il valore grezzo, effettua prima il decode json
       
    public function getAttribute($key) {
        
        if(in_array($key, $this->translatable_fields ) && self::$auto_translate){
            
        
            $inAttributes = array_key_exists($key, $this->attributes);
            
            

            // If the key references an attribute, we can just go ahead and return the
            // plain attribute value from the model. This allows every attribute to
            // be dynamically accessed through the _get method without accessors.
            if ($inAttributes || $this->hasGetMutator($key))
            {
                    $value = $this->getAttributeValue($key);
            }

            // If the key already exists in the relationships array, it just means the
            // relationship has already been loaded, so we'll just return it out of
            // here because there is no need to query within the relations twice.
            if (array_key_exists($key, $this->relations))
            {
                    $value = $this->relations[$key];
            }

            // If the "attribute" exists as a method on the model, we will just assume
            // it is a relationship and will load and return results from the query
            // and hydrate the relationship's value on the "relationships" array.
            $camelKey = camel_case($key);

            if (method_exists($this, $camelKey))
            {
                    $value =  $this->getRelationshipFromMethod($key, $camelKey);
            }
            
            return Language::translate($value);
        }
        else
            return parent::getAttribute($key);
    }
   

    
    private function _decodeJson($field){
        $json = $field;
        $translated = Language::translate($json);
        return $translated;
    }
    
    
    
    
    
    
    
    
    
}

?>