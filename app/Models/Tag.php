<?php namespace App\Models;

use Config, DB;

 
class Tag extends \Eloquent {
 
    protected $table = 'blm_tag';
    
    
    public function taggable()
    {
        return $this->morphTo();
    }

    
    
    public static function find_or_create(Array $tags){
        $result = Array();
        
        
        for($i = 0; $i < count($tags); $i++){
            $tags[$i] = strtolower($tags[$i]);
        }
        
        $actual = Tag::whereIn('name', $tags)->get();
        
        
        foreach($actual as $item){
            $result[] = $item->id;
            $tags = array_diff($tags, Array($item->name));
        }
        foreach($tags as $item){
            if(trim($item) != ''){
                $tag = new Tag;
                $tag->name = $item;
                $tag->save();
                $result[] = $tag->id;
            }
        }
        
        return $result;
    }
    
    
    
    public static function favourites($count){
        $query = 'SELECT COUNT(blm_taggable.tag_id) AS cnt, blm_tag.name FROM blm_tag INNER JOIN blm_taggable ON  blm_taggable.tag_id = blm_tag.id GROUP BY blm_taggable.tag_id ORDER BY cnt DESC LIMIT 0,'.$count;
        
        $result = DB::select($query);
        
        return $result;
        
    }
    
    
}

?>