<?php namespace App\Models;

use Config, DB;
 
class Picture extends \Eloquent {
 
    protected $table = 'blm_picture';
    
    
     public function pictureable()
    {
        return $this->morphTo();
    }
    
   
    
    public function setMain(){
        
        if(isset($this->id)){
            
            DB::update('update blm_picture set main = 0 where pictureable_id = ? AND pictureable_type = ?', array($this->picturable_id, $this->picturable_type));
            
            $this->main = true;
            $this->save();
                       
            
        }
        return $this;
        
    }
    
    public function save(array $options = array()){
        
        if($this->main == true){
            DB::update('update blm_picture set main = 0 where id <> ? AND pictureable_id = ? AND pictureable_type = ?', array($this->id, $this->pictureable_id, $this->pictureable_type));
            
        }
        return parent::save();
    }
    
     public function delete(){
        
         if(isset(self::$filename) && file_exists(self::$filename))
             unlink(self::$filename);
        
        return parent::delete();
    }
    
}

?>