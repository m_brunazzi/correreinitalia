<?php namespace App\Models;

use Config;
 
class Article extends Translatable {
        
    protected $table = 'blm_article';
    protected $translatable_fields = Array('name', 'summary','description');
    protected $searchable_fields = Array('name', 'summary', 'description');
    
    protected $validation_rules = Array(
                                        'label' => 'required'
                                        );
   
    
    
    protected $dates = array('date');
    private static $current_language;
       
    
    // ACCESSORS & MUTATORS
 
    public function getDateAttribute($value){
        $date_formatted = implode('/',array_reverse(explode('-',$value)));
        if($date_formatted == '00/00/0000')
            $date_formatted = '';
        return $date_formatted;
    }
    
    public function setDateAttribute($value){
        $date_formatted = implode('-',array_reverse(explode('/',$value)));
         $this->attributes['date'] = $date_formatted;
    }
    
    // RELATIONS

    public function category()
    {
        return $this->belongsToMany('App\Models\ArticleCategory', 'blm_article_article_category', 'article_id', 'article_category_id');
    }
    
}

?>