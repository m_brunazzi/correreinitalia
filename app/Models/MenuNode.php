<?php namespace App\Models;

use Config;
use URL;
 
class MenuNode extends Translatable {
 
    protected $table = 'blm_menu_node';
    
    protected $searchable_fields = Array('title');
    
    protected $validation_rules = Array(
                                     'menu_id' => 'required',
                                     'language_id' => 'required',
                                     'title' => 'required'
                                     );
    
    
    public static $nested_html = '';
    public static $nested_nodes = '';
    public static $structured_nodes = '';
    
    public function children(){
        return $this->hasMany('MenuNode', 'parent_id');
        
    }
    
    public static function deleteRecursive($node_id){
        $node = MenuNode::with('children')->find($node_id);
        if(count($node->children) > 0){
            foreach($node->children as $item){
                MenuItem::deleteRecursive($item->id);
            }
        }
        $node->delete();
        return true;
    }
    
    public static function saveTaxonomyNode($menu_node, $order, $parent_id = null){
        if(!isset($menu_node['id']))
            return false;
            
        $node = MenuNode::find($menu_node['id']);
        if(!$node)
            return false;
            
        $node->order = $order;
        $node->parent_id = $parent_id;
        $node->save();
        
       
            
        if(isset($menu_node['children']) && count($menu_node['children']) > 0){
            $order_child = 1;
        
            foreach($menu_node['children'] as $item){
                MenuNode::saveTaxonomyNode($item, $order_child++, $menu_node['id']);
            }
        }
        
        return true;
        
    }
    
    
    // FUNZIONI PER CREARE IL MENU SU CUI APPLICARE IL PLUGIN NESTABLE
    
    public static function buildNestableMenu($menu_id, $language_id){
        // preparo la struttura dati
        
        $nodes_tmp = MenuNode::where('menu_id', '=', $menu_id)->where('language_id', '=', $language_id)->orderBy('order', 'asc')->get();
        MenuNode::$nested_nodes = Array();
        
        MenuNode::$nested_html = '<ol class="dd-list">';
        foreach($nodes_tmp as $item){
            $parent_id = $item->parent_id?$item->parent_id:0;
            MenuNode::$nested_nodes[$parent_id][] = $item;    
        }
     
        if(isset(MenuNode::$nested_nodes[0]) && count(MenuNode::$nested_nodes[0])){
            
            foreach(MenuNode::$nested_nodes[0] as $item){
                MenuNode::_nestedMenuRecursion($item, $language_id);
            }
        }
        MenuNode::$nested_html .= '</ol>';

        return MenuNode::$nested_html;
    }
    
    private static function _nestedMenuRecursion($node, $language_id){
        MenuNode::$nested_html .= '<li class="dd-item dd3-item" data-id="'.$node->id.'" '.$node->extra.'><div class="dd-handle dd3-handle">Drag</div><div class="dd3-content"><span class="badge">'.$node->id.'</span><span class="dd3-label">'.$node->title.'</span><div class="dd3-actions"><a class="node_edit" rel="'.$node->id.'"  href="#"><span class="glyphicon glyphicon-edit"></span></a>                    <a  class="node_add_child node_add" rel="'.$node->id.'"><span class="glyphicon glyphicon-plus-sign" data-language-id="'.$language_id.'"></span></a>                    <a class="node_delete" rel="'.$node->id.'"><span class="glyphicon glyphicon-remove-circle"></span></a></div></div>';
        if(isset(MenuNode::$nested_nodes[$node->id]) && count(MenuNode::$nested_nodes[$node->id]) > 0){
            MenuNode::$nested_html .= '<ol class="dd-list">';
            foreach(MenuNode::$nested_nodes[$node->id] as $item){
                MenuNode::_nestedMenuRecursion($item, $language_id);
            }
            MenuNode::$nested_html .= '</ol>';
        }
        MenuNode::$nested_html .= '</li>';
    }
    
    
    
    
    public static function buildUl($menu_id, $language_id, $ul_class = '', $is_bs3 = true){
        // preparo la struttura dati
        
        $nodes_tmp = MenuNode::where('menu_id', '=', $menu_id)->where('language_id', '=', $language_id)->orderBy('order', 'asc')->get();
        MenuNode::$nested_nodes = Array();
        MenuNode::$structured_nodes = Array();
        
        MenuNode::$nested_html = '<ul class="'.$ul_class.'">';
        foreach($nodes_tmp as $item){
            $parent_id = $item->parent_id?$item->parent_id:0;
            MenuNode::$nested_nodes[$parent_id][] = $item;    
        }
     
        if(isset(MenuNode::$nested_nodes[0]) && count(MenuNode::$nested_nodes[0])){
            
            foreach(MenuNode::$nested_nodes[0] as $item){
                if($is_bs3)
                    MenuNode::_ulRecursionBs3($item);
                else
                    MenuNode::_ulRecursion($item);
            }
        }
        MenuNode::$nested_html .= '</ul>';

        return MenuNode::$nested_html;
    }
    
    private static function _ulRecursion($node){
        MenuNode::$nested_html .= '<li>';
        //if($node->url != '')
             MenuNode::$nested_html .= '<a href="'.self::handleLink($node->url).'"'.($node->url_blank_page?' target="_blank"':'').'>';
        MenuNode::$nested_html .= $node->title;
        //if($node->url != '')
             MenuNode::$nested_html .= '</a>';
        if(isset(MenuNode::$nested_nodes[$node->id]) && count(MenuNode::$nested_nodes[$node->id]) > 0){
            MenuNode::$nested_html .= '<ul>';
            foreach(MenuNode::$nested_nodes[$node->id] as $item){
                MenuNode::_ulRecursion($item);
            }
            MenuNode::$nested_html .= '</ul>';
        }
        MenuNode::$nested_html .= '</li>';
    }
    
    private static function _ulRecursionBs3($node, $ul_class = "dropdown-menu"){
        
        $has_child = isset(MenuNode::$nested_nodes[$node->id]) && count(MenuNode::$nested_nodes[$node->id]) > 0;
        
        if(!$has_child){
            MenuNode::$nested_html .= '<li>';
            MenuNode::$nested_html .= '<a href="'.self::handleLink($node->url).'"'.($node->url_blank_page?' target="_blank"':'').'>';
            MenuNode::$nested_html .= $node->title;
            MenuNode::$nested_html .= '</a>';
        }
        else{
            MenuNode::$nested_html .= '<li class="dropdown">';
            MenuNode::$nested_html .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
            MenuNode::$nested_html .= $node->title;
            MenuNode::$nested_html .= '<span class="caret"></span>';
            MenuNode::$nested_html .= '</a>';
        }
             
        if($has_child){
            MenuNode::$nested_html .= '<ul class="'.$ul_class.'">';
            foreach(MenuNode::$nested_nodes[$node->id] as $item){
                MenuNode::_ulRecursionBs3($item, $ul_class);
            }
            MenuNode::$nested_html .= '</ul>';
        }
        MenuNode::$nested_html .= '</li>';
    }
    
    
    public static function buildHierarchy($menu_id, $language_id){
        $nodes_tmp = MenuNode::where('menu_id', '=', $menu_id)->where('language_id', '=', $language_id)->orderBy('order', 'asc')->get();
        MenuNode::$nested_nodes = Array();
        
        // costruisco l'array di base dove il primo indice rappresenta il padre
        foreach($nodes_tmp as $item){
            $parent_id = $item->parent_id?$item->parent_id:0;
            MenuNode::$nested_nodes[$parent_id][] = $item;    
        }
        
        return MenuNode::$nested_nodes;
        
    }
    
    public static function handleLink($url){
        if($url == ''){
            return '#';
        }
        else if(substr($url,0,7) == 'http://' || substr($url,0,8) == 'https://'){ 
            return $url;
        }
        else{
            if(substr($url,0,1) != '/')
                    $url = '/'.$url;
            return URL::to('/').$url;
        }
    }
    
  
}

?>