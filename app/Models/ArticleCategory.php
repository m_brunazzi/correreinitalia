<?php namespace App\Models;

use Config;
 
class ArticleCategory extends Translatable {
 
    protected $table = 'blm_article_category';
    protected $translatable_fields = Array('name','description');
    protected $searchable_fields = Array('name','description');
    
    protected $validation_rules = Array(
                                     'label' => 'required'
                                     );
    
    public function article()
    {
        return $this->belongsToMany('App\Models\Article', 'blm_article_article_category', 'article_category_id', 'article_id');
    }
  
}

?>