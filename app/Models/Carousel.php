<?php namespace App\Models;

use Config;

 
class Carousel extends Translatable {
 
    protected $table = 'blm_carousel';
    protected $translatable_fields = Array('name','summary');
    protected $searchable_fields = Array('name','summary');
    
    protected $validation_rules = Array(
                                     'label' => 'required'
                                     );
  
}

?>