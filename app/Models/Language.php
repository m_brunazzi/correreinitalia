<?php namespace App\Models;

use Config, Session, Input;
 
class Language extends Content {
 
    protected $table = 'blm_language';
    public static $available_languages = null;
    public static $current_language = null;
    
    
    
    public static function initialize(){
        self::loadLanguages();
        self::$current_language = self::getCurrent();
    }
    
    
   
    public static function getCurrent(){
        
        $session_var = Config::get('bloom.session_language_var');
        $session_data = Session::get($session_var);
        
        if(isset($session_data['code']) && isset($session_data['id'])){
            //$session_data = Session::get($session_var);
            
            \App::setLocale($session_data['code']);
            return $session_data['id'];
        }
        else{
            $code = \App::getLocale();
            
            $language = self::where('code', $code)->get();
            if($language->first()){
                 $session_data = Array('id' => $language->first()->id, 'code' => $language->first()->code);
                 Session::put($session_var, $session_data);
                 \App::setLocale($language->first()->code);
                 return $language->first()->id;
            }
        }
    }
    
    public static function setCurrent($language_id){
        
        $language = self::find($language_id);
        
        //if(!language || !$language->online)
        if(!$language)
            return false;
        
        
        $session_var = Config::get('bloom.session_language_var');
        
        
        if($language && $session_var){
            
            $session_data = Array('id' => $language->id, 'code' => $language->code);
            Session::put($session_var, $session_data);
            \App::setLocale($language->code);
            self::$current_language = $language->id;
            return $language->id;
        }
        else
           return false;
    }
    
    
    
    public static function getLanguages(){
        return self::$available_languages;
    }
    
    public static function getLanguagesOptions($key = 'id'){
        $result = Array();
        
        foreach(self::$available_languages as $item){
            $result[$item->$key] = $item->language;
        }
        
        return $result;
    }
    
    
    
    private static function loadLanguages(){
        
        $languages = self::where('online', 1)->get();
        if($languages){
            foreach($languages as $item){
                self::$available_languages[$item->id] = $item;
            }
            return true;
        }
        return false;
    }
    
    //
    // Converte un array contenente le traduzioni (in cui gli indici sono gli id delle lingue) in una stringa json
    //
    static public function encodeTranslations($translations){
        
        if(!is_array($translations) || count($translations) == 0)
            return false;
        
        // controllo che gli indici corrispondano a lingue disponibili nel database
               
        $validTranslations = Array();
        if(is_array(self::$available_languages) && count(self::$available_languages) > 0){
            foreach(self::$available_languages as $key => $value){
                if(isset(self::$available_languages[$key]) && isset($translations[$key]))
                    $validTranslations[$key] = $translations[$key];
            }   
    
        }
        
        return json_encode($validTranslations);
        
    }
    
    //
    //  Converte una stringa json in un array di traduzione, verificandone indici e contenuto
    //
    static public function decodeTranslations($string){
        
        // Potrebbe servire un TRY CATCH per le eccezioni
        
        
        // controllo che l'array decodificato sia corretto
        $validArray = Array();

        $decoded = json_decode($string, true);
        
        if($decoded === null)
            return false;   // ritorna falso se non riesco a decodificare la stringa
        
        foreach(self::$available_languages as $key => $value){
            if(isset($decoded[$key]) && is_string($decoded[$key]))
                $validArray[$key] = $decoded[$key];
        }
        return $validArray;
        
    }
    
    //
    //  Converte una stringa json in un array di traduzione, verificandone indici e contenuto
    //
    static public function translate($string, $language_id = null){
        
        $no_translation = '---';
        
        try{
            $decoded = json_decode($string, true);
        
            if($decoded === null)
                return $string;    // ritorna la stringa originale se non riesco a decodificare il json
            
            if(!$language_id)
                $language_id = self::$current_language; // se non passo la lingua in cui voglio la traduzione, me lo traduce nella lingua corrente
            
            
            // TODO: valutare se passare la traduzione in un altra lingua nel caso la richiesta non sia disponibile
            if(!isset($decoded[$language_id]))
                throw new \Exception('Traduzione non disponibile');   
            else
                return $decoded[$language_id];
            
        }
        catch(\Exception $e){
            return $no_translation;
        }
                
    }
    
    
    //
// ritorna una stringa contenente un widget per inserire le traduzioni
// 
// @param $codes: Array che contiene elementi in cui l'indice è l'id della lingua e il valore è il suo nome (es. 1 => Italiano)
// @param $translations: Array che contiene elementi in cui l'indice è l'id della lingua e il valore è la traduzione

static public function multilanguageWidget($widget_name, $translations = Array(), $active_code = null, $html_editor = true){
    
    
    
    if(!is_array(self::$available_languages) || count(self::$available_languages) == 0)
        return false;
    
    
    
    if(!isset($active_code)){
        reset(self::$available_languages);
        $active_code = key(self::$available_languages);
    }
    
    $html = '<div role="tabpanel">'."\n";
    
    // Linguette
    $html .= '<ul class="nav nav-tabs nav-tabs-translation" role="tablist">'."\n";
    foreach(self::$available_languages as $id => $language){
        $html .= '<li role="presentation" '.($id == $active_code?'class="active"':'').'><a href="#'.$widget_name.'-'.$language->language.'" aria-controls="'.$language->language.'" role="tab" data-toggle="tab">'.$language->language.'</a></li>'."\n";
        
    }
    $html .= '</ul>'."\n";
    
    // Pannelli
    $html .= '<div class="tab-content">'."\n";
    
    foreach(self::$available_languages as $id => $language){
        
        //$html .= '<li role="presentation" '.($code == $active_code?'class="active"':'').'><a href="#home" aria-controls="'.$language.'" role="tab" data-toggle="tab">'.$language.'</a></li>'."\n";
        $html .= '<div role="tabpanel" class="tab-pane tab-pane-translation '.($id == $active_code?'active':'').'" id="'.$widget_name.'-'.$language->language.'">'."\n";
        $html .= '<div class="html-editor-container">'."\n";
        if($html_editor)
            $html .=        '<textarea name="'.$widget_name.'['.$id.']" class="html-editor">'.@$translations[$id].'</textarea>'."\n";
        else
            $html .=        '<input name="'.$widget_name.'['.$id.']" class="form-control" value="'.strip_tags(@$translations[$id]).'"/>'."\n";
        
        $html .= '<div class="text-center"><small>'.$language->language.'</small></div>'."\n";
        $html .= '</div>'."\n";
        $html .= '</div>'."\n";
    }
    
    $html .= '</div>'."\n";
    
    $html .= '</div>'."\n";
    
    return $html;
}
    
   
 
}

?>