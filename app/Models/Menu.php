<?php namespace App\Models;

use Config;

 
class Menu extends Translatable {
 
    protected $table = 'blm_menu';
    protected $translatable_fields = Array('name','description');
    protected $searchable_fields = Array('name','description');
    
    protected $validation_rules = Array(
                                     'label' => 'required'
                                     );
  
}

?>