<?php

namespace App\Http\Middleware;

use Closure, Auth, Redirect;

class BloomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || !Auth::user()->admin)
            return Redirect::route('public.homepage');
        return $next($request);
    }
}
