<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// PUBLIC
Route::get('/', array('as' => 'public.homepage',       'uses' => 'Www\MainController@homepage'));
Route::get('/article/{article_id}/{article_slug?}', array('as' => 'public.article.show',       'uses' => 'Www\ArticleController@show'));
Route::get('/category/{category_id}/{category_slug?}', array('as' => 'public.category.show',       'uses' => 'Www\ArticleController@category'));



// ADMINISTRATION
Route::get('admin/login',   array('as' => 'admin.login',       'uses' => 'Admin\DashboardController@login'));


Route::get('facebook/login',   array('as' => 'facebook.login',       'uses' => 'Auth\SocialiteController@redirectToProvider'));
Route::get('facebook/check',   array('as' => 'facebook.check',       'uses' => 'Auth\SocialiteController@handleProviderCallback'));

Route::group(array('prefix' => 'admin', 'middleware' => 'bloom.auth'), function(){
    
    //Route::get('dashboard',   array('as' => 'admin.dashboard',       'uses' => 'Admin\DashboardController@index'));
    Route::get('dashboard',   array('as' => 'admin.dashboard',       'uses' => 'Admin\DashboardController@index'));
    Route::resource('Article', 'Admin\ArticleController');
    
    Route::resource('User', 'Admin\UserController');
    Route::resource('Group', 'Admin\GroupController');
    Route::resource('Language', 'Admin\LanguageController');
    Route::resource('ArticleCategory', 'Admin\ArticleCategoryController');
   
    Route::resource('Carousel', 'Admin\CarouselController');
    Route::resource('Menu', 'Admin\MenuController');
    
    Route::get('settings',   array('as' => 'admin.settings',       'uses' => 'Admin\SettingsController@index')); 
    Route::post('settings/save',   array('as' => 'admin.settings.save',       'uses' => 'Admin\SettingsController@save')); 
    Route::get('settings/delete/{group}/{key}',   array('as' => 'admin.settings.delete',       'uses' => 'Admin\SettingsController@delete')); 
});


// RPC
Route::group(array('prefix' => 'rpc'), function(){
    
    // Autenticazione
    Route::post('login',   array('as' => 'rpc.login', 'uses' => 'Rpc\RpcAuthController@postLogin'));
    Route::get('logout',   array('as' => 'rpc.logout', 'uses' => 'Rpc\RpcAuthController@getLogout'));
    
    // Form - immagini
    Route::post('form/upload/picture',   array('as' => 'rpc.form.upload.picture', 'uses' => 'Rpc\RpcFormController@uploadPicture'));
    Route::get('form/delete/picture',   array('as' => 'rpc.form.delete.picture', 'uses' => 'Rpc\RpcFormController@deletePicture'));
    Route::get('form/load/pictures',   array('as' => 'rpc.form.load.pictures', 'uses' => 'Rpc\RpcFormController@loadPictures'));
    Route::get('form/setMain/picture',   array('as' => 'rpc.form.setmain.picture', 'uses' => 'Rpc\RpcFormController@setMainPicture'));
    Route::post('form/rename/picture',   array('as' => 'rpc.form.rename.picture', 'uses' => 'Rpc\RpcFormController@renamePicture'));
    Route::get('form/setOrder/picture',   array('as' => 'rpc.form.setorder.picture', 'uses' => 'Rpc\RpcFormController@setOrderPicture'));
    
    
    // Form - allegati
    Route::post('form/upload/attachment',   array('as' => 'rpc.form.upload.attachment', 'uses' => 'Rpc\RpcFormController@uploadAttachment'));
    Route::get('form/delete/attachment',   array('as' => 'rpc.form.delete.attachment', 'uses' => 'Rpc\RpcFormController@deleteAttachment'));
    Route::get('form/load/attachments',   array('as' => 'rpc.form.load.attachments', 'uses' => 'Rpc\RpcFormController@loadAttachments'));
    Route::post('form/rename/attachment',   array('as' => 'rpc.form.rename.attachment', 'uses' => 'Rpc\RpcFormController@renameAttachment'));
    Route::get('form/setOrder/attachment',   array('as' => 'rpc.form.setorder.attachment', 'uses' => 'Rpc\RpcFormController@setOrderAttachment'));
    
    // Menu
    Route::get('menu/loadNode/{node_id}',   array('as' => 'rpc.menu.loadNode', 'uses' => 'Rpc\RpcMenuController@loadNode'));
    Route::post('menu/saveNode',   array('as' => 'rpc.menu.saveNode', 'uses' => 'Rpc\RpcMenuController@saveNode'));
    Route::get('menu/deleteNode/{node_id}',   array('as' => 'rpc.menu.deleteNode', 'uses' => 'Rpc\RpcMenuController@deleteNode'));
    Route::post('menu/saveTaxonomy',   array('as' => 'rpc.menu.saveTaxonomy', 'uses' => 'Rpc\RpcMenuController@saveTaxonomy'));
    Route::get('menu/loadMenu/{menu_id}/{language_id}',   array('as' => 'rpc.menu.loadMenu', 'uses' => 'Rpc\RpcMenuController@loadMenu'));
    Route::get('menu/duplicateMenu/{menu_id}/{source_language_id}/{destination_language_id}',   array('as' => 'rpc.menu.duplicateMenu', 'uses' => 'Rpc\RpcMenuController@duplicateMenu'));
});