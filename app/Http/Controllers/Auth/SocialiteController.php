<?php

namespace App\Http\Controllers\Auth;
use Socialize;
use Auth;
use User;
use Redirect;

class SocialiteController extends \App\Http\Controllers\Controller
{
    
    public $redirect_route = 'public.homepage';
    
    public function redirectToProvider()
    {
        return Socialize::with('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        
        $user = Socialize::with('facebook')->user();
        $email = $user->getEmail();
        
        // Check if a user with the facebook email exists
        
        $local = User::where('email', $email)->first();
        if($local){
            // existent user login
            Auth::loginUsingId($local->id);
        }
        else{
            // create new user and login
            
            $local = new User;
            $local->name= $user->getName();
            $local->email= $email;
            $local->password= bcrypt(uniqid());
            $local->active= true;
            $local->admin= false;
            $local->save();
            
            Auth::loginUsingId($local->id);
        }
        
        return Redirect::route($this->redirect_route);
        //dd($user);
        // $user->token;
    }
}
