<?php 
namespace App\Http\Controllers\Www;

use App\Http\Controllers\PublicController;

use Auth, Form, Input, Redirect, Sentry, View, Notification, Config;

use Meta;
use Article;
use ArticleCategory;

class ArticleController extends PublicController {
    
    function show($article_id, $description = null){
        
        $article = Article::enableTranslation()->with('picture', 'attachment')->find($article_id);
        
        Meta::set('title', $article->name);
        Meta::set('description', $article->description);
        if(@$article->picture->first())
            Meta::set('image', asset(@$article->picture->first()->filename));
        
        return View::make('www.Article.show')
                ->with('article', $article);
         
    }
    
    function category($category_id, $description = null){
        
        Article::enableTranslation();
        $category = ArticleCategory::enableTranslation()->with('article', 'picture', 'attachment')->find($category_id);
        
        Meta::set('title', $category->name);
        Meta::set('description', $category->description);
        if($category->picture->first())
            Meta::set('image', asset($category->picture->first()->filename));
        
        return View::make('www.ArticleCategory.show')
                ->with('category', $category);
    }
    
    
    
}