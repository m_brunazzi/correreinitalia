<?php 
namespace App\Http\Controllers\Www;
 
use App\Http\Controllers\PublicController;
use Auth, BaseController, Form, Input, Redirect, View, Notification, Config;


class MainController extends PublicController {
    
    
    function homepage(){
        
        return View::make('www.homepage');
    }
    
}