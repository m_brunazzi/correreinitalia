<?php

namespace App\Http\Controllers;

abstract class BloomController extends Controller
{
    public function __construct()
    {
        # Default title
        Meta::title(Config::get('bloom.site_name'));

        # Default robots
        Meta::meta('robots', 'index,follow');
        
    }
    
    function asset_css($css){
        $this->head_assets['css'][] = $css;
    }

    function asset_js($js){
        $this->head_assets['js'][] = $js;
    }
    
}
