<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;

use Article;
use ArticleCategory;

use Language;
use Tag;
use Content;


class ArticleController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = new Article;
                
                
                
                $rows = $rows->filter($this->search_string);
                
                
                $article_category_id = Input::get('article_category_id');
                if($article_category_id){
                   $rows = $rows->whereHas('category', function ($query) use ($article_category_id) {
                        $query->where('blm_article_category.id', '=', $article_category_id);
                    });
                }
                
                $rows = $rows->with('category')->paginate(Config::get('bloom.admin_items_per_page'));
                
		return View::make('admin.Article.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('options_category' , collection_options(ArticleCategory::get(), 'label'))
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Articoli' => route('admin.Article.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Article.store',
                'model_name' => 'Article'
                
            );
            
            
                        
            return View::make('admin.Article.form')
                    ->with('tags_favourites', Tag::favourites(10))  
                    ->with('form', $form)
                    ->with('categories', ArticleCategory::all());
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
 
            if (Article::validate()){
				$record = new Article;
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->summary = Language::encodeTranslations(Input::get('summary'));
                $record->description = Language::encodeTranslations(Input::get('description'));
                $record->date = Input::get('date');
                $record->online = Input::get('online');
              
                $record->save();
                
                $record->category()->sync(is_array(Input::get('category'))?Input::get('category'):Array());
                
                $tags_array = explode(',', Input::get('tags'));
                $tags = Tag::find_or_create($tags_array);
                $record->tag()->sync($tags);
                
                
                
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Article.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Article::with('tag')->find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Articoli' => route('admin.Article.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Article'
            );

            return View::make('admin.Article.form')
                        ->with('form', $form)                        
                        ->with('tags_favourites', Tag::favourites(10))                        
                        ->with('record', $record)
                        ->with('categories', ArticleCategory::all());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (Article::validate()){
		$record = Article::find($id);
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->summary = Language::encodeTranslations(Input::get('summary'));
                $record->description = Language::encodeTranslations(Input::get('description'));
                $record->date = Input::get('date');
                $record->online = Input::get('online');
                
                $record->save();
                
                $record->category()->sync(is_array(Input::get('category'))?Input::get('category'):Array());
                
                $tags_array = explode(',', Input::get('tags'));
                $tags = Tag::find_or_create($tags_array);
                $record->tag()->sync($tags);
                                
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Article.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Article::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Article::find($id);
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Article.index');
                
	}

}