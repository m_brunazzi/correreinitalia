<?php 

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;


use Carousel;

use Language;
use Tag;
use Content;




class CarouselController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = new Carousel;
                
                if($this->search_string){
                    $rows = $rows->filter(Array('search' => $this->search_string));
                }
                
                $rows = $rows->paginate(Config::get('bloom.admin_items_per_page'));
                
		return View::make('admin.Carousel.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Carousel' => route('admin.Carousel.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Carousel.store',
                'model_name' => 'Carousel',
                'hide_pictures' => true,
                'hide_attachments' => true,
                
            );
            
            return View::make('admin.Carousel.form')
                    ->with('tags_favourites', Tag::favourites(10))  
                    ->with('form', $form);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
 
            if (Carousel::validate()){
		$record = new Carousel;
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->summary = Language::encodeTranslations(Input::get('summary'));
                $record->online = Input::get('online');
                $record->image = $record->handleUpload('image', Config::get('bloom.public_images_upload_folder'));
                $record->save();
                
                $tags_array = explode(',', Input::get('tags'));
                $tags = Tag::find_or_create($tags_array);
                $record->tag()->sync($tags);
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Carousel.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Carousel::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Carousel::with('tag')->find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Carousel' => route('admin.Carousel.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Carousel',
                'hide_pictures' => true,
                'hide_attachments' => true,
                
            );

            return View::make('admin.Carousel.form')
                        ->with('form', $form)                        
                        ->with('tags_favourites', Tag::favourites(10))                        
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (Carousel::validate()){
		$record = Carousel::find($id);
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->summary = Language::encodeTranslations(Input::get('summary'));
                $record->online = Input::get('online');
                $record->image = $record->handleUpload('image', Config::get('bloom.public_images_upload_folder'));
                
                $record->save();
                
                $tags_array = explode(',', Input::get('tags'));
                $tags = Tag::find_or_create($tags_array);
                $record->tag()->sync($tags);
                                
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Carousel.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Carousel::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Carousel::find($id);
                $record->delete();
                
                // TODO: cancellare l'immagine
                
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Carousel.index');
	}

}