<?php 
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config, DB;

use Language;
use ArticleCategory;
use Tag;


class ArticleCategoryController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            
            
                $rows = new ArticleCategory;
                $rows = $rows->filter($this->search_string);
                $rows = $rows->with('article')->paginate(Config::get('bloom.admin_items_per_page'));
                
                
                
		return View::make('admin.ArticleCategory.list')
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Categorie di articolo' => route('admin.ArticleCategory.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.ArticleCategory.store',
                'model_name' => 'ArticleCategory'
                
            );
            
            
                        
            return View::make('admin.ArticleCategory.form')
                    ->with('tags_favourites', Tag::favourites(10))  
                    ->with('form', $form);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
 
            if (ArticleCategory::validate()){
		$record = new ArticleCategory;
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->description = Language::encodeTranslations(Input::get('description'));
                
                // Gestire titolo e descrizione
                
                $record->save();
                
                $tags_array = explode(',', Input::get('tags'));
                $tags = Tag::find_or_create($tags_array);
                $record->tag()->sync($tags);
            
              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.ArticleCategory.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(ArticleCategory::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = ArticleCategory::with('tag')->find($id);
            
            
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Categorie di articolo' => route('admin.ArticleCategory.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'ArticleCategory'
                
            );

            return View::make('admin.ArticleCategory.form')
                        ->with('form', $form)                        
                        ->with('tags_favourites', Tag::favourites(10))                        
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (ArticleCategory::validate()){
		$record = ArticleCategory::find($id);
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->description = Language::encodeTranslations(Input::get('description'));
                $record->save();
                
                $tags_array = explode(',', Input::get('tags'));
                $tags = Tag::find_or_create($tags_array);
                $record->tag()->sync($tags);
                                
             
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.ArticleCategory.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(ArticleCategory::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = ArticleCategory::find($id);
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.ArticleCategory.index');
	}

}