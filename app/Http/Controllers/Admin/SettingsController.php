<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

use Setting, Notification, DB,View, Input, Redirect, Config;

class SettingsController extends AdminController {

	public function index()
        {
            //$rows = DB::table('settings')->get();
            $data = Config::listDb($wildcard = null)->orderBy('key')->get();
            $rows  = Array();
            foreach($data as $item){
                
                
                $key_parsed = explode('.',$item->key);
               
                $row = new \StdClass();
                $row->id = $item->id;
                $row->group = $key_parsed[0];
                $row->key = $key_parsed[1];
                $row->value = json_decode($item->value);
                    
                
                $rows[] = $row;
            }
            return View::make('admin.settings.list')
                    ->with('rows', $rows);
        }
        
        
	public function save()
	{
            $key_new = snake_case(Input::get('key_new'));
            $group_new = snake_case(Input::get('group_new'));
            $value_new = Input::get('value_new');

            if($key_new && $value_new){
                Config::store($group_new.'.'.$key_new, $value_new);
                Notification::success('Parametro <strong>'.$group_new.'.'.$key_new.'</strong>salvato correttamente.');
            }
            
            $keys = Input::get('key');
            $groups = Input::get('group');
            $values = Input::get('value');
            
            
            
            if($keys && $groups && $values){
                
                
                foreach($keys as $index => $value){

                    if(isset($groups[$index]) && $groups[$index] != '' && isset($values[$index]) && $values[$index] != ''){
                        $key = snake_case($keys[$index]);
                        $group = snake_case($groups[$index]);
                        $value = $values[$index];
                        
                        Config::store($group.'.'.$key, $value);

                    }
                }
            }
            
            Notification::success('Parametri aggiornati correttamente.');
            
            return Redirect::route('admin.settings');
            
            
	}
        
        
        public function delete($group, $key)
        {
            Config::forget($group.'.'.$key);
            Notification::success('Parametro <strong>'.$group.'.'.$key.'</strong> cancellato correttamente.');
            return Redirect::route('admin.settings');
        }

}