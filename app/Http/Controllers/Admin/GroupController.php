<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;
use App\Models\Group;


use App\Services\Validators\GroupValidator;

class GroupController extends AdminController{
        
        var $admin_group_id = null;
    
        public function __construct(){
            parent::__construct();
            
            
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = Group::with('user');
                
                $rows = $rows->paginate(Config::get('bloom.admin_items_per_page'));
                
                $query_string = array_except(Input::all(), array('page'));
                
		return View::make('admin.Group.list')
                        ->with('rows' , $rows)
                        ->with('admin_group_id' , $this->admin_group_id)
                        ->with('query_string' , $query_string);
                
                
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
            $record = new Group;
            
          
              
            return View::make('admin.Group.form')
                        ->with('record' , $record);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $validation = new GroupValidator;
 
            if ($validation->passes()){
		$record = new Group;
                $record->name = Input::get('name');
                $record->description = Input::get('description');
                $record->save();
                
                                
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Group.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            
            
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Group::find($id);
            
            return View::make('admin.Group.form')
            
                        ->with('record' , $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validation = new GroupValidator;
 
            if ($validation->passes()){
		$record = Group::find($id);
                $record->name = Input::get('name');
                $record->description = Input::get('description');
                $record->save();
                
                              
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Group.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
                if($id != $this->admin_group_id){
                    $record = Group::find($id);
                
                    $record->delete();
                
                    Notification::success('Record cancellato correttamente.');
                    return Redirect::route('admin.Group.index');
                }
                else{
                    Notification::success('Impossibile cancellare il gruppo degli amministratori');
                    return Redirect::route('admin.Group.index');
                    
                }
	}

}