<?php namespace App\Controllers\Admin;
 
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;
use File;
use App\Models\Picture;
use App\Models\Collection;
use App\Models\Product;
use App\Models\Text;
use App\Services\Validators\PictureValidator;

class PictureController extends \CrudController {
    
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                
               // la lista è nel modulo delle linee
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                $pictureable_type = Input::get('pictureable_type');
                $pictureable_id = Input::get('pictureable_id');
                
                $pictureable = $pictureable_type::find($pictureable_id);
                
                if(!$pictureable_type || !$pictureable_id)
                    throw new \Exception('Errore nella richiesta');
                
                  $txt_items = Array(
                        Array('name' => 'txt_title', 'label' => 'Titolo')
                   );

		 return View::make('admin.Picture.form')
                         ->with('pictureable' , $pictureable)
                         ->with('pictureable_type' , $pictureable_type)
                         ->with('pictureable_id' , $pictureable_id)
                         ->with('multilanguage_form' , Text::multilanguage_form($txt_items));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            $validation = new PictureValidator;
 
            if ($validation->passes()){
		
                $pictureable_type = Input::get('pictureable_type');
                $pictureable_id = Input::get('pictureable_id');
                
                $pictureable = $pictureable_type::find($pictureable_id);
                
                $record = new Picture;
                $record->label = Input::get('label');
                $record->main = Input::get('main');

                //$record->pictureable_type = Input::get('pictureable_type');
                //$record->pictureable_id = Input::get('pictureable_id');
                $record->save();
                
                $title = Text::make($record->id, 'Picture.txt_title', Input::get('txt_title'),Input::get('txt_title_id'));
                $record->title()->associate($title);
               
                $record->img = handle_file_upload('img',$record->label);
                
                $pictureable->picture()->save($record);
                
                //$record->save();
                
                //$pictureable_id = $record->pictureable_id;
                //$pictureable_type = $record->pictureable_type;

                Notification::success('Record salvato correttamente.');
                return Redirect::to(route('admin.'.$pictureable_type.'.edit', $pictureable_id).'#images');
                //return Redirect::route('admin.'.$pictureable_type.'.edit', $pictureable_id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
            
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->edit($id);
	}
        
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Picture::with('title')->find($id);
            
            //$pictureable = $record->pictureable_type::find($record->pictureable_id);
            
            $title = @Text::find($record->txt_title)->translation;
            
            $txt_items = Array(
                        Array('name' => 'txt_title', 'label' => 'Titolo', 'value' => @$record->txt_title, 'translation' => collection_options(@$title,'translation','language_id'))
                    );

            return View::make('admin.Picture.form')
                    ->with('record', $record)
                    ->with('multilanguage_form' , Text::multilanguage_form($txt_items));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		 $validation = new PictureValidator;
 
                if ($validation->passes()){
                    $record = Picture::find($id);
                    $record->label = Input::get('label');
                    $record->main = Input::get('main');
                    $record->save();
                
                    $title = Text::make($record->id, 'Picture.txt_title', Input::get('txt_title'),Input::get('txt_title_id'));
                    $record->title()->associate($title);
                   
                    
                    $record->img = handle_file_upload('img',$record->label, @$record->img);
                    $record->save();
                    
                    $pictureable_id = $record->pictureable_id;
                    $pictureable_type = $this->extract_picturable_type($record->pictureable_type);
                    
                    Notification::success('Record salvato correttamente.');
                    return Redirect::to(route('admin.'.$pictureable_type.'.edit', $pictureable_id).'#images');
                }
            
                return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Picture::find($id);
                
                $pictureable_id = $record->pictureable_id;
                $pictureable_type = $this->extract_picturable_type($record->pictureable_type);
                
                
                if($record->img != '')
                    unlink($record->img);                   
                
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                //return Redirect::route('admin.'.$pictureable_type.'.edit', $pictureable_id);
                return Redirect::to(route('admin.'.$pictureable_type.'.edit', $pictureable_id).'#images');
	}

}