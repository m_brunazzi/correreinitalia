<?php 

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
 
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;
use Language;
use App\Services\Validators\LanguageValidator;

class LanguageController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                
               $rows = new Language;
                
                if($this->search_string){
                    $rows = $rows->search($this->search_string);
                }
                
               
                
                $rows = $rows->paginate(Config::get('bloom.admin_items_per_page'));
                
                
                
		return View::make('admin.Language.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		 return View::make('admin.Language.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            $validation = new LanguageValidator;
 
            if ($validation->passes()){
		$record = new Language;
                $record->language = Input::get('language');
                $record->code = Input::get('code');
                $record->online = Input::get('online');
                $record->icon = $record->handleUpload('icon', Config::get('bloom.public_images_upload_folder'));
                $record->save();
                
                
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Language.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
            
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            return View::make('admin.Language.form')->with('record', Language::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		 $validation = new LanguageValidator;
 
                if ($validation->passes()){
                    $record = Language::find($id);
                    $record->language = Input::get('language');
                    $record->code = Input::get('code');
                    $record->online = Input::get('online');
                    $record->icon = $record->handleUpload('icon', Config::get('bloom.public_images_upload_folder'));
                    $record->save();
                    Notification::success('Record salvato correttamente.');
                    return Redirect::route('admin.Language.edit', $record->id);
                }
            
                return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = language::find($id);
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Language.index');
	}

}