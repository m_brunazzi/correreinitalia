<?php namespace 
App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

use Form, Input, Redirect, View, Notification, Config, Mail, Validator;
use Illuminate\Support\MessageBag;
use User, Country, Group;

class UserController extends AdminController{
    
        var $admin_user_id = null;
    
        public function __construct(){
            parent::__construct();
            $admin = User::where('email', '=', Config::get('bloom.admin_email'))->first();
            if($admin)
                $this->admin_user_id = $admin->id;
        }

        protected function validator(array $data, $new = true)
        {
            
            if($new)
                $rules = [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
                ];
            else
                $rules = [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'password' => 'confirmed|min:6',
                ];
            
            return Validator::make($data, $rules);
        }
        
        
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $rows = new User;
            $rows = $rows->paginate(Config::get('bloom.admin_items_per_page'));

            $query_string = array_except(Input::all(), array('page'));

            return View::make('admin.User.list')
                    ->with('rows' , $rows)
                    ->with('admin_user_id' , $this->admin_user_id)
                    ->with('query_string' , $query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            $record = new User;
            
            $groups = Group::all();
            $groups_ids = [];
            
            
            return View::make('admin.User.form')
                        //->with( 'options_country' , collection_options(Country::orderBy('name')->get(), 'name'))
                        ->with('groups' , $groups)
                        ->with('groups_ids' , $groups_ids)
                        ->with('record' , $record);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $validator = self::validator(Input::all());
            if($validator->fails()){
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            
            $user = new User;
            $user->name= Input::get('name');
            $user->email= Input::get('email');
            $user->password= bcrypt(Input::get('password'));
            $user->active= Input::get('active');
            $user->admin= Input::get('admin');
            $user->save();
            
            $user->group()->sync(Input::get('group')?Input::get('group'):array());
            
            Notification::success('Record salvato correttamente.');
            return Redirect::route('admin.User.edit', $user->id);
            
            
            
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
            
            
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
           
            $record = User::with('group')->find($id);
                    
            $groups = Group::all();
            $groups_ids = [];
            
            foreach($record->group as $item){
                $groups_ids[] = $item->id;
            }
            
            return View::make('admin.User.form')
                        ->with( 'options_country' , collection_options(Country::orderBy('name')->get(), 'name'))
                        ->with('groups' , $groups)
                        ->with('groups_ids' , $groups_ids)
                        ->with('record' , $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            
            $validator = self::validator(Input::all(), false);
            if($validator->fails()){
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            
            $user = User::find($id);
            $user->name= Input::get('name');
            $user->email= Input::get('email');
            if(Input::get('password'))
                $user->password= bcrypt(Input::get('password'));
            $user->active= Input::get('active');
            $user->admin= Input::get('admin');
            $user->save();
            
            $user->group()->sync(Input::get('group')?Input::get('group'):array());
            
            Notification::success('Record salvato correttamente.');
            return Redirect::route('admin.User.edit', $user->id);
            
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{       
            
            $messages = new MessageBag;
           
            try{
               
                if($id == $this->admin_user_id){
                      
                    Notification::error('Impossibile cancellare il gruppo degli amministratori');
                    throw new \Exception('Cannot delete administrator');
                }
                
                $user = User::find($id);
                $user->group()->detach();
                $user->delete();
                
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.User.index');
            }
            catch (\Exception $e)
            {
                $messages->add('deletion_error', $e->getMessage());
            }
           
            
            return Redirect::route('admin.User.index')->withErrors($messages);;
                    
            
	}
        
        
        

}