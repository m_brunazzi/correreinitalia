<?php namespace App\Controllers\Admin;
 
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;
use File;
use App\Models\Attachment;
use App\Models\Collection;
use App\Models\Product;
use App\Models\Text;
use App\Models\AttachmentType;

use App\Services\Validators\AttachmentValidator;

class AttachmentController extends \CrudController {
    
    
        private function extract_attachmentable_type($model){
            $tokens = explode('\\', $model);
            return end($tokens);
            
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                
               // la lista è nel modulo dei model associati
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
                $attachmentable_type = Input::get('attachmentable_type');
                $attachmentable_id = Input::get('attachmentable_id');
                
                if(!$attachmentable_type || !$attachmentable_id)
                    throw new \Exception('Errore nella richiesta');
                    
                
                  $txt_items = Array(
                        Array('name' => 'txt_title', 'label' => 'Titolo')
                   );

		 return View::make('admin.Attachment.form')
                         ->with('attachmentable_type' , $attachmentable_type)
                         ->with('attachmentable_id' , $attachmentable_id)
                         ->with('options_type' , collection_options(AttachmentType::orderBy('label')->get(), 'label'))
                         ->with('multilanguage_form' , Text::multilanguage_form($txt_items));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
            $validation = new AttachmentValidator;
 
            if ($validation->passes()){
		
                $attachmentable_type = Input::get('attachmentable_type');
                $attachmentable_id = Input::get('attachmentable_id');
                
                
                
                $attachmentable = $attachmentable_type::find($attachmentable_id);
                
                $record = new Attachment;
                $record->label = Input::get('label');
                $record->online = Input::get('online');
                $record->type_id = Input::get('type_id');

                $record->save();
                
                $title = Text::make($record->id, 'Attachment.txt_title', Input::get('txt_title'),Input::get('txt_title_id'));
                $record->title()->associate($title);
                
                $description = Text::make($record->id, 'Attachment.txt_description', Input::get('txt_$description'),Input::get('txt_$description_id'));
                $record->description()->associate($description);
               
                $upload_result = handle_attachment_upload('file',$record->label);
                $record->file = $upload_result['file'];
                $record->size = $upload_result['size'];
                $record->extension = $upload_result['extension'];
                
                $attachmentable->attachment()->save($record);
                
                //$record->save();
                
                //$attachmentable_id = $record->attachmentable_id;
                //$attachmentable_type = $record->attachmentable_type;

                Notification::success('Record salvato correttamente.');
                return Redirect::to(route('admin.'.$attachmentable_type.'.edit', $attachmentable_id).'#attachment');
            }
            
            return Redirect::back()->withInput()->withErrors($validation->errors);
            
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$this->edit($id);
	}

        
        
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            $record = Attachment::with('title', 'description')->find($id);
            
            $title = @Text::find($record->txt_title)->translation;
            $description = @Text::find($record->txt_description)->translation;
            
            $txt_items = Array(
                        Array('name' => 'txt_title', 'label' => 'Titolo', 'value' => @$record->txt_title, 'translation' => collection_options(@$title,'translation','language_id')),
                        Array('name' => 'txt_description', 'label' => 'Descrizione', 'value' => @$record->txt_description, 'translation' => collection_options(@$description,'translation','language_id'), 'is_html' => true)
                    );

            return View::make('admin.Attachment.form')
                    ->with('record', $record)
                    ->with('options_type' , collection_options(AttachmentType::orderBy('label')->get(), 'label'))
                    ->with('multilanguage_form' , Text::multilanguage_form($txt_items));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		 $validation = new AttachmentValidator;
 
                if ($validation->passes()){
                    $record = Attachment::find($id);
                    $record->label = Input::get('label');
                    $record->type_id = Input::get('type_id');
                    $record->online = Input::get('online');
                    $record->save();
                
                    $title = Text::make($record->id, 'attachment.txt_title', Input::get('txt_title'),Input::get('txt_title_id'));
                    $record->title()->associate($title);
                    
                    $description = Text::make($record->id, 'attachment.txt_description', Input::get('txt_description'),Input::get('txt_description_id'));
                    $record->description()->associate($description);
                   
                    
                    $upload_result = handle_attachment_upload('file',$record->label, @$record);
                    $record->file = $upload_result['file'];
                    $record->size = $upload_result['size'];
                    $record->extension = $upload_result['extension'];
                    
                    $record->save();
                    
                    $attachmentable_id = $record->attachmentable_id;
                    $attachmentable_type = $this->extract_attachmentable_type($record->attachmentable_type);
                    
                    Notification::success('Record salvato correttamente.');
                    return Redirect::to(route('admin.'.$attachmentable_type.'.edit', $attachmentable_id).'#attachment');
                }
            
                return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Attachment::find($id);
                
                $attachmentable_id = $record->attachmentable_id;
                $attachmentable_type = $this->extract_picturable_type($record->attachmentable_type);
                
                unlink($record->file);
                
                $record->delete();
                Notification::success('Record cancellato correttamente.');
                return Redirect::to(route('admin.'.$attachmentable_type.'.edit', $attachmentable_id).'#attachment');
	}

}