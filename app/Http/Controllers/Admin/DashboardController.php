<?php 

namespace App\Http\Controllers\Admin;


use View, Notification, Cache;
use App\Http\Controllers\AdminController;

class DashboardController extends AdminController {

	public function login()
        {
            return View::make('admin.auth.login');
        }
        
	public function index()
	{
            
         return View::make('admin.dashboard');
	}
        
        public function clearCache()
	{
            Cache::flush();
            Notification::success('Cache svuotata con successo');
            return Redirect::route('admin.dashboard');
	}
}