<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AdminController;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Notification, Config;


use Menu;
use MenuNode;

use Language;
use Tag;
use Content;




class MenuController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
                $rows = new Menu;
                
                if($this->search_string){
                    $rows = $rows->filter(Array('search' => $this->search_string));
                }
                
                $rows = $rows->paginate(Config::get('bloom.admin_items_per_page'));
                
		return View::make('admin.Menu.list')
                        ->with('search' , $this->search_string)
                        ->with('rows' , $rows)
                        ->with('query_string' , $this->query_string);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
                        
            // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Menu' => route('admin.Menu.index'),
                    'Modulo' => ''
                    ),
                'action' => 'admin.Menu.store',
                'model_name' => 'Menu',
                'hide_pictures' => true,
                'hide_attachments' => true,
                
            );
            
            return View::make('admin.Menu.form')
                    ->with('form', $form);
                
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
 
            if (Menu::validate()){
		$record = new Menu;
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->description = Language::encodeTranslations(Input::get('description'));
                $record->save();
                
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Menu.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Menu::$errors['validation']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            
            $this->asset_js(asset('bower_components/Nestable/jquery.nestable.js')); // Nestable JS
            $this->asset_css(asset('assets/css/nestable.css')); // Nestable CSS
            
            $record = Menu::with('tag')->find($id);
            
             // uso il modulo generico, passandogli i parametri che servono per la personalizzazione
            
            $form = Array(
                'title' => $record->label,
                'breadcrumbs' =>  Array(
                    'Dashboard' => route('admin.dashboard') ,
                    'Menu' => route('admin.Menu.index'),
                    'Modulo' => ''
                    ),
                'model_name' => 'Menu',
                'hide_pictures' => true,
                'hide_attachments' => true,
                
            );
            /*
            $nested_menu_html = Array();
            foreach($this->languages as $key => $value){
                $nested_menu_html[$key] = MenuNode::buildNestableMenu($record->id, $value);
            }
            */
            

            return View::make('admin.Menu.form')
                        ->with('form', $form)   
                        //->with('nested_menu_html', $nested_menu_html)
                        ->with('record', $record);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
 
            if (Menu::validate()){
		$record = Menu::find($id);
                $record->label = Input::get('label');
                $record->name = Language::encodeTranslations(Input::get('name'));
                $record->description = Language::encodeTranslations(Input::get('description'));
                $record->save();
                
                Notification::success('Record salvato correttamente.');
                return Redirect::route('admin.Menu.edit', $record->id);
            }
            
            return Redirect::back()->withInput()->withErrors(Menu::$errors['validation']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$record = Menu::find($id);
                $record->delete();
                
                Notification::success('Record cancellato correttamente.');
                return Redirect::route('admin.Menu.index');
	}

}