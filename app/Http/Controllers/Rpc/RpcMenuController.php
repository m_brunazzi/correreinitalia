<?php 
 namespace App\Http\Controllers\Rpc;


use App\Http\Controllers\AdminController;

use Response, Input, Config, \Carbon\Carbon, Mail, \Exception;
//use App\Models\Contact;
use App\Models\MenuNode;

class RpcMenuController extends AdminController {
    
    function saveNode(){
        
        
        $id = Input::get('id');
        $menu_id = Input::get('menu_id');
        $language_id = Input::get('language_id');
        $title = Input::get('title');
        $url = Input::get('url');
        $url_blank_page = Input::get('url_blank_page');
        $extra = Input::get('extra');
        
        
        
        $result = Array();
        
        try{
            if(!$id)
                $node = new MenuNode; 
            else
                $node = MenuNode::find($id); 
                
            if(!$node)
                throw new Exception('Impossibile salvare il nodo');
            
            $node->menu_id = $menu_id;
            $node->language_id = $language_id;
            $node->title = $title;
            $node->url = $url;
            $node->url_blank_page = $url_blank_page;
            
            $node->extra = $extra;
            
            $node->save();
            
            $result['status'] = 1;
            $result['message'] = 'Nodo salvato correttamente';
            $result['data'] = $node->toArray();
                
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
            
        }
        
        return Response::json($result);
        
    }
    
    function loadNode($node_id){
        
        
        $result = Array();
        
        try{
            $node = MenuNode::find($node_id);
            
            if(!$node)
                throw new \Exception('Nodo non trovato');
            
            $result['status'] = 1;
            $result['message'] = 'Nodo caricato correttamente';
            $result['data'] = $node->toArray();
            
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
            
        }
        
        return Response::json($result);
        
    }
    
    function deleteNode($node_id){
        
        
        $result = Array();
        
        try{
            MenuNode::deleteRecursive($node_id);
            
            $result['status'] = 1;
            $result['message'] = 'Nodo cancellato correttamente';
            
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
            
        }
        
        return Response::json($result);
        
    }
        
    function saveTaxonomy(){
        
         $result = Array();
        
        try{
           $taxonomy = Input::get('taxonomy');
           
           
           
            
            $order = 1;
            foreach($taxonomy as $item){
                MenuNode::saveTaxonomyNode($item, $order++, null);
                
            }
            
            
            
            $result['status'] = 1;
            $result['message'] = 'Menu salvato correttamente';
            
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
            
        }
        
        return Response::json($result);
    
        
        
    }
    
    
    
    function loadMenu($menu_id, $language_id){
        
       
        $result = Array();
        
        try{
            $menu_html = MenuNode::buildNestableMenu($menu_id, $language_id);
           
            $result['status'] = 1;
            $result['message'] = 'Menu caricato correttamente';
            $result['data'] = $menu_html;
            $result['language_id'] = $language_id;
            
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
            
        }
        
        return Response::json($result);
        
    }
    
    
    
    function duplicateMenu($menu_id, $source_language_id, $destination_language_id){
        
       
        $result = Array();
        
        try{
            
            // Cancello tutti i nodi nella lingua di destinazione
            MenuNode::where('language_id', $destination_language_id)->delete();
            
            
            // Carico  i nodi nella lingua originale
            $nodes = MenuNode::where('menu_id', $menu_id)->where('language_id', $source_language_id)->get();
            
            //$mapping = Array();
            $created = Array();
            
            foreach($nodes as $item){
                
                $new = $item->replicate();
                $new->language_id = $destination_language_id;
                $new->save();
                
                $created[$item->id] = $new;
            }
            
            foreach($created as $item){
                
                // Devo riassegnare il parent_id
                if($item->parent_id && isset($created[$item->parent_id])){
                    $item->parent_id = $created[$item->parent_id]->id;
                    $item->save();
                    
                }
            }
            
            
            
            $result['status'] = 1;
            $result['message'] = 'Menu duplicato correttamente';
            $result['menu_id'] = $menu_id;
            $result['language_id'] = $destination_language_id;
            
        }
        catch(\Exception $e){
            $result['status'] = 0;
            $result['message'] = $e->getMessage();
            
        }
        
        return Response::json($result);
        
    }
}