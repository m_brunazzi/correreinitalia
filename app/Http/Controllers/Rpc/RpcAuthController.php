<?php namespace App\Http\Controllers\Rpc;
 
use Form, Input, Redirect, Auth;
use User;
use Notification;
use App\Http\Controllers\Controller;

class RpcAuthController extends Controller {
    
    public function postLogin()
    {
        $credentials = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password'),
            'active' => 1,
            'admin' => 1,
        );
       
        try
        {
            $user = Auth::attempt($credentials);
            
            if ($user)
            {
                $destination = route('admin.dashboard');
                return Redirect::to($destination);
            }
            else
            {
                Notification::error('Account non valido');
                return Redirect::back();
            }
        }
        catch(\Exception $e)
        {
            Notification::error($e->getMessage());
            return Redirect::back();
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return Redirect::route('public.homepage');
    }

}
