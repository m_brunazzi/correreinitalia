<?php 

namespace App\Http\Controllers\Rpc;


use App\Http\Controllers\AdminController;
 
use BaseController, Form, Input, Redirect, Response, Config;

use Notification;
use Picture;
use Attachment;
use Image;

class RpcFormController extends AdminController {
    
    // PICTURES
    
    public function uploadPicture()
    {
        $data = Array();
            
        try{
            
            $pictureable_type = Input::get('pictureable_type');
            $pictureable_id = Input::get('pictureable_id');
            $destination_path = Input::get('destination_path');
            
            if(!$destination_path)
                throw new \Exception('Destination Path not set');

            // verifico che esista la classe ricevuta 
            if(!class_exists($pictureable_type))
                throw new \Exception('Eloquent Model not found');
            
            // carico il record indicato
            $pictureable = $pictureable_type::find($pictureable_id);
            if(!$pictureable)
                throw new \Exception('Record not found');
            
            // creo un nuovo record di tipo Picture
            $picture = new Picture;
            
            
            // gestisco il file ricevuto
            $file = Input::file('file');
            $picture->label = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension(); //if you need extension of the file
            
            $filename = uniqid().'.'.$extension;
            $uploadSuccess = $file->move($destination_path, $filename);
            
            $picture->filename = $destination_path.$filename;
            $picture->save();
            
            
            $pictureable->picture()->save($picture);
            
            $data['status'] = 1;
            $data['label'] = $picture->label;
            $data['filename'] = $picture->filename;
               
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    public function loadPictures()
    {
        $data = Array();
            
        try{
            
            $pictureable_type = Input::get('pictureable_type');
            $pictureable_id = Input::get('pictureable_id');

            // verifico che esista la classe ricevuta 
            if(!class_exists($pictureable_type))
                throw new \Exception('Eloquent Model not found');
            
            // carico il record indicato
            $pictureable = $pictureable_type::with(Array('picture' => function($query){$query->orderBy('order'); }))->find($pictureable_id);
            if(!$pictureable)
                throw new \Exception('Record not found');
            
            $data['status'] = 1;
            
            $pictures = Array();
            foreach($pictureable->picture as $item){
                $item->thumbnail = Image::resize($item->filename, Config::get('bloom.admin_thumbnail_width'), Config::get('bloom.admin_thumbnail_height'), true);
                $pictures[] = $item;
            }
            
            $data['data'] = $pictures;
               
        }
        catch(\Exception $e){
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }

    
    public function deletePicture()
    {
        try{
            
            $picture = Picture::find(Input::get('picture_id'));
            
            if(!$picture)            
                throw new \Exception('Immagine non trovata');
            
            $picture->delete();
            
            $data['status'] = 1;
            $data['message'] = 'Immagine cancellata correttamente';
               
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    
    public function renamePicture()
    {
        try{
                       
            
           $picture = Picture::find(Input::get('pk'));
            
            if(!$picture)            
                throw new \Exception('Immagine non trovata');
            
            $picture->label = Input::get('value');
            $picture->save();
            
            $data['status'] = 1;
            $data['message'] = 'Immagine cancellata correttamente';
               
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    public function setMainPicture()
    {
        try{
            
            $picture = Picture::find(Input::get('picture_id'));
            $main = Input::get('picture_main');
            
            
            if(!$picture)            
                throw new \Exception('Immagine non trovata');
            
            if($main)
                $picture->setMain();
            else{
                $picture->main = false;
                $picture->save();
            }
            
            $data['status'] = 1;
            $data['picture_id'] = $picture->id;
            $data['picture_main'] = $main;
            $data['message'] = 'Stato modificato correttamente';
               
        }
        catch(\Exception $e){
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    
    public function setOrderPicture()
    {
        try{
            
            $record_id = Input::get('record_id');
            $ids = Input::get('ids');
           
            $counter = 0;
            if(empty($ids))
                throw new \Exception('Nessun id specificato');
            
            foreach($ids as $item){
                $pic = Picture::find($item);
                if($pic && $pic->pictureable_id == $record_id){
                    $pic->order = $counter;
                    $pic->save();
                    $counter++;
                }
            }
            $data['status'] = 1;
            $data['record_id'] = $record_id;
            $data['ids'] = implode(',', $ids);
            $data['message'] = 'Ordine modificato correttamente';
        }
        catch(\Exception $e){
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
     // ATTACHMENTS
    
    public function uploadAttachment()
    {
        $data = Array();
            
        try{
            
            $attachmentable_type = Input::get('attachmentable_type');
            $attachmentable_id = Input::get('attachmentable_id');
            $destination_path = Input::get('destination_path');
            
            
            if(!$destination_path)
                throw new \Exception('Destination Path not set');
            
            // verifico che esista la classe ricevuta 
            if(!class_exists($attachmentable_type))
                throw new \Exception('Eloquent Model not found');
            
            // carico il record indicato
            $attachmentable = $attachmentable_type::find($attachmentable_id);
            if(!$attachmentable)
                throw new \Exception('Record not found');
            
            // creo un nuovo record di tipo Attachment
            $attachment = new Attachment;
            
            
            // gestisco il file ricevuto
            $file = Input::file('file');
            $attachment->label = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension(); //if you need extension of the file
            
            $filename = uniqid().'.'.$extension;
            
            $attachment->size = $file->getSize();
            $uploadSuccess = $file->move($destination_path, $filename);
            
            
            
            $attachment->extension = $extension;
            $attachment->download_count = 0;
            $attachment->filename = $destination_path.$filename;
            $attachment->save();
            
            $attachmentable->attachment()->save($attachment);
            
            $data['status'] = 1;
            $data['label'] = $attachment->label;
            $data['filename'] = $attachment->filename;
               
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    public function loadAttachments()
    {
        $data = Array();
            
        try{
            
            $attachmentable_type = Input::get('attachmentable_type');
            $attachmentable_id = Input::get('attachmentable_id');

            // verifico che esista la classe ricevuta 
            if(!class_exists($attachmentable_type))
                throw new \Exception('Eloquent Model not found');
            
            // carico il record indicato
            $attachmentable = $attachmentable_type::with(Array('attachment' => function($query){ $query->orderBy('order'); }))->find($attachmentable_id);
            if(!$attachmentable)
                throw new \Exception('Record not found');
            
            $data['status'] = 1;
            
            $data['data'] = $attachmentable->attachment;
            
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }

    
    public function deleteAttachment()
    {
        try{
            
            $attachment = Attachment::find(Input::get('attachment_id'));
            
            if(!$attachment)            
                throw new \Exception('Allegato non trovato');
            
            $attachment->delete();
            
            $data['status'] = 1;
            $data['message'] = 'Allegato cancellato correttamente';
               
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    public function renameAttachment()
    {
        try{
                       
            
           $attachment = Attachment::find(Input::get('pk'));
            
            if(!$attachment)            
                throw new \Exception('Allegato non trovato');
            
            $attachment->label = Input::get('value');
            $attachment->save();
            
            $data['status'] = 1;
            $data['message'] = 'Allegato rinominato correttamente';
               
        }
        catch(\Exception $e){
        
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }
    
    
    public function setOrderAttachment()
    {
        try{
            $record_id = Input::get('record_id');
            $ids = Input::get('ids');
            
            $counter = 0;
            if(empty($ids))
                throw new \Exception('Nessun id specificato');
            
            foreach($ids as $item){
                $att = Attachment::find($item);
                if($att && $att->attachmentable_id == $record_id){
                    $att->order = $counter;
                    $att->save();
                    $counter++;
                }
            }
            $data['status'] = 1;
            $data['record_id'] = $record_id;
            $data['ids'] = implode(',', $ids);
            $data['message'] = 'Ordine modificato correttamente';
        }
        catch(\Exception $e){
            $data['status'] = 0;
            $data['message'] = $e->getMessage();
        }
                
        return Response::json($data);
    }

}

