<?php 

namespace App\Http\Controllers;

use Input;
use View;
use Auth;
use Language;
use MenuNode;

class PublicController extends BloomController {
        
        var $query_string = ''; // Parametri ricevuti in get senza il parametro page (per la paginazione)
        var $search_string = ''; 

        function __construct(){
            
            Language::initialize();
            $language = Input::get('lang');
            if($language)
                Language::setCurrent($language);
            
            $this->asset_css(asset('assets/css/bootstrap.min.css'));
            $this->asset_js(asset('bower_components/jquery/dist/jquery.min.js'));
            $this->asset_js(asset('bower_components/bootstrap-sass/assets/javascripts/bootstrap.js'));
            
            
            View::composer('www._layouts.*', function($view)
            {
                $view->with('head_assets',  $this->head_assets);
            });
            
            View::composer('www.*', function($view)
            {
                $view->with('languages',  Language::where('online',1)->get());
                $view->with('language_current',  Language::getCurrent());
                $view->with('main_menu_nodes',  MenuNode::buildHierarchy(1,Language::getCurrent()));
                $view->with('user',  Auth::user());
                
            });
            
            
            
        }

       

}