<?php namespace App\Services\Validators;
 
class ArticleCategoryValidator extends Validator {
 
    public static $rules = array(
        'label' => 'required',
    );
 
}