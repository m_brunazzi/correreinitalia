<?php namespace App\Services\Validators;
 
class GalleryTypeValidator extends Validator {
 
    public static $rules = array(
        'label' => 'required',
    );
 
}