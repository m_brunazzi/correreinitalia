<?php namespace App\Services\Validators;
 
class NewsletterValidator extends Validator {
 
    public static $rules = array(
        'sender_email' => 'required',
        'title' => 'required',
        'subject' => 'required',
        'body' => 'required'
    );
 
}