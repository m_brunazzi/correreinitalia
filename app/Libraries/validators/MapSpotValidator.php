<?php namespace App\Services\Validators;
 
class MapSpotValidator extends Validator {
 
    public static $rules = array(
        'map_id' => 'required',
        'name' => 'required'
    );
 
}