<?php namespace App\Services\Validators;
 
class NewsletterRecipientValidator extends Validator {
 
    public static $rules = array(
        'email' => 'required',
        'group_id' => 'required'
    );
}