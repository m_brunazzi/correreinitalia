<?php namespace App\Services\Validators;
 
class Contest2014Validator extends Validator {
 
    public static $rules = array(
        'essay' => 'required',
        'img1' => 'image',
        'img2' => 'image',
        'img3' => 'image',
    );
 
}