<?php namespace App\Services\Validators;
 
class ContactValidator extends Validator {
 
    public static $rules = array(
        'email' => 'email|required|unique:blm_contact',
    );
 
}