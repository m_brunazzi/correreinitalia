<?php namespace App\Services\Validators;
 
class PictureValidator extends Validator {
 
    public static $rules = array(
        'label' => 'required',
        'img' => 'image'
    );
 
}