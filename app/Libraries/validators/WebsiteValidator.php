<?php namespace App\Services\Validators;
 
class WebsiteValidator extends Validator {
 
    public static $rules = array(
        'name' => 'required',
        'prefix'  => 'required',
    );
 
}