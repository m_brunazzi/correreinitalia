<?php namespace App\Services\Validators;
 
class CalendarEventValidator extends Validator {
 
    public static $rules = array(
        'label' => 'required',
        'calendar_id' => 'required',
        'date_start' => 'required'
    );
 
}