<?php namespace App\Services\Validators;
 
class LanguageValidator extends Validator {
 
    public static $rules = array(
        'language' => 'required',
        'code'  => 'required',
    );
 
}