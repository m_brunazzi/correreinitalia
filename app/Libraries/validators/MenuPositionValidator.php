<?php namespace App\Services\Validators;
 
class MenuPositionValidator extends Validator {
 
    public static $rules = array(
        'website_id' => 'required',
        'name' => 'required'
    );
 
}