<?php namespace App\Services\Validators;
 
class MapValidator extends Validator {
 
    public static $rules = array(
        'website_id' => 'required',
        'name' => 'required',
        'img' => 'image'
    );
 
}