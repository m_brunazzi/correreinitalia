<?php namespace App\Services\Validators;
 
class SliderValidator extends Validator {
 
    public static $rules = array(
        'label' => 'required',
        'img' => 'image'
    );
 
}