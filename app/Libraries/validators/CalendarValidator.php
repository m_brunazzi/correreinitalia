<?php namespace App\Services\Validators;
 
class CalendarValidator extends Validator {
 
    public static $rules = array(
        'label' => 'required',
        'color' => 'required'
    );
 
}